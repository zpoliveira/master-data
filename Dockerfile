﻿FROM mcr.microsoft.com/dotnet/sdk:5.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
#WORKDIR /dev

COPY . .
COPY ./master-data.sln .
#COPY ./src/Domain/master-data.domain.csproj ./src/Domain/
#COPY ./src/Mappers/master-data.dtos/master-data.dtos.csproj ./src/Mappers/master-data.dtos/
#COPY ./src/Persistence/master-data.persistence.csproj ./src/Persistence/
#COPY ./src/Repository/master-data.repository.csproj ./src/Repository/
#COPY ./src/Service/master-data.service.csproj ./src/Service/
#COPY ./src/WebApi/master-data.webapi.csproj ./src/WebApi/
#COPY ./tests/master-data.tests/master-data.tests.csproj ./tests/master-data.tests/

RUN dotnet build "./src/WebApi/master-data.webapi.csproj"
RUN dotnet publish "./src/WebApi/master-data.webapi.csproj" -c Release -o /app/out

# Build runtime image
FROM base as final
WORKDIR /app
COPY --from=build /app/out .
ENTRYPOINT ["dotnet", "master-data.webapi.dll"]
