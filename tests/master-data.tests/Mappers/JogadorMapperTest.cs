﻿using System;
using System.Linq;
using AutoFixture;
using FluentAssertions;
using master_data.domain.Model.Entities;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Mappers;
using master_data.mappers.Shared;
using Xunit;

namespace master_data.tests.Mappers
{
    public class JogadorMapperTest
    {
        public JogadorMapperTest()
        {
            _fixture = new Fixture();
            // client has a circular reference from AutoFixture point of view
            _fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            _moqJogador = _fixture.Create<Jogador>();

            _moqRegistarJogadorDto = _fixture.Create<RegistarJogadorDto>();
            _moqRegistarJogadorDto.dataNascimento = "01/01/1980";

            _moqRepositoryDto = _fixture.Create<JogadorRepositoryDto>();
            _moqRepositoryDto.DataNascimento = DateTime.Now;
            _moqRepositoryDto.Humor = "FELIZ"; //new HumorRepositoryDto("FELIZ");

            _mapper = new JogadorMapper(new TagMapper()/*,new HumorMapper()*/);
        }

        private Fixture _fixture { get; }
        private Jogador _moqJogador { get; }
        private RegistarJogadorDto _moqRegistarJogadorDto { get; }
        private JogadorRepositoryDto _moqRepositoryDto { get; }

        private JogadorMapper _mapper { get; }

        
       /* [Fact]
        public void ToRepositoryDto_GivenDomainJogador_ReturnsDto()
        {
            //Arrange
            var expected = new JogadorRepositoryDto(
                new RepoGuid(_moqJogador.Id),
                _moqJogador.Nome.Value,
                _moqJogador.Email.Value,
                humor: _moqJogador.Humor.Value.ToString(),
                password: _moqJogador.Password.Value,
                telefone: _moqJogador.Telefone.Value,
                dataNascimento: _moqJogador.DataNascimento.Value,
                urlFacebook: _moqJogador.UrlFacebook.Value,
                urlLinkedIn: _moqJogador.UrlLinkedIn.Value);

            //Act
            var result = _mapper.ToRepositoryDto(_moqJogador);

            //Assert
            result.Should().Be(expected);
        }
        

        
        [Fact]
        public void FromRepositoryDto_GivenDtoJogador_ReturnsDomain()
        {
            //Arrange
            var expected = new Jogador(
                new Nome(_moqRepositoryDto.Nome),
                new Email(_moqRepositoryDto.Email),
                new DataNascimento(_moqRepositoryDto.DataNascimento),
                new Password(_moqRepositoryDto.Password),
                new Humor((EstadoHumor) Enum.Parse(typeof(EstadoHumor), _moqRepositoryDto.Humor.ToString())),
                new Url(_moqRepositoryDto.UrlFacebook),
                new Url(_moqRepositoryDto.UrlLinkedIn),
                new Telefone(_moqRepositoryDto.Telefone)
                );
            
            //Act
            var result = _mapper.FromRepositoryDto(_moqRepositoryDto);

            //Assert
            result.Should().Be(expected);
        }*/
        

        
       /* [Fact]
        public void ToRegistarUtilizadorDto_GivenDomainJogador_ReturnsDto()
        {
            //Arrange
            var expected = new RegistarJogadorDto(
                _moqJogador.Nome.Value,
                _moqJogador.Email.Value,
                _moqJogador.Password.Value,
                _moqJogador.Telefone.Value,
                _moqJogador.UrlFacebook.Value,
                _moqJogador.UrlLinkedIn.Value,
                _moqJogador.DataNascimento.Value.ToShortDateString(),
                _moqJogador.Humor.ToString(),
                _moqJogador.Tags.Select(t => t.Value).ToList()
            );

            //Act
            var result = _mapper.ToRegistarUtilizadorDto(_moqJogador);

            //Assert
            result.Should().Be(expected);
        }
        

        
        [Fact]
        public void FromRegistarUtilizadorDto_GivenDomainJogador_ReturnsDomain()
        {
            //Arrange
            var expected = new Jogador(
                new Nome(_moqRegistarJogadorDto.nome),
                new Email(_moqRegistarJogadorDto.email),
                password: new Password(_moqRegistarJogadorDto.password),
                telefone: new Telefone(_moqRegistarJogadorDto.telefone),
                urlFacebook: new Url(_moqRegistarJogadorDto.urlFacebook),
                urlLinkedIn: new Url(_moqRegistarJogadorDto.urlLinkedIn),
                dataNascimento: new DataNascimento(_moqRegistarJogadorDto.dataNascimento));


            //Act
            var result = _mapper.FromRegistarUtilizadorDto(_moqRegistarJogadorDto);

            //Assert
            result.Should().Be(expected);
        }*/
        
    }
}