using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions.Execution;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Interfaces;
using master_data.mappers.Mappers;
using master_data.mappers.Shared;
using master_data.persistence.Jogadores;
using master_data.persistence.Tags;
using master_data.repository.Interfaces;
using master_data.repository.Shared;
using master_data.service.Jogador;
using master_data.service.Relacionamento;
using masterdata.api.controllers.Jogador;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace master_data.tests.Integracao
{
    public class EditarRelacionamentoControllerService
    {

        private IRelacionamentoMapper _mapper;
        private Mock<IRelacionamentoRepository> _moqRelacionamentoRepository;
        private Mock<ITagRepository> _moqTagRepository;
        private Mock<IUnitOfWork> _moqUnitofWork;
        private IRelacionamentoService _service;
        private ServiceCollection _serviceProvider;
        
        
        public EditarRelacionamentoControllerService()
        {
            
        }

        public  void NotFound()
        {
            /*_serviceProvider = new ServiceCollection();
            _serviceProvider.AddTransient<IRelacionamentoMapper, RelacionamentoMapper>();
            _serviceProvider.AddScoped<IRelacionamentoService, RelacionamentoService>();

            var serviceProvider = _serviceProvider.BuildServiceProvider();

            _mapper = serviceProvider.GetService<IRelacionamentoMapper>();
            _moqRelacionamentoRepository = new Mock<IRelacionamentoRepository>();
            _moqTagRepository = new Mock<ITagRepository>();
            _moqUnitofWork = new Mock<IUnitOfWork>();

            Guid jogadorDe = Guid.NewGuid();
            Guid jogadorPara = Guid.NewGuid();

            EditarRelacionamentoDto dto = new EditarRelacionamentoDto(jogadorDe, jogadorPara, 2, new List<string>());
            
            _moqRelacionamentoRepository
                .Setup(x => x.getRelacionamentoAsync(It.IsAny<RepoGuid>(), It.IsAny<RepoGuid>()))
                .ReturnsAsync((RelacionamentoRepositoryDto) null);

            _serviceProvider.AddScoped((IRe)_moqRelacionamentoRepository.Object);
            
            
            RelacionamentoController controller = new RelacionamentoController(_service);
            var result = await controller.EditarRelacionamento(jogadorDe, jogadorPara, dto);
            */
            

        }

    }
}