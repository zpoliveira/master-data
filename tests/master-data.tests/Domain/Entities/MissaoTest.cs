using System;
using System.Threading.Tasks;
using master_data.domain.Model.Entities;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;
using Xunit;

namespace master_data.tests.Domain.Entities
{
    public class MissaoTest
    {

        [Fact]
        public void contrutor()
        {
            Jogador jogadorDesafiado = new Jogador(Guid.NewGuid(), new Nome("Jorge Manuel"), new Email("mail@mail.com"),
                new Password("123*asd"),new Humor(EstadoHumor.FELIZ),null,null,new Telefone("999999999"),new DataNascimento(DateTime.Today.AddYears(-20)), 
                null, null,  null, null);
            
            Jogador jogadorObjectivo = new Jogador(Guid.NewGuid(), new Nome("Jorge Miguel"), new Email("mailMiguel@mail.com"),
                new Password("123*asd"),new Humor(EstadoHumor.FELIZ),null,null,new Telefone("999999999"),new DataNascimento(DateTime.Today.AddYears(-20)), 
                null, null,  null, null);

            Guid expectedGuid = Guid.NewGuid();
            
            Missao missaoOk = new Missao(expectedGuid, jogadorDesafiado, jogadorObjectivo, new GrauDificuldade(2), null,
                new DataHoraInicio(DateTime.Today), null);
            
           Func<Missao> funcThow = () => new Missao(Guid.Empty, null,null,null,null, null,null);
            var returnGuidEmpty =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnGuidEmpty.Message.Contains("id", StringComparison.CurrentCultureIgnoreCase));
            
             funcThow = () => new Missao(Guid.NewGuid(),null ,null,null,null, null,null);
            var returnJogadorDesafiadoNull =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnJogadorDesafiadoNull.Message.Contains("jogador", StringComparison.CurrentCultureIgnoreCase));
            
            funcThow = () => new Missao(Guid.NewGuid(), jogadorDesafiado,null,null,null, null,null);
            var returnJogadorObjectivoNull =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnJogadorObjectivoNull.Message.Contains("jogador", StringComparison.CurrentCultureIgnoreCase));
            
            funcThow = () => new Missao(Guid.NewGuid(), jogadorDesafiado,jogadorDesafiado,null,null, null,null);
            var returnJogadorDesafiadoObjectivoIgual =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnJogadorDesafiadoObjectivoIgual.Message.Contains("jogador", StringComparison.CurrentCultureIgnoreCase));
            
            
            funcThow = () => new Missao(Guid.NewGuid(), jogadorDesafiado,jogadorObjectivo,null,null, null,null);
            var returnGrauDificuldadeNull =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnGrauDificuldadeNull.Message.Contains("grau", StringComparison.CurrentCultureIgnoreCase));
            
            funcThow = () => new Missao(Guid.NewGuid(), jogadorDesafiado,jogadorObjectivo,new GrauDificuldade(2),null, null,null);
            var returnDataHoraInicioNull =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnDataHoraInicioNull.Message.Contains("data", StringComparison.CurrentCultureIgnoreCase));
            
            
            funcThow = () => new Missao(Guid.NewGuid(), jogadorDesafiado,jogadorObjectivo,new GrauDificuldade(2),null, new DataHoraInicio(DateTime.Today),new DataHoraFim(DateTime.Today.AddDays(-10)));
            var returnDataFimMenorDataInicio =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnDataFimMenorDataInicio.Message.Contains("data", StringComparison.CurrentCultureIgnoreCase));
            
            Missao missaoNova = new Missao( jogadorDesafiado,jogadorObjectivo,new GrauDificuldade(2), new DataHoraInicio(DateTime.Today));
            Assert.NotNull(missaoNova);
        }

        [Fact]
        public void equalsTest()
        {
            Jogador jogadorDesafiado = new Jogador(Guid.NewGuid(), new Nome("Jorge Manuel"), new Email("mail@mail.com"),
                new Password("123*asd"),new Humor(EstadoHumor.FELIZ),null,null,new Telefone("999999999"),new DataNascimento(DateTime.Today.AddYears(-20)), 
                null, null,  null, null);
            
            Jogador jogadorObjectivo = new Jogador(Guid.NewGuid(), new Nome("Jorge Miguel"), new Email("mailMiguel@mail.com"),
                new Password("123*asd"),new Humor(EstadoHumor.FELIZ),null,null,new Telefone("999999999"),new DataNascimento(DateTime.Today.AddYears(-20)), 
                null, null,  null, null);

            Guid expectedGuid = Guid.NewGuid();
            
            Missao missaoOk = new Missao(expectedGuid, jogadorDesafiado, jogadorObjectivo, new GrauDificuldade(2), null,
                new DataHoraInicio(DateTime.Today), null);
            
            Missao result = new Missao(expectedGuid,jogadorDesafiado, jogadorObjectivo, new GrauDificuldade(2), null,
                new DataHoraInicio(DateTime.Today), null);
            
            Assert.True(missaoOk.Equals( result));
            
            Assert.True(missaoOk.Equals(missaoOk));
            
            Assert.False(missaoOk.Equals(null));
            
            Assert.False(missaoOk.Equals(jogadorDesafiado));
        }

        [Fact]
        public void GetHashCodeTest()
        {
            Jogador jogadorDesafiado = new Jogador(Guid.NewGuid(), new Nome("Jorge Manuel"), new Email("mail@mail.com"),
                new Password("123*asd"),new Humor(EstadoHumor.FELIZ),null,null,new Telefone("999999999"),new DataNascimento(DateTime.Today.AddYears(-20)), 
                null, null,  null, null);
            
            Jogador jogadorObjectivo = new Jogador(Guid.NewGuid(), new Nome("Jorge Miguel"), new Email("mailMiguel@mail.com"),
                new Password("123*asd"),new Humor(EstadoHumor.FELIZ),null,null,new Telefone("999999999"),new DataNascimento(DateTime.Today.AddYears(-20)), 
                null, null,  null, null);
            
            Guid expectedGuid = Guid.NewGuid();
            
            Missao missaoOk = new Missao(expectedGuid, jogadorDesafiado, jogadorObjectivo, new GrauDificuldade(2), null,
                new DataHoraInicio(DateTime.Today), null);
            
            Missao result = new Missao(expectedGuid,jogadorDesafiado, jogadorObjectivo, new GrauDificuldade(2), null,
                new DataHoraInicio(DateTime.Today), null);
            
            Assert.Equal(missaoOk.GetHashCode(),result.GetHashCode());
            
            result = new Missao(jogadorDesafiado, jogadorObjectivo, new GrauDificuldade(2),
                new DataHoraInicio(DateTime.Today));
            
            Assert.NotEqual(missaoOk.GetHashCode(),result.GetHashCode());
            
        }

        [Fact]
        public void GetPropertiesTest()
        {
            Jogador jogadorDesafiado = new Jogador(Guid.NewGuid(), new Nome("Jorge Manuel"), new Email("mail@mail.com"),
                new Password("123*asd"),new Humor(EstadoHumor.FELIZ),null,null,new Telefone("999999999"),new DataNascimento(DateTime.Today.AddYears(-20)), 
                null, null,  null, null);
            
            Jogador jogadorObjectivo = new Jogador(Guid.NewGuid(), new Nome("Jorge Miguel"), new Email("mailMiguel@mail.com"),
                new Password("123*asd"),new Humor(EstadoHumor.FELIZ),null,null,new Telefone("999999999"),new DataNascimento(DateTime.Today.AddYears(-20)), 
                null, null,  null, null);
            
            Guid expectedGuid = Guid.NewGuid();

            GrauDificuldade dificuldade = new GrauDificuldade(2);

            Pontos pontos = new Pontos(10);

            DataHoraInicio dataHoraInicio = new DataHoraInicio(DateTime.Now.AddDays(-10));

            DataHoraFim dataHoraFim = new DataHoraFim(DateTime.Now);
            
            Missao missaoOk = new Missao(expectedGuid, jogadorDesafiado, jogadorObjectivo, dificuldade, pontos,
                dataHoraInicio, dataHoraFim);
            
            Assert.Equal(missaoOk.JogadorDesafiado, jogadorDesafiado);
            
            Assert.Equal(missaoOk.JogadorObjectivo, jogadorObjectivo);
            
            Assert.Equal(missaoOk.GrauDificuldade, dificuldade);
            
            Assert.Equal(missaoOk.Pontos, pontos);
            
            Assert.Equal(missaoOk.DataHoraInicio, dataHoraInicio);
            
            Assert.Equal(missaoOk.DataHoraFim, dataHoraFim);
        }
        

    }
}