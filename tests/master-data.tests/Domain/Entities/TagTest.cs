using System;
using master_data.domain.Model.Entities;
using master_data.domain.Shared;
using Xunit;

namespace master_data.tests.Domain.Entities
{
    public class TagTest
    {
        
        private Tag exp1 ;
            
        private  Tag exp3 ;

        private Tag exp2 ;

        public TagTest()
        {
            exp1 = new Tag(".net");
            
            exp3 = new Tag(".net");

            exp2 = new Tag("C#");
        }

        [Fact]
        public void ConstrutorTest()
        {
            Func<Tag> funcThow = () => new Tag(null);
            var returnNull =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnNull.Message.Contains("tag", StringComparison.CurrentCultureIgnoreCase));
            
            
            funcThow = () => new Tag("");
            var returnEmpty =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnEmpty.Message.Contains("tag", StringComparison.CurrentCultureIgnoreCase));

            Tag tag = new Tag(".Net");
            
            Assert.NotNull(tag);
            Assert.IsType<Tag>(tag);

        }
        

        [Fact]
        public void GetPropertyTest()
        {
            string expexted = ".net";

            Tag obj = new Tag(expexted);
            
            Assert.Equal(expexted, obj.Value);

        }

        [Fact]
        public void EqualsTest()
        {

            Assert.False(exp1.Equals(null));
            
            Assert.True(exp1.Equals(exp1));
            
            Assert.False(exp1.Equals(DateTime.Now));
            
            Assert.True(exp1.Equals(exp3));
            
            
        }


        [Fact]
        public void GetHashCodeTest()
        {
            
            Assert.Equal(exp1.GetHashCode(),exp3.GetHashCode());
            
            Assert.NotEqual(exp1.GetHashCode(),exp2.GetHashCode());
            
            
        }
    }
}