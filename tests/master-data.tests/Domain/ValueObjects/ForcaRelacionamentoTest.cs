using System;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;
using Xunit;

namespace master_data.tests.Domain.ValueObjects
{
    public class ForcaRelacionamentoTest
    {
        private ForcaRelacionamento exp1 ;
            
        private ForcaRelacionamento exp3 ;

        private ForcaRelacionamento exp2 ;

        public ForcaRelacionamentoTest()
        {
            exp1 = new ForcaRelacionamento(10);
            
            exp3 = new ForcaRelacionamento(10);

            exp2 = new ForcaRelacionamento(15);
        }

        [Fact]
        public void ConstrutorTest()
        {
            ForcaRelacionamento ForcaRelacionamento = new ForcaRelacionamento(10);
            
            Assert.NotNull(ForcaRelacionamento);
            Assert.IsType<ForcaRelacionamento>(ForcaRelacionamento);

        }
        

        [Fact]
        public void GetPropertyTest()
        {
            int expexted = 10;

            ForcaRelacionamento obj = new ForcaRelacionamento(expexted);
            
            Assert.Equal(expexted, obj.Value);

        }

        [Fact]
        public void EqualsTest()
        {

            Assert.False(exp1.Equals(null));
            
            Assert.True(exp1.Equals(exp1));
            
            Assert.False(exp1.Equals(DateTime.Now));
            
            Assert.True(exp1.Equals(exp3));
            
            
        }


        [Fact]
        public void GetHashCodeTest()
        {
            
            Assert.Equal(exp1.GetHashCode(),exp3.GetHashCode());
            
            Assert.NotEqual(exp1.GetHashCode(),exp2.GetHashCode());
            
            
        }
        
    }
}