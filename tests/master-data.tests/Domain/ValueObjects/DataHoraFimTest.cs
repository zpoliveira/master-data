using System;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;
using Xunit;

namespace master_data.tests.Domain.ValueObjects
{
    public class DataHoraFimTest
    {
        private DataHoraFim exp1;

        private DataHoraFim exp3;

        private DataHoraFim exp2;

        public DataHoraFimTest()
        {
            exp1 = new DataHoraFim(DateTime.Today);
            
            exp3 = new DataHoraFim(DateTime.Today);

            exp2 = new DataHoraFim(DateTime.Today.AddDays(3));
        }

        [Fact]
        public void ConstrutorTest()
        {
            
            DataHoraFim DataHoraFim = new DataHoraFim(DateTime.Now);
            
            Assert.NotNull(DataHoraFim);
            Assert.IsType<DataHoraFim>(DataHoraFim);

        }
        

        [Fact]
        public void GetPropertyTest()
        {
            DateTime expexted = DateTime.Today;

            DataHoraFim obj = new DataHoraFim(expexted);
            
            Assert.Equal(expexted, obj.value);

        }

        [Fact]
        public void EqualsTest()
        {

            Assert.False(exp1.Equals(null));
            
            Assert.True(exp1.Equals(exp1));
            
            Assert.False(exp1.Equals(DateTime.Now));
            
            Assert.True(exp1.Equals(exp3));
            
            
        }


        [Fact]
        public void GetHashCodeTest()
        {
            
            Assert.Equal(exp1.GetHashCode(),exp3.GetHashCode());
            
            Assert.NotEqual(exp1.GetHashCode(),exp2.GetHashCode());
            
            
        }
    }
}