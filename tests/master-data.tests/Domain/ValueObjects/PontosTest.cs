using System;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;
using Xunit;

namespace master_data.tests.Domain.ValueObjects
{
    public class PontosTest
    {
        private Pontos exp1 ;
            
        private Pontos exp3 ;

        private Pontos exp2 ;

        public PontosTest()
        {
            exp1 = new Pontos(10);
            
            exp3 = new Pontos(10);

            exp2 = new Pontos(15);
        }

        [Fact]
        public void ConstrutorTest()
        {
            
            Func<Pontos> funcThow = () => new Pontos(-1);
            var returnNegative =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnNegative.Message.Contains("Pontos", StringComparison.CurrentCultureIgnoreCase));

            Pontos Pontos = new Pontos(10);
            
            Assert.NotNull(Pontos);
            Assert.IsType<Pontos>(Pontos);

        }
        

        [Fact]
        public void GetPropertyTest()
        {
            int expexted = 10;

            Pontos obj = new Pontos(expexted);
            
            Assert.Equal(expexted, obj.Value);

        }

        [Fact]
        public void EqualsTest()
        {

            Assert.False(exp1.Equals(null));
            
            Assert.True(exp1.Equals(exp1));
            
            Assert.False(exp1.Equals(DateTime.Now));
            
            Assert.True(exp1.Equals(exp3));
            
            
        }


        [Fact]
        public void GetHashCodeTest()
        {
            
            Assert.Equal(exp1.GetHashCode(),exp3.GetHashCode());
            
            Assert.NotEqual(exp1.GetHashCode(),exp2.GetHashCode());
            
            
        }
    }
}