using System;
using master_data.domain.Model.ValueObjects;
using Xunit;

namespace master_data.tests.Domain.ValueObjects
{
    public class DataHoraInicioTest
    {
        private DataHoraInicio exp1;

        private DataHoraInicio exp3;

        private DataHoraInicio exp2;

        public DataHoraInicioTest()
        {
            exp1 = new DataHoraInicio(DateTime.Today);
            
            exp3 = new DataHoraInicio(DateTime.Today);

            exp2 = new DataHoraInicio(DateTime.Today.AddDays(3));
        }

        [Fact]
        public void ConstrutorTest()
        {
            
            DataHoraInicio DataHoraInicio = new DataHoraInicio(DateTime.Now);
            
            Assert.NotNull(DataHoraInicio);
            Assert.IsType<DataHoraInicio>(DataHoraInicio);

        }
        

        [Fact]
        public void GetPropertyTest()
        {
            DateTime expexted = DateTime.Today;

            DataHoraInicio obj = new DataHoraInicio(expexted);
            
            Assert.Equal(expexted, obj.value);

        }

        [Fact]
        public void EqualsTest()
        {

            Assert.False(exp1.Equals(null));
            
            Assert.True(exp1.Equals(exp1));
            
            Assert.False(exp1.Equals(DateTime.Now));
            
            Assert.True(exp1.Equals(exp3));
            
            
        }


        [Fact]
        public void GetHashCodeTest()
        {
            
            Assert.Equal(exp1.GetHashCode(),exp3.GetHashCode());
            
            Assert.NotEqual(exp1.GetHashCode(),exp2.GetHashCode());
            
            
        }
    }
}