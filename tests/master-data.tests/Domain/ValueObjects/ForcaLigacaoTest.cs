using System;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;
using Xunit;

namespace master_data.tests.Domain.ValueObjects
{
    public class ForcaLigacaoTest
    {
        private ForcaLigacao exp1 ;
            
        private ForcaLigacao exp3 ;

        private ForcaLigacao exp2 ;

        public ForcaLigacaoTest()
        {
            exp1 = new ForcaLigacao(10);
            
            exp3 = new ForcaLigacao(10);

            exp2 = new ForcaLigacao(15);
        }

        [Fact]
        public void ConstrutorTest()
        {
            
            Func<ForcaLigacao> funcThow = () => new ForcaLigacao(-1);
            var returnNegative =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnNegative.Message.Contains("Força", StringComparison.CurrentCultureIgnoreCase));
            
            funcThow = () => new ForcaLigacao(0);
            var returnZero =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnZero.Message.Contains("Força", StringComparison.CurrentCultureIgnoreCase));
            
            funcThow = () => new ForcaLigacao(101);
            var returnCem =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnCem.Message.Contains("Força", StringComparison.CurrentCultureIgnoreCase));
            
            ForcaLigacao ForcaLigacao = new ForcaLigacao(10);
            
            Assert.NotNull(ForcaLigacao);
            Assert.IsType<ForcaLigacao>(ForcaLigacao);

        }
        

        [Fact]
        public void GetPropertyTest()
        {
            int expexted = 10;

            ForcaLigacao obj = new ForcaLigacao(expexted);
            
            Assert.Equal(expexted, obj.Value);

        }

        [Fact]
        public void EqualsTest()
        {

            Assert.False(exp1.Equals(null));
            
            Assert.True(exp1.Equals(exp1));
            
            Assert.False(exp1.Equals(DateTime.Now));
            
            Assert.True(exp1.Equals(exp3));
            
            
        }


        [Fact]
        public void GetHashCodeTest()
        {
            
            Assert.Equal(exp1.GetHashCode(),exp3.GetHashCode());
            
            Assert.NotEqual(exp1.GetHashCode(),exp2.GetHashCode());
            
            
        }
    }
}