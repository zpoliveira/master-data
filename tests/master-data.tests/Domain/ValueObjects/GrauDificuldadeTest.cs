using System;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;
using Xunit;

namespace master_data.tests.Domain.ValueObjects
{
    public class GrauDificuldadeTest
    {

        GrauDificuldade exp1 ;
            
        GrauDificuldade exp3 ;

        GrauDificuldade exp2 ;

        public GrauDificuldadeTest()
        {
             exp1 = new GrauDificuldade(1);
            
             exp3 = new GrauDificuldade(1);

             exp2 = new GrauDificuldade(3);
        }


        [Fact]
        public void ConstrutorTest()
        {
            Func<GrauDificuldade> funcThow = () => new GrauDificuldade(0);
            var returnValueZero =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnValueZero.Message.Contains("Grau", StringComparison.CurrentCultureIgnoreCase));
            
            
            funcThow = () => new GrauDificuldade(-1);
            var returnValueNegativo =  Assert.Throws<BusinessRuleValidationException>(funcThow);
            Assert.True(returnValueNegativo.Message.Contains("Grau", StringComparison.CurrentCultureIgnoreCase));

            GrauDificuldade gd = new GrauDificuldade(1);
            
            Assert.NotNull(gd);
            Assert.IsType<GrauDificuldade>(gd);

        }
        

        [Fact]
        public void GetPropertyTest()
        {
            int expexted = 6;

            GrauDificuldade gd = new GrauDificuldade(expexted);
            
            Assert.Equal(expexted, gd.value);

        }

        [Fact]
        public void EqualsTest()
        {
            
            
            Assert.False(exp1.Equals(null));
            
            Assert.True(exp1.Equals(exp1));
            
            Assert.False(exp1.Equals(DateTime.Now));
            
            Assert.True(exp1.Equals(exp3));
            
            
        }


        [Fact]
        public void GetHashCodeTest()
        {

            Assert.Equal(exp1.GetHashCode(),exp3.GetHashCode());
            
            Assert.NotEqual(exp1.GetHashCode(),exp2.GetHashCode());
            
            
        }
        

    }
}