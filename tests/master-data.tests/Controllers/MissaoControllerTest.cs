using System;
using master_data.domain.Shared;
using master_data.mappers.Dtos.Controller.Missao;
using master_data.service.Missao;
using masterdata.api;
using masterdata.api.Controllers.Missao;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace master_data.tests.Controllers
{
    public class MissaoControllerTest
    {
        [Fact]
        public void RegistarMissao()
        {
            var bodyRequest = new RegistarMissaoDto()
            {
                GrauDificuldade = 2,
                JogadorDesafiado = "jodador1@mail.com",
                JogadorObjectivo = "jogador2@mail.com"
            };

            var mockService = new Mock<IMissaoService>();
            mockService.SetupSequence(x => x.RegistarMissao(bodyRequest))
                .ReturnsAsync(bodyRequest)
                .Throws<BusinessRuleValidationException>()
                .Throws<ApplicationException>();

            var controller = new MissaoController(mockService.Object);
            
            var responseOk = controller.RegistarMissao(bodyRequest).Result;
            Assert.IsType<CreatedResult>(responseOk);
            Assert.Equal(((CreatedResult) responseOk).Value, bodyRequest);
            
            var responseBussinessRuleValidation =
                controller.RegistarMissao( bodyRequest).Result;
            Assert.IsType<BadRequestObjectResult>(responseBussinessRuleValidation);
            Assert.IsType<ErrorDto>(((BadRequestObjectResult) responseBussinessRuleValidation).Value);
            
            
            var responseApllicationValidation =
                controller.RegistarMissao( bodyRequest).Result;
            Assert.IsType<BadRequestObjectResult>(responseBussinessRuleValidation);
            Assert.IsType<ErrorDto>(((BadRequestObjectResult) responseBussinessRuleValidation).Value);
            
            
        }
    }
}