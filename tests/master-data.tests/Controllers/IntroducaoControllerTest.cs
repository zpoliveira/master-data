using System;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using master_data.mappers.Dtos.Controller.Introducao;
using master_data.service.Introducao;
using masterdata.api.controllers.Introducao;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace master_data.tests.Controllers
{
    public class IntroducaoControllerTest
    {
        [Fact]
        public void CriarPedidodeLigacaoAsync_NoErrors()
        {
            var mockService = new Mock<IIntroducaoService>();
            var _controller = new IntroducaoController(mockService.Object);

            //Arrange
            var ligacao = new PedidoLigacaoDto("1161335@isep.ipp.pt", "1111111@isep.ipp.pt", 10);
            mockService.Setup(service => service.criaPedidoLigacaoAsync(ligacao));

            // Act
            var result = _controller.CriarPedidodeLigacaoAsync(ligacao).Result;

            // Assert
            result.Should().NotBe(new BadRequestResult());
        }

        [Fact]
        public void CriarPedidodeLigacaoAsync_returnsBadrequest()
        {
            var mockService = new Mock<IIntroducaoService>();
            var _controller = new IntroducaoController(mockService.Object);

            //Arrange
            var ligacao = new PedidoLigacaoDto("1161335@isep.ipp.pt", "1111111@isep.ipp.pt", 10);
            mockService.Setup(service => service.criaPedidoLigacaoAsync(ligacao))
                .ThrowsAsync(new ApplicationException("Não foi encontrado o jogador requisitante"));

            // Act
            try
            {
                var result = _controller.CriarPedidodeLigacaoAsync(ligacao).Result;
            }
            catch (Exception e)
            {
                // Assert
                Assert.IsType<BadRequestObjectResult>(e);
            }
        }
    }
}