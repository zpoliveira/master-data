using System;
using System.Collections.Generic;
using FluentAssertions;
using master_data.domain.Shared;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Controller.Relacionamento;
using master_data.service.Jogador;
using master_data.service.Relacionamento;
using masterdata.api;
using masterdata.api.controllers.Jogador;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace master_data.tests.Controllers
{
    public class RelacionamentoControllerTest
    {
        [Fact]
        public void EditarRelacionamento()
        {
            var JogadorUrlDe = "mail@mail.com";
            var JogadorUrlPara = "mail2@mail.com";

            var bodyRequest =
                new EditarRelacionamentoDto(JogadorUrlDe, JogadorUrlPara, 2, new List<string>());

            var mockService = new Mock<IRelacionamentoService>();
            mockService.SetupSequence(x => x.EditarRelacionamentoAsync(bodyRequest))
                .ReturnsAsync((EditarRelacionamentoDto) null)
                .ReturnsAsync(bodyRequest)
                .Throws<BusinessRuleValidationException>();


            var controller = new RelacionamentoController(mockService.Object);

            var responseNotFound = controller.EditarRelacionamento( bodyRequest).Result;
            Assert.IsType<NotFoundResult>(responseNotFound);

            var responseOk = controller.EditarRelacionamento( bodyRequest).Result;
            Assert.IsType<OkObjectResult>(responseOk);
            Assert.Equal(((OkObjectResult) responseOk).Value, bodyRequest);

            var responseBussinessRuleValidation =
                controller.EditarRelacionamento( bodyRequest).Result;
            Assert.IsType<BadRequestObjectResult>(responseBussinessRuleValidation);
            Assert.IsType<ErrorDto>(((BadRequestObjectResult) responseBussinessRuleValidation).Value);
        }
    }
}