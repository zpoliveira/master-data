using System;
using System.Threading.Tasks;
using FluentAssertions;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.service.Jogador;
using masterdata.api.controllers.Jogador;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace master_data.tests.Controllers
{
    public class JogadorControllerTest
    {
        [Fact]
        public void updateUtilizador_returnsWrongEmailError()
        {
            var mockService = new Mock<IJogadorService>();
            var _controller = new JogadorController(mockService.Object);

            //Arrange
            var dto = new UpdateJogadorDto("nome", "email1@isep.pt", "123", "911111111", "http://facebook.com/nome",
                "http://linkdin.com/nome", "01/01/1980", "NEUTRO", "Portuguesa", "Porto","Descrição Breve",  
                "",new []{"ISEP"});


            // Act
            try
            {
                _controller.UpdateUtilizador("email2@isep.pt", dto);
            }
            catch (Exception e)
            {
                // Assert
                Assert.IsType<BadRequestObjectResult>(e);
            }
        }

        [Fact]
        public async Task updateUtilizador_returnsJogadorInexistente()
        {
            //Arrange
            var dto = new UpdateJogadorDto("nome", "email1@isep.pt", "123", "911111111", "http://facebook.com/nome",
                "http://linkdin.com/nome", "01/01/1980", "NEUTRO", "Portuguesa", "Porto","Descrição Breve", 
                "",new []{"ISEP"});


            var mockService = new Mock<IJogadorService>();
            mockService.Setup(service => service.updateJogadorAsync(dto))
                .Throws<ApplicationException>();
            var _controller = new JogadorController(mockService.Object);

            // Act
            var e = await Assert.ThrowsAsync<ApplicationException>(() =>
                _controller.UpdateUtilizador("email1@isep.pt", dto));

            // Assert
            Assert.IsType<ApplicationException>(e);
        }

        [Fact]
        public void updateUtilizador_returnsOkResult()
        {
            //Arrange
            var dto = new UpdateJogadorDto("nome", "email1@isep.pt", "123", "911111111", "http://facebook.com/nome",
                "http://linkdin.com/nome", "01/01/1980", "NEUTRO", "Portuguesa", "Porto", "Descrição Breve", 
                "",new []{"ISEP"});


            var mockService = new Mock<IJogadorService>();
            var _controller = new JogadorController(mockService.Object);

            // Act
            var result = _controller.UpdateUtilizador("email1@isep.pt", dto);
            var ok = result.Result;
            // Assert
            ok.Should().NotBeNull();
            ok.Should().BeOfType<OkResult>();
        }


        [Fact]
        public void editarHumor_sucesso()
        {
            //Arrange
            var dto = new EditarHumorDto("a@isep.ipp.pt", "FELIZ");


            var mockService = new Mock<IJogadorService>();
            var controller = new JogadorController(mockService.Object);

            // Act
            var result = controller.EditarEstadoHumor(dto.email,dto);
            var success = result.Result;

            Console.Out.WriteLine("Test Ctrl OK");

            // Assert
            success.Should().NotBeNull();
            success.Should().BeOfType<CreatedResult>();
           
        }
    }
}