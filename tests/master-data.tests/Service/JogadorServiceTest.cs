﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Interfaces;
using master_data.mappers.Mappers;
using master_data.mappers.Shared;
using master_data.repository.Interfaces;
using master_data.repository.Shared;
using master_data.service.Jogador;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;

namespace master_data.tests.Service
{
    public class JogadorServiceTest
    {
        //Mocks
        private readonly Fixture _fixture;
        private readonly RegistarJogadorDto _moqRegistarJogadorDto;
        private readonly Mock<IJogadorRepository> _moqJogadorRepository;
        private readonly Mock<ITagRepository> _moqTagRepository;
        private readonly Mock<IUnitOfWork> _moqUnitofWork;
        private readonly Mock<IEstadoHumorRepository> _moqHumorRepository;

        //testing fields
        private readonly IJogadorService _jogadorService;
        private readonly IJogadorMapper _jogadorMapper;
        private readonly ITagMapper _tagMapper;
        private readonly IHumorMapper _humorMapper;

        public JogadorServiceTest()
        {
            _fixture = new Fixture();
            // client has a circular reference from AutoFixture point of view
            _fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            _moqRegistarJogadorDto = _fixture.Create<RegistarJogadorDto>();
            _moqRegistarJogadorDto.dataNascimento = "01/01/1980";
            _moqRegistarJogadorDto.avatar = "";
            _moqRegistarJogadorDto.tagList = new List<string>(){"tag"};

            _moqJogadorRepository = _fixture.Create<Mock<IJogadorRepository>>();
            _moqHumorRepository = _fixture.Create<Mock<IEstadoHumorRepository>>();
            _moqTagRepository = new Mock<ITagRepository>();
            _moqUnitofWork = new Mock<IUnitOfWork>();

            var services = new ServiceCollection();
            services.AddTransient<IJogadorMapper, JogadorMapper>();
            services.AddTransient<ITagMapper, TagMapper>();

            var serviceProvider = services.BuildServiceProvider();

            _jogadorMapper = serviceProvider.GetService<IJogadorMapper>();
            _humorMapper = serviceProvider.GetService<IHumorMapper>();

            _jogadorService = new JogadorService(_moqUnitofWork.Object, 
            _jogadorMapper,_moqTagRepository.Object,  _moqJogadorRepository.Object,  
            _humorMapper, _moqHumorRepository.Object);
        }





        [Fact]
        public async Task CriarNovoJogadorAsync_GivenRegistarJogadorDto_ShouldReturnSuccessAndSameDto()
        {
            //arrange
            var mapper = new JogadorMapper(new TagMapper());

            var repoDto = mapper.ToRepositoryDto(
                mapper.FromRegistarUtilizadorDto(
                    _moqRegistarJogadorDto));


            _moqJogadorRepository.Setup(a =>
                    a.AddAsync(It.Is<JogadorRepositoryDto>(r => r.Equals(repoDto))))
                .ReturnsAsync(repoDto);

            _moqTagRepository.Setup(x => x.GetByValuesAsync(It.IsAny<List<string>>()))
                .ReturnsAsync(new List<TagRepoDto>());

            _moqTagRepository.Setup(x=> x.AddRangeAsync(new List<TagRepoDto>()))
                .Verifiable();
            //act
            var result = await _jogadorService.criarNovoJogadorAsync(_moqRegistarJogadorDto);

            //assert
            result.Should().Be(_moqRegistarJogadorDto);
            _moqJogadorRepository.Verify(r =>
                    r.AddAsync(It.IsAny<JogadorRepositoryDto>()),
                Times.AtMostOnce());
        }

        [Fact]
        public async void updateJogadorAsync_ShouldReturnError()
        {
            // Arrange
            var dto = new UpdateJogadorDto("nome", "email1@isep.pt", "123", "911111111", "http://facebook.com/nome",
                "http://linkdin.com/nome", "01/01/1980", "NEUTRO", "Portuguesa", "Porto", "Descrição Breve", 
                "",new []{"ISEP"});
            var mockRepo = new Mock<IJogadorRepository>();
            var mockMapper = new Mock<IJogadorMapper>();
            var _service = new JogadorService(_moqUnitofWork.Object, 
            _jogadorMapper,_moqTagRepository.Object,  _moqJogadorRepository.Object,  
            _humorMapper, _moqHumorRepository.Object);
            mockRepo.Setup(repository => repository.GetUtilizadorByEmailAsync("email@isep.pt"))
                .Returns((Task<JogadorRepositoryDto>) null);

            // Act
            var error = await Assert.ThrowsAsync<ApplicationException>(() => _service.updateJogadorAsync(dto));

            // Assert
            error.Should().BeOfType<ApplicationException>();
            error.Message.Should().Be("Jogador Inexistente");
        }

        [Fact]
        public async void pesquisarJogadoresAsync_shouldReturnRegistarJogadorDto()
        {
            // Arrange
            var registarDto = new RegistarJogadorDto(
                "nome",
                "email1@isep.pt",
                "123",
                "911111111",
                "http://facebook.com/nome",
                "http://linkdin.com/nome",
                "01/01/1980",
                "FELIZ",
                "Portugues",
                "Vila Nova de Gaia",
                "Descrição Breve", 
                null,
                new[]
                {
                    "testeTag"
                });
            var expected = new List<RegistarJogadorDto>();
            expected.Add(registarDto);
            
            var repoDto = new JogadorRepositoryDto(new RepoGuid("2777f919-82e7-4a85-9761-4840dec383d1"), "nome",
                "email1@isep.pt", "123", "911111111", "http://facebook.com/nome",
                "http://linkdin.com/nome", "FELIZ", "Portuguesa", "Porto", "Descrição Breve",
                new byte[]{},new DateTime(123456789));
            var repoReturn = new List<JogadorRepositoryDto>();
            repoReturn.Add(repoDto);
            var pesquisaDto = new PesquisarJogadorDto();
            pesquisaDto.nome = "nome";

            var _service = new JogadorService(_moqUnitofWork.Object, 
            _jogadorMapper,_moqTagRepository.Object,  _moqJogadorRepository.Object,  
            _humorMapper, _moqHumorRepository.Object);;
            _moqJogadorRepository.Setup(repo => repo.PesquisarJogadoresAsync(It.IsAny<PesquisarJogadorDto>()))
                .ReturnsAsync(repoReturn);

            // Act
            var result = await _service.pesquisarJogadoresAsync(pesquisaDto);

            // Assert
            result.Count.Should().Be(expected.Count);
        }

        [Fact]
        public async Task pesquisarJogadoresAsync_shouldReturnPesquisaSemResultados()
        {
            // Arrange
            var repoReturn = new List<JogadorRepositoryDto>();
            var pesquisaDto = new PesquisarJogadorDto();
            pesquisaDto.nome = "nome";
            var mockRepo = new Mock<IJogadorRepository>();
            var mockMapper = new Mock<IJogadorMapper>();
            var _service = new JogadorService(_moqUnitofWork.Object, 
            _jogadorMapper,_moqTagRepository.Object,  _moqJogadorRepository.Object,  
            _humorMapper, _moqHumorRepository.Object);
            mockRepo.Setup(repo => repo.PesquisarJogadoresAsync(It.IsNotNull<PesquisarJogadorDto>()))
                .ReturnsAsync(new List<JogadorRepositoryDto>());

            // Act
            var error = await Assert.ThrowsAsync<ArgumentException>(() =>
                _service.pesquisarJogadoresAsync(pesquisaDto));

            // Assert
            error.Should().BeOfType<ArgumentException>();
            error.Message.Should().Be("Pesquisa sem resultados");
        }
    }
}