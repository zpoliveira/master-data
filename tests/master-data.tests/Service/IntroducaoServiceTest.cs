using System;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using master_data.mappers.Dtos.Controller.Introducao;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Interfaces;
using master_data.mappers.Shared;
using master_data.repository.Interfaces;
using master_data.repository.Shared;
using master_data.service.Introducao;
using Moq;
using Xunit;

namespace master_data.tests.Service
{
    public class IntroducaoServiceTest
    {
        //Mocks
        private readonly Fixture _fixture;
        private readonly Mock<IUnitOfWork> _moqUnitofWork;

        public IntroducaoServiceTest()
        {
            _fixture = new Fixture();
            // client has a circular reference from AutoFixture point of view
            _fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            _moqUnitofWork = _fixture.Create<Mock<IUnitOfWork>>();
        }


        [Fact]
        public async void criaPedidoLigacaoAsync_ErroDeJogadorRequisitante()
        {
            // Arrange
            var dto = new PedidoLigacaoDto("email1@isep.pt", "email2@isep.pt", 10);
            var mockRepo = new Mock<IIntroducaoRepository>();
            var mockJogadorRepo = new Mock<IJogadorRepository>();
            var mockTagRepo = new Mock<ITagRepository>();
            var mockRelRepo = new Mock<IRelacionamentoRepository>();
            var mockMapper = new Mock<IIntroducaoMapper>();
            var mockRelacRepo = new Mock<IRelacionamentoRepository>();
            
            var _service = new IntroducaoService(
                _moqUnitofWork.Object,
                mockMapper.Object,
                mockRepo.Object,
                mockJogadorRepo.Object, 
                mockTagRepo.Object, 
                mockRelRepo.Object);
            mockJogadorRepo.Setup(repository => repository.GetUtilizadorByEmailAsync(dto.emailJogadorRequisitante));

            // Act
            try
            {
                await _service.criaPedidoLigacaoAsync(dto);
            }
            catch (Exception e)
            {
                // Assert
                Assert.IsType<ApplicationException>(e);
                e.Message.Should().Be("Não foi encontrado o jogador requisitante");
            }
        }

        [Fact]
        public async void criaPedidoLigacaoAsync_ErroDeJogadorObjetivo()
        {
            // Arrange
            var dto = new PedidoLigacaoDto("email1@isep.pt", "email2@isep.pt", 10);
            var repoDto = new JogadorRepositoryDto(new RepoGuid("2777f919-82e7-4a85-9761-4840dec383d1"), "nome",
                "email1@isep.pt", "123", "911111111", "http://facebook.com/nome",
                "http://linkdin.com/nome", null, "Portuguesa", "Porto", "Descrição Breve", 
                new byte[]{},new DateTime(123456789));
            
            var mockRepo = new Mock<IIntroducaoRepository>();
            var mockJogadorRepo = new Mock<IJogadorRepository>();
            var mockTagRepo = new Mock<ITagRepository>();
            var mockRelRepo = new Mock<IRelacionamentoRepository>();
            var mockMapper = new Mock<IIntroducaoMapper>();
            var mockRelacRepo = new Mock<IRelacionamentoRepository>();
            
            var _service = new IntroducaoService(_moqUnitofWork.Object,
                mockMapper.Object,
                mockRepo.Object,
                mockJogadorRepo.Object,
                mockTagRepo.Object,
                mockRelacRepo.Object);

            mockJogadorRepo.Setup(repository => repository.GetUtilizadorByEmailAsync(dto.emailJogadorRequisitante))
                .ReturnsAsync(repoDto);
            mockJogadorRepo.Setup(repository => repository.GetUtilizadorByEmailAsync(dto.emailJogadorObjectivo));

            // Act
            try
            {
                await _service.criaPedidoLigacaoAsync(dto);
            }
            catch (Exception e)
            {
                // Assert
                Assert.IsType<ApplicationException>(e);
                e.Message.Should().Be("Não foi encontrado o jogador objetivo");
            }
        }
    }
}