using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using master_data.domain.Shared;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Controller.Relacionamento;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Interfaces;
using master_data.mappers.Mappers;
using master_data.mappers.Shared;
using master_data.repository.Interfaces;
using master_data.repository.Shared;
using master_data.service.Jogador;
using master_data.service.Relacionamento;
using master_data.social_engine.Engine;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;
using Range = Moq.Range;

namespace master_data.tests.Service
{
    public class RelacionamentoServiceTest
    {
        //Mocks
        private readonly Fixture _fixture;
        private readonly EditarRelacionamentoDto _moqRegistarJogadorDto;
        private Mock<IRelacionamentoRepository> _moqRelacionamentoRepository;
        private Mock<IJogadorRepository> _moqJogadorRepository;
        private Mock<ITagRepository> _moqTagRepository;
        private Mock<IUnitOfWork> _moqUnitofWork;
        private Mock<SocialNetwork> _moqSocialNetwork;
        private readonly IRelacionamentoMapper _mapper;

        private readonly RelacionamentoRepositoryDto _repositoryDto;

        //testing fields
        private IRelacionamentoService _service;

        public RelacionamentoServiceTest()
        {
            var services = new ServiceCollection();
            services.AddTransient<IRelacionamentoMapper, RelacionamentoMapper>();


            var serviceProvider = services.BuildServiceProvider();

            _mapper = serviceProvider.GetService<IRelacionamentoMapper>();

            var humorDto = "FELIZ"; //new HumorRepositoryDto("FELIZ");


            _moqRegistarJogadorDto =
                new EditarRelacionamentoDto("mail@mail.com", "mail2@mail2.com", 2, new List<string> {"tag"});
            _repositoryDto = new RelacionamentoRepositoryDto(new RepoGuid(Guid.NewGuid()),
                new JogadorRepositoryDto(new RepoGuid(Guid.NewGuid()), "Miguel", "mail@mail.com",
                    "pass", "919999999", "url", "url2", humorDto, "Portuguesa", "Porto", "Descrição Breve", 
                    new byte[]{}, DateTime.Now),new JogadorRepositoryDto(new RepoGuid(Guid.NewGuid()), "Miguel",
                    "mail2@mail.com", "pass", "919999929", "url", "url2", humorDto, "Portuguesa", "Porto", "Descrição Breve",
                    new byte[]{}, DateTime.Now),
                1,2,new List<TagRepoDto> {new(new RepoGuid(Guid.NewGuid()), "tag")});
           
        }

        [Fact]
        public async Task EditarRelacionamentoTest()
        {
            _moqJogadorRepository = new Mock<IJogadorRepository>();
            _moqRelacionamentoRepository = new Mock<IRelacionamentoRepository>();
            _moqTagRepository = new Mock<ITagRepository>();
            _moqUnitofWork = new Mock<IUnitOfWork>();
            _moqSocialNetwork = new Mock<SocialNetwork>();
            _moqJogadorRepository = new Mock<IJogadorRepository>();

            _moqRelacionamentoRepository
                .SetupSequence(x => x.getRelacionamentoAsync(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync((RelacionamentoRepositoryDto) null)
                .ReturnsAsync(_repositoryDto)
                .ReturnsAsync(_repositoryDto)
                .ReturnsAsync(_repositoryDto)
                .ReturnsAsync(_repositoryDto);

            _moqTagRepository.SetupSequence(x => x.GetByValuesAsync(It.IsAny<List<string>>()))
                .ReturnsAsync(new List<TagRepoDto> {new("tag1")})
                .ReturnsAsync(new List<TagRepoDto> {new("tag1")})
                .ReturnsAsync(new List<TagRepoDto> {new("tag1")})
                .ReturnsAsync(new List<TagRepoDto> {new("tag1")})
                .ReturnsAsync(new List<TagRepoDto> {new("tag1")});

            _moqTagRepository.Setup(x => x.AddRangeAsync(new List<TagRepoDto> {new("tag2")}))
                .Verifiable();

            _moqUnitofWork.Setup(x => x.CommitAsync())
                .ReturnsAsync(It.IsInRange<int>(1, 2, Range.Inclusive));

            _service = new RelacionamentoService(
                _moqUnitofWork.Object,
                _mapper,
                _moqRelacionamentoRepository.Object,
                _moqJogadorRepository.Object,
                _moqTagRepository.Object,
                _moqSocialNetwork.Object);


            //Não existe relacioanamento
            var returnNull = await _service.EditarRelacionamentoAsync(_moqRegistarJogadorDto);
            Assert.Null(returnNull);

            //Força ligação invalid
            _moqRegistarJogadorDto.Forca = 200;
            Func<Task<EditarRelacionamentoDto>> funcThow = () =>
                _service.EditarRelacionamentoAsync(_moqRegistarJogadorDto);
            var returnThrowExForcaLigacao = await Assert.ThrowsAsync<BusinessRuleValidationException>(funcThow);
            Assert.True(returnThrowExForcaLigacao.Message.Contains("força", StringComparison.CurrentCultureIgnoreCase));

            //Lista de tags vazia
            _moqRegistarJogadorDto.Forca = 2;
            _moqRegistarJogadorDto.LstTags = new List<string>();
            funcThow = () => _service.EditarRelacionamentoAsync(_moqRegistarJogadorDto);
            var returnThrowListaVazia = await Assert.ThrowsAsync<BusinessRuleValidationException>(funcThow);
            Assert.True(returnThrowListaVazia.Message.Contains("tag", StringComparison.CurrentCultureIgnoreCase));

            //Tag sem valor
            _moqRegistarJogadorDto.LstTags = new List<string> {"  ", "tag"};
            var returnThrowTagSemValor = await Assert.ThrowsAsync<BusinessRuleValidationException>(() =>
                _service.EditarRelacionamentoAsync(_moqRegistarJogadorDto));
            Assert.True(returnThrowTagSemValor.Message.Contains("tag", StringComparison.CurrentCultureIgnoreCase));

            //OK
            _moqRegistarJogadorDto.LstTags = new List<string> {"tag"};
            var ok = await _service.EditarRelacionamentoAsync(_moqRegistarJogadorDto);
            Assert.IsType<EditarRelacionamentoDto>(ok);
            Assert.NotNull(ok);
        }
    }
}