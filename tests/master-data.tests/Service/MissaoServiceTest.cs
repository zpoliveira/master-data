using System;
using System.Threading.Tasks;
using AutoFixture;
using master_data.domain.Shared;
using master_data.mappers.Dtos.Controller.Missao;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Interfaces;
using master_data.mappers.Mappers;
using master_data.mappers.Shared;
using master_data.repository.Interfaces;
using master_data.repository.Shared;
using master_data.service.Missao;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;
using Range = Moq.Range;

namespace master_data.tests.Service
{
    public class MissaoServiceTest
    {
        
        private readonly Fixture _fixture;
        private readonly RegistarMissaoDto _registarMissao;
        private readonly JogadorRepositoryDto _jogadorDesafiado;
        private readonly JogadorRepositoryDto _jogadorObjectivo;
        private readonly MissaoRepositoryDto _missaoRepository;
        private Mock<IJogadorRepository> _moqJogadorRepository;
        private Mock<IMissaoRepository> _moqMissaoRepository;
        private Mock<IUnitOfWork> _moqUnitofWork;
        private readonly IMissaoMapper _missaoMapper;
        private readonly IJogadorMapper _jogadorMapper;
        private readonly ITagMapper _tagMapper;
        private MissaoService _service;
        
        public MissaoServiceTest()
        {
            var services = new ServiceCollection();
            services.AddTransient<IMissaoMapper, MissaoMapper>();
            services.AddTransient<IJogadorMapper, JogadorMapper>();
            services.AddTransient<ITagMapper, TagMapper>();


            var serviceProvider = services.BuildServiceProvider();

            _missaoMapper = serviceProvider.GetService<IMissaoMapper>();
            _jogadorMapper = serviceProvider.GetService<IJogadorMapper>();
            _tagMapper = serviceProvider.GetService<ITagMapper>(); 


            _registarMissao = new RegistarMissaoDto()
            {
                JogadorDesafiado = "desafiado@mail.com",
                GrauDificuldade = 2,
                JogadorObjectivo = "objectivo@mail.com"
            };

            _jogadorDesafiado = new JogadorRepositoryDto(new RepoGuid(Guid.NewGuid()),
                "Desafiado", _registarMissao.JogadorDesafiado, "123*asd",
                "999999999", null, null, "FELIZ", null, null, 
                null, null, new DateTime(1990, 12, 1));
            
            _jogadorObjectivo = new JogadorRepositoryDto(new RepoGuid(Guid.NewGuid()),
                "Objectivo", _registarMissao.JogadorObjectivo, "123*asd",
                "999999999", null, null, "FELIZ", null, null, 
                null, null, new DateTime(1990, 12, 1));

            _missaoRepository = new MissaoRepositoryDto(new RepoGuid(It.IsNotNull<Guid>()),
                _jogadorDesafiado.Id, _jogadorObjectivo.Id, _registarMissao.GrauDificuldade,
                0, DateTime.Now, null);
        }

        [Fact]
        public async Task RegistarMissaoTest()
        {
            _moqMissaoRepository = new Mock<IMissaoRepository>();
            _moqJogadorRepository = new Mock<IJogadorRepository>();
            _moqUnitofWork = new Mock<IUnitOfWork>();

            _moqMissaoRepository
                .SetupSequence(x => x.AddAsync(_missaoRepository))
                .ReturnsAsync((MissaoRepositoryDto)null)
                .ReturnsAsync(_missaoRepository);

            _moqJogadorRepository.SetupSequence(x => x.GetUtilizadorByEmailAsync(_registarMissao.JogadorDesafiado))
                .ReturnsAsync((JogadorRepositoryDto)null)
                .ReturnsAsync(_jogadorDesafiado)
                .ReturnsAsync(_jogadorDesafiado);
            
            _moqJogadorRepository.SetupSequence(x => x.GetUtilizadorByEmailAsync(_registarMissao.JogadorObjectivo))
                .ReturnsAsync(_jogadorObjectivo)
                .ReturnsAsync(_jogadorObjectivo)
                .ReturnsAsync(_jogadorObjectivo);

            _moqUnitofWork.SetupSequence(x => x.CommitAsync())
                .ReturnsAsync(0)
                .ReturnsAsync(1);

            _service = new MissaoService(_moqJogadorRepository.Object,_moqMissaoRepository.Object,_moqUnitofWork.Object,
                                _missaoMapper,_jogadorMapper, _tagMapper);

            
            // Jogador null
            Func<Task<RegistarMissaoDto>> funcThow = () =>
                _service.RegistarMissao(_registarMissao);;
            var returnThrowJogadorNull = await Assert.ThrowsAsync<BusinessRuleValidationException>(funcThow);
            Assert.True(returnThrowJogadorNull.Message.Contains("jogador", StringComparison.CurrentCultureIgnoreCase));
            
            // not save
             funcThow = () =>
                _service.RegistarMissao(_registarMissao);;
            var returnThrowNotSave = await Assert.ThrowsAsync<ApplicationException>(funcThow);
            Assert.True(returnThrowNotSave.Message.Contains("gravar", StringComparison.CurrentCultureIgnoreCase));

            // ok
            var returnNotNull = await _service.RegistarMissao(_registarMissao);
            Assert.NotNull(returnNotNull);
            
        }
    }
}