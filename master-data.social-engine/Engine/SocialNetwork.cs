﻿using System;
using System.Collections.Generic;
using System.Linq;
using master_data.domain.Model.Entities;
using master_data.mappers.Interfaces;
using master_data.repository.Interfaces;
using master_data.social_engine.Structures;
using QuickGraph;
using QuickGraph.Algorithms;

namespace master_data.social_engine.Engine
{
    public class SocialNetwork
    {
        private AdjacencyGraph<JogadorNode, RelacionamentoEdge> _graph;
        
        private readonly IJogadorRepository _jogadorRepository;
        private readonly IJogadorMapper _jogadorMapper;
        private readonly IRelacionamentoRepository _relacionamentoRepository;
        private readonly IRelacionamentoMapper _relacionamentoMapper;

        private bool isInitialized;

        public SocialNetwork(IJogadorRepository jogadorRepository,
            IJogadorMapper jogadorMapper,
            IRelacionamentoRepository relacionamentoRepository,
            IRelacionamentoMapper relacionamentoMapper)
        {
            _jogadorRepository = jogadorRepository;
            _jogadorMapper = jogadorMapper;
            _relacionamentoRepository = relacionamentoRepository;
            _relacionamentoMapper = relacionamentoMapper;
        }

        public SocialNetwork(
            RelacionamentoEdge initRel,
            IJogadorRepository jogadorRepository,
            IJogadorMapper jogadorMapper,
            IRelacionamentoRepository relacionamentoRepository,
            IRelacionamentoMapper relacionamentoMapper)
        {
            _jogadorRepository = jogadorRepository;
            _relacionamentoRepository = relacionamentoRepository;
            _graph = new AdjacencyGraph<JogadorNode, RelacionamentoEdge>();

            _graph.AddVerticesAndEdge(initRel);
        }

        public void Initialize()
        {
            if (isInitialized) return;
            
            var jogadores = _jogadorRepository.GetAllAsync()
                .Result.Select(j => _jogadorMapper.FromRepositoryDto(j))
                .ToList();
            
            var relacionamentos = _relacionamentoRepository.GetAllAsync()
                .Result.Select(j => _relacionamentoMapper.toDomain(j))
                .ToList();

            addRangeRelacionamentos(relacionamentos, jogadores);
        }

        public bool addRangeRelacionamentos(IEnumerable<RelacionamentoEdge> edges)
        {
            var added = _graph.AddVerticesAndEdgeRange(edges);

            if (added <= 0)
            {
                Console.Out.WriteLine("Social Engine: FAILED to add connections to engine");
                return false;
            }

            Console.Out.WriteLine($"Social Engine: Added {added} connections to engine");

            return true;
        }

        public bool addRangeRelacionamentos(IEnumerable<Relacionamento> relacionamentos, IEnumerable<Jogador> jogadores)
        {
            var edges = relacionamentos.Select(r =>
            {
                var jogList = jogadores.ToList();

                return new RelacionamentoEdge(
                    jogList.Where(j => j.Id == r.JogadorDe)
                        .Select(jogador => new JogadorNode(jogador.Id, jogador.Nome.Value)).Single(),
                    jogList.Where(j => j.Id == r.JogadorPara)
                        .Select(jogador => new JogadorNode(jogador.Id, jogador.Nome.Value)).Single(),
                    r.ForcaLigacao.Value,
                    r.LstTag.Select(lt => lt.Value).ToList()
                );
            });

            return addRangeRelacionamentos(edges);
        }

        public IEnumerable<RelacionamentoEdge> getNodePrespective(JogadorNode node)
        {
            var res = _graph.TreeBreadthFirstSearch(node).Invoke(node, out var resList);
            return res ? resList : null;
        }
    }
}