﻿using System;
using System.Collections.Generic;
using System.Linq;
using master_data.domain.Model.Entities;
using master_data.mappers.GraphEntities;
using master_data.mappers.Interfaces;
using master_data.repository.Interfaces;
using QuikGraph;

namespace master_data.social_engine.Engine
{
    public class SocialNetwork
    {
        private AdjacencyGraph<JogadorNode, RelacionamentoEdge> _graph;

        private readonly IJogadorRepository _jogadorRepository;
        private readonly IJogadorMapper _jogadorMapper;
        private readonly IRelacionamentoRepository _relacionamentoRepository;
        private readonly IRelacionamentoMapper _relacionamentoMapper;

        private bool isInitialized;

        protected SocialNetwork()
        {
        }

        public SocialNetwork(IJogadorRepository jogadorRepository,
            IJogadorMapper jogadorMapper,
            IRelacionamentoRepository relacionamentoRepository,
            IRelacionamentoMapper relacionamentoMapper)
        {
            _jogadorRepository = jogadorRepository;
            _jogadorMapper = jogadorMapper;
            _relacionamentoRepository = relacionamentoRepository;
            _relacionamentoMapper = relacionamentoMapper;

            _graph = new AdjacencyGraph<JogadorNode, RelacionamentoEdge>();
        }

        public SocialNetwork(
            RelacionamentoEdge initRel,
            IJogadorRepository jogadorRepository,
            IJogadorMapper jogadorMapper,
            IRelacionamentoRepository relacionamentoRepository,
            IRelacionamentoMapper relacionamentoMapper)
        {
            _jogadorRepository = jogadorRepository;
            _jogadorMapper = jogadorMapper;
            _relacionamentoRepository = relacionamentoRepository;
            _relacionamentoMapper = relacionamentoMapper;

            _graph = new AdjacencyGraph<JogadorNode, RelacionamentoEdge>();
            _graph.AddVerticesAndEdge(initRel);
        }

        public void Initialize()
        {
            if (isInitialized) return;

            _graph = new AdjacencyGraph<JogadorNode, RelacionamentoEdge>();

            var jogadores = _jogadorRepository.GetAllAsync()
                .Result.Select(j => _jogadorMapper.FromRepositoryToDomain(j))
                .ToList();

            var relacionamentos = _relacionamentoRepository.GetAllAsync()
                .Result.Select(j => _relacionamentoMapper.toDomain(j))
                .ToList();

            addRangeRelacionamentos(relacionamentos, jogadores);
        }

        public bool addRangeRelacionamentos(IEnumerable<RelacionamentoEdge> edges)
        {
            var relacionamentoEdges = edges.ToList();
            var count = relacionamentoEdges.Count;

            switch (count)
            {
                case > 0:
                    _graph.AddVerticesAndEdgeRange(relacionamentoEdges);
                    break;
                case <= 0:
                    Console.Out.WriteLine("Social Engine: FAILED to add connections to engine");
                    return false;
            }

            Console.Out.WriteLine($"Social Engine: Added {count} connections to engine");

            return true;
        }

        public bool addRangeRelacionamentos(IEnumerable<Relacionamento> relacionamentos, IEnumerable<Jogador> jogadores)
        {
            var relacionamentosList = relacionamentos.ToList();
            var edges = relacionamentosList.Select(r =>
            {
                var jogList = jogadores.ToList();
                var relList = relacionamentosList.ToList();

                return new RelacionamentoEdge(
                    jogList.Where(j => j.Id == r.JogadorDe)
                        .Select(jogador => new JogadorNode(jogador.Id, jogador.Nome.Value, jogador.Email.Value, true))
                        .Single(),
                    jogList.Where(j => j.Id == r.JogadorPara)
                        .Select(jogador => new JogadorNode(jogador.Id, jogador.Nome.Value, jogador.Email.Value, false))
                        .Single(),
                    new RelacionamentoDetail
                    {
                        forcaLigacao = r.ForcaLigacao.Value,
                        forcaRelacao = r.ForcaRelacionamento.Value,
                        listTags = r.LstTag.Select(lt => lt.Value).ToList()
                    },
                    relList.Where(r2 => r2.JogadorDe == r.JogadorPara).Select(r2 => new RelacionamentoDetail
                    {
                        forcaLigacao = r2.ForcaLigacao.Value,
                        forcaRelacao = r2.ForcaRelacionamento.Value,
                        listTags = r2.LstTag.Select(lt => lt.Value).ToList()
                    }).SingleOrDefault()
                );
            });

            return addRangeRelacionamentos(edges);
        }

        public JogadorNode getNodePerspective(Guid jogadorId, int depth)
        {
            var node = _graph.Vertices.FirstOrDefault(v => v.IdJogador == jogadorId);

            return getNodePerspective(node, depth);
        }

        public JogadorNode getNodePerspective(JogadorNode node, int depth)
        {
            node.listLigacoes = new List<RelacionamentoEdge>();

            var nodeLigacoes = _graph.Edges
                .Where(e => Equals(e.Source, node))
                .ToList();

            var visited = new List<JogadorNode>();
            visited.Add(node);
            visited.AddRange(nodeLigacoes.Select(n => n.Target));

            node.listLigacoes = getNetworkUpToLevel(nodeLigacoes, ref visited, 0, depth).ToList();

            return node;
        }

        private IEnumerable<RelacionamentoEdge> getNetworkUpToLevel(
            IEnumerable<RelacionamentoEdge> edges,
            ref List<JogadorNode> visited,
            int levels,
            int maxDepth)
        {
            //Condicao de paragem
            if (levels >= maxDepth) return new List<RelacionamentoEdge>();

            var res = new List<RelacionamentoEdge>();

            foreach (var edge in edges)
            {
                var currentNode = edge.Target;

                var currentNodeEdges = _graph.Edges
                    .Where(e => Equals(e.Source, currentNode))
                    .ToList();

                if (!visited.Contains(currentNode))
                {
                    var currentNodeSubTree = getNetworkUpToLevel(currentNodeEdges, ref visited, levels + 1, maxDepth);

                    currentNode.listLigacoes = currentNodeSubTree.ToList();
                }

                res.Add(edge);
            }

            return res;
        }
    }
}