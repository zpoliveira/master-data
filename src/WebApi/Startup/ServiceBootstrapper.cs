﻿using master_data.mappers.Interfaces;
using master_data.mappers.Mappers;
using master_data.persistence.Introducao;
using master_data.persistence.Jogadores;
using master_data.persistence.Missao;
using master_data.persistence.Tags;
using master_data.repository.Interfaces;
using master_data.service.Introducao;
using master_data.service.Jogador;
using master_data.service.Missao;
using master_data.service.Relacionamento;
using master_data.social_engine.Engine;
using Microsoft.Extensions.DependencyInjection;
using master_data.persistence.EstadoHumor;

namespace masterdata.webapi
{
    public static class ServiceBootstrapper
    {
        internal static void ConfigureRepositories(IServiceCollection services)
        {
            services.AddScoped<IJogadorRepository, JogadorRepository>();
            services.AddScoped<IRelacionamentoRepository, RelacionamentoRepository>();
            services.AddScoped<ITagRepository, TagRepository>();
            services.AddScoped<IIntroducaoRepository, IntroducaoRepository>();
            services.AddScoped<IMissaoRepository,MissaoRepository>();
            
            services.AddScoped<IEstadoHumorRepository, EstadoHumorRepository>();
        }

        internal static void ConfigureMappers(IServiceCollection services)
        {
            services.AddSingleton<IJogadorMapper, JogadorMapper>();
            services.AddSingleton<IRelacionamentoMapper, RelacionamentoMapper>();
            services.AddSingleton<ITagMapper, TagMapper>();
            services.AddSingleton<IIntroducaoMapper, IntroducaoMapper>();
            services.AddSingleton<IHumorMapper, HumorMapper>();
            services.AddSingleton<IMissaoMapper,MissaoMapper>();
        }

        internal static void ConfigureApplicationServices(IServiceCollection services)
        {
            services.AddScoped<IJogadorService, JogadorService>();
            services.AddScoped<IIntroducaoService, IntroducaoService>();
            services.AddScoped<IRelacionamentoService, RelacionamentoService>();
            services.AddScoped<IMissaoService, MissaoService>();

            services.AddScoped<SocialNetwork>();
        }
    }
}