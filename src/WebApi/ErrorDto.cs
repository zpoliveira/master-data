﻿namespace masterdata.api
{
    public class ErrorDto
    {
        public ErrorDto(string message, string stackTrace)
        {
            Message = message;
            StackTrace = stackTrace;
        }

        public string Message { get; }
        public string StackTrace { get; }
    }
}