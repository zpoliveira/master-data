using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Controller.Relacionamento;
using master_data.mappers.Dtos.Controller.Shared;
using master_data.service.Jogador;
using master_data.service.Relacionamento;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace masterdata.api.controllers.Jogador
{
    [Route("/api/[controller]")]
    [ApiController]
    public class RelacionamentoController : ControllerBase
    {
        private IRelacionamentoService _service { get; }

        public RelacionamentoController(IRelacionamentoService service)
        {
            _service = service;
        }

        [HttpGet("niveis")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JogadorComLigacoesDiretasDto))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> VerRedeJogadorEmNiveis([FromQuery] int niveis, [FromQuery] string emailJogador)
        {
            if (niveis <= 0 || string.IsNullOrWhiteSpace(emailJogador)) return BadRequest("Dados errados");
            try
            {
                var dtoRes = await _service.verRedeLigacaoEmNiveis(niveis, emailJogador);

                if (dtoRes is not null)
                {
                    return Ok(dtoRes);
                }

                return BadRequest("Erro a encontrar rede");
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }


        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EditarRelacionamentoDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErrorDto))]
        public async Task<IActionResult> EditarRelacionamento(
            [FromBody] EditarRelacionamentoDto dto)
        {
            
            try
            {
                var result = await _service.EditarRelacionamentoAsync(dto);
                if (result is null) return NotFound();

                return Ok(result);
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<RelacionamentoDto>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> GetRelacionamentos([FromQuery] PaginaRelacionamentoDto pagina)
        {
           
            try
            {
                var result = await _service.GetRelacionamentosAsync(pagina);
                
                var metadata = new
                {
                    result.ContagemTotal,
                    result.TamanhoPagina,
                    result.NumeroPagina,
                    result.PaginasTotal,
                    result.TemProxima,
                    result.TemAnterior
                };
                Response.Headers.Add("x-paginacao", JsonConvert.SerializeObject(metadata,new JsonSerializerSettings 
                { 
                    ContractResolver = new CamelCasePropertyNamesContractResolver() 
                }));
                return Ok(result);

            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }
    }
}