﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Repository;
using master_data.service.Jogador;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace masterdata.api.controllers.Jogador
{
    [Route("/api/[controller]")]
    [ApiController]
    public class JogadorController : ControllerBase
    {
        public JogadorController(IJogadorService jogadorService)
        {
            _jogadorService = jogadorService;
        }

        private readonly IJogadorService _jogadorService;

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(RegistarJogadorDto))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> RegistarNovoUtilizador(RegistarJogadorDto dto)
        {
            try
            {
                var result = await _jogadorService.criarNovoJogadorAsync(dto);

                return new CreatedResult(nameof(RegistarJogadorDto), result);
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }

        [HttpPut("{email}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RegistarJogadorDto))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> UpdateUtilizador(string email, UpdateJogadorDto dto)
        {
            if (email != dto.email)
            {
                var error = new ErrorDto("Wrong email", "UpdateUtilizador");
                return BadRequest(error);
            }

            await _jogadorService.updateJogadorAsync(dto);
            return new OkResult();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RegistarJogadorDto))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<ActionResult<IEnumerable<RegistarJogadorDto>>> GetAll()
        {
            try
            {
                return await _jogadorService.GetAllAsync();
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }

        [HttpPatch("humor/{email}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RegistarJogadorDto))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> EditarEstadoHumor(string email, EditarHumorDto dto)
        {
            try
            {
                if (email != dto.email)
                {
                    var error = new ErrorDto("Email errado", "Editar Estado Humor");
                    return BadRequest(error);
                }

                var result = await _jogadorService.editarEstadoHumorAsync(dto);

                return new CreatedResult(nameof(RegistarJogadorDto), result);
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }


        [HttpGet("/api/[controller]/pesquisarJogadores")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RegistarJogadorDto))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<ActionResult<IEnumerable<RegistarJogadorDto>>> PesquisarJogadoresAsync(
            [FromQuery] PesquisarJogadorDto query)
        {
            try
            {
                return await _jogadorService.pesquisarJogadoresAsync(query);
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public void EliminarJogadorByEmailAsync(string email)
        {
            _jogadorService.deleteJogadorByEmail(email);
        }


        [HttpGet("estadosHumor")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<ActionResult<IEnumerable<HumorControllerDto>>> ObterEstadosHumorAsync()
        {
            try
            {
                return await _jogadorService.obterEstadosHumorAsync();
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }

        

        [HttpGet("Sugestoes")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<JogadorSugestaoDto>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> JogadoresSugestaoAsync([FromQuery] PaginaJogadorSugestaoDto pagina)
        {
            try
            {
                var result = await _jogadorService.GetSugestoesJogador(pagina);
                var metadata = new
                {
                    result.ContagemTotal,
                    result.TamanhoPagina,
                    result.NumeroPagina,
                    result.PaginasTotal,
                    result.TemProxima,
                    result.TemAnterior
                };
                Response.Headers.Add("x-paginacao", JsonConvert.SerializeObject(metadata,new JsonSerializerSettings 
                { 
                    ContractResolver = new CamelCasePropertyNamesContractResolver() 
                }));
                return Ok(result);
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
           
        }
    }
}