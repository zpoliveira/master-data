using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Introducao;
using master_data.mappers.Dtos.Repository;
using master_data.service.Introducao;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace masterdata.api.controllers.Introducao
{
    [Route("/api/[controller]")]
    [ApiController]
    public class IntroducaoController : ControllerBase
    {
        public IntroducaoController(IIntroducaoService introducaoService)
        {
            _introducaoService = introducaoService;
        }

        private readonly IIntroducaoService _introducaoService;

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<LigacaoPendenteDto>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> ObterListaPedidosLigacaoPendentesAsync(
            [FromQuery] ObterListaPedidosLigacaoPendentesDto dto)
        {
            try
            {
                var result = await _introducaoService.obterListaPedidosLigacaoPendentesAsync(dto);

                return Ok(result);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }

        [HttpPost("pedidoLigacao")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> CriarPedidodeLigacaoAsync([FromBody] PedidoLigacaoDto dto)
        {
            try
            {
                await _introducaoService.criaPedidoLigacaoAsync(dto);
                return Accepted();
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }


        [HttpPost("pedidoIntroducao")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> CriarPedidodeIntroducaoAsync([FromBody] PedidoIntroducaoDto dto)
        {
            try
            {
                await _introducaoService.criarPedidoIntroducaoAsync(dto);
                return Accepted();
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }


        [HttpPut("aceitar")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> AceitarPedidoIntroducao([FromBody] AceitarIntroducaoDto dto)
        {
            try
            {
                var result = await _introducaoService.aceitarPedidoIntroducaoAsync(dto);

                if (result) return Accepted();

                return NotFound(dto);
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }
    }
}