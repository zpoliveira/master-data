using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Controller.Missao;
using master_data.mappers.Dtos.Controller.Relacionamento;
using master_data.service.Missao;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace masterdata.api.Controllers.Missao
{
    [Route("/api/[controller]")]
    [ApiController]
    public class MissaoController : ControllerBase
    {
        
        private IMissaoService _service { get; }

        public MissaoController(IMissaoService service)
        {
            _service = service;
        }
        
        
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(RegistarJogadorDto))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> RegistarMissao(RegistarMissaoDto dto)
        {
            try
            {
                var result = await _service.RegistarMissao(dto);

                return new CreatedResult(nameof(RegistarMissaoDto), result);
            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<MissaoDto>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDto))]
        public async Task<IActionResult> GetMissoes([FromQuery] PaginaMissaoDto pagina)
        {
            try
            {
                var result = await _service.GetMissoesAsync(pagina);
                
                var metadata = new
                {
                    result.ContagemTotal,
                    result.TamanhoPagina,
                    result.NumeroPagina,
                    result.PaginasTotal,
                    result.TemProxima,
                    result.TemAnterior
                };
                Response.Headers.Add("x-paginacao", JsonConvert.SerializeObject(metadata,new JsonSerializerSettings 
                { 
                    ContractResolver = new CamelCasePropertyNamesContractResolver() 
                }));
                return Ok(result);

            }
            catch (Exception e)
            {
                var error = new ErrorDto($"{e.Message} \n {e.InnerException}", e.StackTrace);
                return BadRequest(error);
            }
        }
        
    }
}