﻿using System;
using System.Collections.Generic;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;

namespace master_data.domain.Model.Entities
{
    public class Jogador : IEntity, IAggregateRoot
    {
        protected Jogador()
        {
        }

        public Jogador(
            Guid id,
            Nome nome,
            Email email,
            Password password,
            Humor humor,
            Url urlFacebook,
            Url urlLinkedIn,
            Telefone telefone, 
            DataNascimento dataNascimento,
            Nacionalidade nacionalidade,
            CidadeResidencia cidadeResidencia,
            DescricaoBreve descricaoBreve,
            Avatar avatar)
        {
            Id = id;
            Nome = nome;
            Email = email;
            Password = password;
            Humor = humor;
            UrlFacebook = urlFacebook;
            UrlLinkedIn = urlLinkedIn;
            Telefone = telefone;
            DataNascimento = dataNascimento;
            Nacionalidade = nacionalidade;
            CidadeResidencia = cidadeResidencia;
            DescricaoBreve = descricaoBreve;
            Avatar = avatar;

            Tags = new List<Tag>();
        }

        public Jogador(
            Nome nome,
            Email email,
            Password password,
            Url urlFacebook,
            Url urlLinkedIn,
            Telefone telefone, 
            DataNascimento dataNascimento,
            Nacionalidade nacionalidade,
            CidadeResidencia cidadeResidencia,
            DescricaoBreve descricaoBreve,
            Avatar avatar)
        {
            Id = Guid.NewGuid();
            Nome = nome;
            Email = email;
            Password = password;
            Humor = new Humor(EstadoHumor.NEUTRO);
            UrlFacebook = urlFacebook;
            UrlLinkedIn = urlLinkedIn;
            Telefone = telefone;
            DataNascimento = dataNascimento;
            Nacionalidade = nacionalidade;
            CidadeResidencia = cidadeResidencia;
            DescricaoBreve = descricaoBreve;
            Avatar = avatar;

            Tags = new List<Tag>();
        }

        public Jogador(Nome nome,
            Email email,
            Password password,
            Humor humor,
            Url urlFacebook,
            Url urlLinkedIn,
            Telefone telefone,
            DataNascimento dataNascimento,
            Nacionalidade nacionalidade,
            CidadeResidencia cidadeResidencia,
            DescricaoBreve descricaoBreve,
            Avatar avatar,
            ICollection<Tag> tags)
        {
            Nome = nome;
            Email = email;
            Password = password;
            Humor = humor;
            UrlFacebook = urlFacebook;
            UrlLinkedIn = urlLinkedIn;
            Telefone = telefone;
            DataNascimento = dataNascimento;
            Nacionalidade = nacionalidade;
            CidadeResidencia = cidadeResidencia;
            DescricaoBreve = descricaoBreve;
            Avatar = avatar;
            Tags = tags;
        }

        public Guid Id { get; }

        public Jogador(
            Nome nome,
            Email email,
            Password password,
            Humor humor,
            Url urlFacebook,
            Url urlLinkedIn,
            Telefone telefone,
            DataNascimento dataNascimento,
            Nacionalidade nacionalidade,
            CidadeResidencia cidadeResidencia,
            DescricaoBreve descricaoBreve,
            Avatar avatar)
        {
            Id = Guid.NewGuid();
            Nome = nome;
            Email = email;
            Password = password;
            Humor = humor;
            UrlFacebook = urlFacebook;
            UrlLinkedIn = urlLinkedIn;
            Telefone = telefone;
            DataNascimento = dataNascimento;
            Nacionalidade = nacionalidade;
            CidadeResidencia = cidadeResidencia;
            DescricaoBreve = descricaoBreve;
            Avatar = avatar;
        }

        public Nome Nome { get; }

        public Email Email { get; }

        public DataNascimento DataNascimento { get; }

        public Password Password { get; }

        public Humor Humor { get; }

        public Url UrlFacebook { get; }

        public Url UrlLinkedIn { get; }

        public Telefone Telefone { get; }
        public Nacionalidade Nacionalidade { get; }
        public CidadeResidencia CidadeResidencia { get; }
        public DescricaoBreve DescricaoBreve { get; set; }
        public Avatar Avatar { get; set; }
        public ICollection<Tag> Tags { get; }
        public ICollection<Introducao> Introducoes { get; set; }

        public void addTag(Tag tag)
        {
            if (!Tags.Contains(tag))
                Tags.Add(tag);
        }

        public void addIntroducao(Introducao introducao)
        {
            if (!Introducoes.Contains(introducao))
                Introducoes.Add(introducao);
        }

        protected bool Equals(Jogador other)
        {
            return Equals(Nome,
                       other.Nome) &&
                   Equals(Email,
                       other.Email) &&
                   Equals(DataNascimento,
                       other.DataNascimento) &&
                   Equals(Password,
                       other.Password) &&
                   Equals(Humor,
                       other.Humor) &&
                   Equals(UrlFacebook,
                       other.UrlFacebook) &&
                   Equals(UrlLinkedIn,
                       other.UrlLinkedIn) &&
                   Equals(Telefone,
                       other.Telefone);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Jogador) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Nome, Email, DataNascimento, Password, Humor, UrlFacebook, UrlLinkedIn, Telefone);
        }
    }
}