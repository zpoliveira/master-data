using System;
using System.Collections.Generic;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;

namespace master_data.domain.Model.Entities
{
    public class Introducao : IEntity, IAggregateRoot
    {
        protected Introducao()
        {
        }

        public Introducao(
            Jogador jogadorRequisitante,
            Jogador jogadorIntrodutor,
            Jogador jogadorObjectivo,
            ForcaLigacao forcaLigacao,
            bool hasIntrodutor)
        {
            Id = Guid.NewGuid();
            this.jogadorRequisitante = jogadorRequisitante;
            this.jogadorIntrodutor = hasIntrodutor ? jogadorIntrodutor : null;
            this.jogadorObjectivo = jogadorObjectivo;
            ForcaLigacao = forcaLigacao;
            estadoIntroducaoIntrodutor = hasIntrodutor ? new Estado(EstadoEnum.PENDENTE) : null;
            estadoIntroducaoObjectivo = new Estado(EstadoEnum.PENDENTE);
        }

        public Guid Id { get; }

        public Jogador jogadorRequisitante { get; }

        public Jogador jogadorIntrodutor { get; }

        public Jogador jogadorObjectivo { get; }

        public ForcaLigacao ForcaLigacao { get; }

        public Estado estadoIntroducaoIntrodutor { get; }

        public Estado estadoIntroducaoObjectivo { get; }

        private sealed class IntroducaoEqualityComparer : IEqualityComparer<Introducao>
        {
            public bool Equals(Introducao x, Introducao y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.Id.Equals(y.Id) && Equals(x.jogadorRequisitante, y.jogadorRequisitante) &&
                       Equals(x.jogadorIntrodutor, y.jogadorIntrodutor) &&
                       Equals(x.jogadorObjectivo, y.jogadorObjectivo);
            }

            public int GetHashCode(Introducao obj)
            {
                return HashCode.Combine(obj.Id, obj.jogadorRequisitante, obj.jogadorIntrodutor, obj.jogadorObjectivo);
            }
        }

        public static IEqualityComparer<Introducao> IntroducaoComparer { get; } = new IntroducaoEqualityComparer();
    }
}