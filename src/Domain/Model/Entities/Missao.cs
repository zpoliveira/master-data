using System;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;

namespace master_data.domain.Model.Entities
{
    public class Missao : IEntity, IAggregateRoot
    {
        public Missao(Jogador jogadorDesafiado, Jogador jogadorObjectivo, GrauDificuldade grauDificuldade, 
             DataHoraInicio dataHoraInicio): this(Guid.NewGuid(), 
            jogadorDesafiado, jogadorObjectivo, grauDificuldade, new Pontos(0), dataHoraInicio,
            null)
        {
        }

        public Missao(Guid id, Jogador jogadorDesafiado, Jogador jogadorObjectivo, GrauDificuldade grauDificuldade, Pontos pontos, DataHoraInicio dataHoraInicio, DataHoraFim dataHoraFim)
        {
            validaGuidEmpty(id, "Id da missão não pode ser vazio.");
            Id = id;
            validaJogadorNull(jogadorDesafiado,"Jogador desafiado não pode ser nullo.");
            JogadorDesafiado = jogadorDesafiado;
            validaJogadorNull(jogadorObjectivo,"Jogador obectivo não pode ser nullo.");
            JogadorObjectivo = jogadorObjectivo;

            validaJogadoresIgual(jogadorDesafiado, jogadorObjectivo, "Jogador desafiado e objectivo são o mesmo.");
            
            validaGrauDificuldade(grauDificuldade, "Grau de difículdade tem de ser superior a 0.");
            GrauDificuldade = grauDificuldade;
            
            Pontos = pontos;
            validaDataHoraInicioFim(dataHoraInicio, dataHoraFim);
            DataHoraInicio = dataHoraInicio;
            DataHoraFim = dataHoraFim;
        }
        
        public Guid Id { get; }
        
        public Jogador JogadorDesafiado { get; private set; }

        public Jogador JogadorObjectivo { get; private set; }

        public GrauDificuldade GrauDificuldade { get; private set; }

        public Pontos Pontos { get; private set; }

        public DataHoraInicio DataHoraInicio { get; private set; }

        public DataHoraFim DataHoraFim { get; private set; }


        protected bool Equals(Missao other)
        {
            return Id.Equals(other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Missao) obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
        
        private void validaGuidEmpty(Guid guid, string msg)
        {
            if (Guid.Empty == guid) throw new BusinessRuleValidationException(msg);
        }

        private void validaJogadorNull(Jogador jogador, string msg)
        {
            if (jogador is null) throw new BusinessRuleValidationException(msg);
        }
        
        private void validaGrauDificuldade(GrauDificuldade grauDificuldade, string msg)
        {
            if(grauDificuldade is null) throw new BusinessRuleValidationException(msg);
        }
        
        private void validaJogadoresIgual(Jogador jogadorDesafiado, Jogador jogadorObjectivo, string msg)
        {
            if (jogadorDesafiado.Equals(jogadorObjectivo))
                throw new BusinessRuleValidationException(msg);
        }

        private void validaDataHoraInicioFim(DataHoraInicio dataHoraInicio, DataHoraFim dataHoraFim)
        {
            if (dataHoraInicio is null)
                throw new BusinessRuleValidationException("Data/Hora de início tem de ter valor.");
            else if(dataHoraFim is not null)
                if (dataHoraFim.value < dataHoraInicio.value)
                    throw new BusinessRuleValidationException("Data/Hora de fim tem de ser superior à data/hora de início.");
        }
    }
}