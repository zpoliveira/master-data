using System;
using master_data.domain.Shared;

namespace master_data.domain.Model.Entities
{
    public class Tag : IEntity, IAggregateRoot
    {
        public Guid Id { get; }
        
        public string Value { get; private set; }


        public Tag(string value)
        {
            Id = Guid.NewGuid();
            if (string.IsNullOrWhiteSpace(value))
                throw new BusinessRuleValidationException("Não é possível inserir tags sem valor.");
            Value = value;
        }

        public Tag(Guid id, string value)
        {
            Id = id;
            if (string.IsNullOrWhiteSpace(value))
                throw new BusinessRuleValidationException("Não é possível inserir tags sem valor.");
            Value = value;
        }


        protected bool Equals(Tag other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Tag) obj);
        }

        public override int GetHashCode()
        {
            return Value != null ? Value.GetHashCode() : 0;
        }
    }
}