using System;
using System.Collections.Generic;
using System.Linq;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;

namespace master_data.domain.Model.Entities
{
    public class Relacionamento : IEntity
    {
        public Relacionamento(Guid jogadorDe, Guid jogadorPara, ForcaLigacao forcaLigacao,
            ForcaRelacionamento forcaRelacionamento, ICollection<Tag> lstTag)
        {
            Id = Guid.NewGuid();
            JogadorDe = jogadorDe;
            JogadorPara = jogadorPara;
            ForcaLigacao = forcaLigacao;
            ForcaRelacionamento = forcaRelacionamento;
            LstTag = lstTag;
        }

        public Relacionamento(Guid id, Guid jogadorDe, Guid jogadorPara, ForcaLigacao forcaLigacao,
            ForcaRelacionamento forcaRelacionamento, ICollection<Tag> lstTags)
        {
            validaGuid(id, "Id da relação não pode ser vazio.");
            Id = id;
            validaGuid(jogadorDe, "Sem jogador [de] relação.");
            JogadorDe = jogadorDe;
            validaGuid(jogadorPara, "Sem jogador [para] relação.");
            JogadorPara = jogadorPara;
            ForcaLigacao = forcaLigacao;
            ForcaRelacionamento = forcaRelacionamento;
            LstTag = lstTags;
        }

        public Guid Id { get; private set; }

        public Guid JogadorDe { get; private set; }

        public Guid JogadorPara { get; private set; }

        public ForcaLigacao ForcaLigacao { get; private set; }

        public ForcaRelacionamento ForcaRelacionamento { get; private set; }

        private ICollection<Tag> _lstTags;

        public ICollection<Tag> LstTag
        {
            get { return _lstTags.Select(x => new Tag(x.Value)).ToList(); }
            private set
            {
                validaListatag(value, "Sem Tags para a relação.");
                _lstTags = value.Select(x => new Tag(x.Value)).ToList();
            }
        }

        public void AlterarForcaLigacao(int forca)
        {
            ForcaLigacao = new ForcaLigacao(forca);
        }

        public void AlterarListaTags(ICollection<string> lst)
        {
            if (lst is null)
                lst = new List<string>();
            var newLst = new List<Tag>();
            foreach (var item in lst)
            {
                var nova = new Tag(item);
                newLst.Add(nova);
            }

            LstTag = newLst;
        }


        private void validaGuid(Guid guid, string msg)
        {
            if (Guid.Empty == guid) throw new BusinessRuleValidationException(msg);
        }

        private void validaListatag(ICollection<Tag> lst, string msg)
        {
            if (lst is null || !lst.Any()) throw new BusinessRuleValidationException(msg);
        }

        protected bool Equals(Relacionamento other)
        {
            return JogadorDe.Equals(other.JogadorDe) && JogadorPara.Equals(other.JogadorPara);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Relacionamento) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(JogadorDe, JogadorPara);
        }
    }
}