﻿namespace master_data.domain.Model.ValueObjects
{
    public class CidadeResidencia
    {
        protected CidadeResidencia()
        {
        }

        public CidadeResidencia(string value)
        {
            Value = value;
        }

        public string Value { get; set; }

        protected bool Equals(CidadeResidencia other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((CidadeResidencia) obj);
        }

        public override int GetHashCode()
        {
            return Value != null ? Value.GetHashCode() : 0;
        }
    }
}