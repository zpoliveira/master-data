using System.Collections.Generic;
using master_data.domain.Shared;

namespace master_data.domain.Model.ValueObjects
{
    public class DescricaoBreve: IValueObject
    {
        public DescricaoBreve(string descricaoBreve)
        {
            Value = descricaoBreve;
        }

        public string Value { get; }

        protected bool Equals(DescricaoBreve other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DescricaoBreve) obj);
        }

        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }

        private sealed class ValueEqualityComparer : IEqualityComparer<DescricaoBreve>
        {
            public bool Equals(DescricaoBreve x, DescricaoBreve y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.Value == y.Value;
            }

            public int GetHashCode(DescricaoBreve obj)
            {
                return (obj.Value != null ? obj.Value.GetHashCode() : 0);
            }
        }

        public static IEqualityComparer<DescricaoBreve> ValueComparer { get; } = new ValueEqualityComparer();
    }
}