﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using master_data.domain.Shared;


namespace master_data.domain.Model.ValueObjects
{
    [ComplexType]
    public class Humor : IEntity, IValueObject
    {
        public Humor(EstadoHumor value)
        {
            Id = Guid.NewGuid();
            Value = value;
        }

        public Humor(Guid id, EstadoHumor value)
        {
            Id = id;
            Value = value;
        }

        public EstadoHumor Value { get; }
        public Guid Id { get; }

        public override string ToString()
        {
            return Value.ToString();
        }

        protected bool Equals(Humor other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Humor) obj);
        }

        public override int GetHashCode()
        {
            return (int) Value;
        }
    }
}