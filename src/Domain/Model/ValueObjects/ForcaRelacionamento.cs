using System;
using System.ComponentModel.DataAnnotations.Schema;
using master_data.domain.Shared;

namespace master_data.domain.Model.ValueObjects
{
    [ComplexType]
    public class ForcaRelacionamento : IValueObject
    {
        public ForcaRelacionamento(decimal value)
        {
            Value = value;
        }

        public decimal Value { get; private set; }


        protected bool Equals(ForcaRelacionamento other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ForcaRelacionamento) obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}