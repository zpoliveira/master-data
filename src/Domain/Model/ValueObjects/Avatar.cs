using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace master_data.domain.Model.ValueObjects
{
    public class Avatar
    {
        public Avatar(byte[] avatar)
        {
            Value = avatar;
        }
        
        public byte[] Value { get; }

        protected bool Equals(Avatar other)
        {
            return Equals(Value, other.Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Avatar) obj);
        }

        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }

        private sealed class ValueEqualityComparer : IEqualityComparer<Avatar>
        {
            public bool Equals(Avatar x, Avatar y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return Equals(x.Value, y.Value);
            }

            public int GetHashCode(Avatar obj)
            {
                return (obj.Value != null ? obj.Value.GetHashCode() : 0);
            }
        }

        public static IEqualityComparer<Avatar> ValueComparer { get; } = new ValueEqualityComparer();

        public override string ToString()
        {
            return Value is null ? null: Convert.ToBase64String(Value);
        }
    }
}