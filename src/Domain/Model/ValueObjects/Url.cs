﻿using System.ComponentModel.DataAnnotations.Schema;
using master_data.domain.Shared;

namespace master_data.domain.Model.ValueObjects
{
    [ComplexType]
    public class Url : IValueObject
    {
        public Url(string value)
        {
            Value = value;
        }

        public string Value { get; }

        protected bool Equals(Url other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Url) obj);
        }

        public override int GetHashCode()
        {
            return Value != null ? Value.GetHashCode() : 0;
        }
    }
}