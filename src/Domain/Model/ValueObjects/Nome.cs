﻿using System.ComponentModel.DataAnnotations.Schema;
using master_data.domain.Shared;

namespace master_data.domain.Model.ValueObjects
{
    [ComplexType]
    public class Nome : IValueObject
    {
        public Nome(string nome)
        {
            Value = nome;
        }

        public string Value { get; }

        protected bool Equals(Nome other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Nome) obj);
        }

        public override int GetHashCode()
        {
            return Value != null ? Value.GetHashCode() : 0;
        }
    }
}