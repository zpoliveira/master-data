using System;

namespace master_data.domain.Model.ValueObjects
{
    public class DataHoraInicio
    {
        public DataHoraInicio(DateTime value)
        {
            this.value = value;
        }

        public DateTime value { get; private set; }


        protected bool Equals(DataHoraInicio other)
        {
            return value.Equals(other.value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DataHoraInicio) obj);
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }
    }
}