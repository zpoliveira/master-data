using master_data.domain.Shared;

namespace master_data.domain.Model.ValueObjects
{
    public class GrauDificuldade
    {
        public GrauDificuldade(int value)
        {
            validaMaiorZero(value, "Grau de dificuldade tem de ser superior a 0.");
            this.value = value;
        }

        public int value { get; private set; }

        protected bool Equals(GrauDificuldade other)
        {
            return value == other.value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((GrauDificuldade) obj);
        }

        public override int GetHashCode()
        {
            return value;
        }

        private void validaMaiorZero(int value, string msg)
        {
            if (value <= 0) throw new BusinessRuleValidationException(msg);
        }
    }
}