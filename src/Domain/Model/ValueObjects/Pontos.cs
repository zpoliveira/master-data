using System;
using System.ComponentModel.DataAnnotations.Schema;
using master_data.domain.Shared;

namespace master_data.domain.Model.ValueObjects
{
    [ComplexType]
    public class Pontos : IValueObject
    {
        public Pontos(int value)
        {
            validaMaiorIgualZero(value, "Pontos têm de ter um valor igual ou superio a 0.");
            Value = value;
        }

        private void validaMaiorIgualZero(int value, string msg)
        {
            if (value < 0) throw new BusinessRuleValidationException(msg);
        }

        public int Value { get; }

        protected bool Equals(Pontos other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Pontos) obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}