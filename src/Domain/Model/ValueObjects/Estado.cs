using System.ComponentModel.DataAnnotations.Schema;
using master_data.domain.Shared;

namespace master_data.domain.Model.ValueObjects
{
    [ComplexType]
    public class Estado : IValueObject
    {
        public Estado(EstadoEnum value)
        {
            Value = value;
        }

        public EstadoEnum Value { get; }

        public override string ToString()
        {
            return Value.ToString();
        }

        protected bool Equals(Estado other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Estado) obj);
        }

        public override int GetHashCode()
        {
            return (int) Value;
        }
    }
}