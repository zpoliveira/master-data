using System.ComponentModel.DataAnnotations.Schema;
using master_data.domain.Shared;

namespace master_data.domain.Model.ValueObjects
{
    [ComplexType]
    public class ForcaLigacao : IValueObject
    {
        public ForcaLigacao(int value)
        {
            if (value < 1 || value > 100)
                throw new BusinessRuleValidationException(
                    "A Força da ligação tem de ter valores compreendidos entre [1,100].");
            Value = value;
        }

        public int Value { get; private set; }


        protected bool Equals(ForcaLigacao other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ForcaLigacao) obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}