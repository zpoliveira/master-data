﻿using System;
using master_data.domain.Shared;

namespace master_data.domain.Model.ValueObjects
{
    public class DataNascimento : IValueObject
    {
        private readonly DateTime _value;

        public DataNascimento(DateTime value)
        {
            _value = value;
        }

        public DataNascimento(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException(value, "Date time string on Jogador creation is null");

            if (!DateTime.TryParse(value, out _value))
                throw new FormatException("Date time string on Jogador creation is bad format");
        }

        public DateTime Value => _value;

        protected bool Equals(DataNascimento other)
        {
            return _value.Equals(other._value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((DataNascimento) obj);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}