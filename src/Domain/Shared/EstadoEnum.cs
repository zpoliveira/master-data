namespace master_data.domain.Shared
{
    public enum EstadoEnum
    {
        PENDENTE,
        ACEITE,
        REJEITADO
    }
}