using System;

namespace master_data.domain.Shared
{
    public class BusinessRuleValidationException : Exception
    {
        public BusinessRuleValidationException(string message) : base(message)
        {
        }

        public BusinessRuleValidationException(string message, string details) : base(message)
        {
            Details = details;
        }

        public BusinessRuleValidationException() : base()
        {
        }

        public string Details { get; }
    }
}