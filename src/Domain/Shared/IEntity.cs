﻿using System;

namespace master_data.domain.Shared
{
    public interface IEntity
    {
        public Guid Id { get; }
    }
}