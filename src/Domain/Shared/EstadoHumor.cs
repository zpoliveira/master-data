﻿namespace master_data.domain.Shared
{
    public enum EstadoHumor
    {
        FELIZ,
        TRISTE,
        NEUTRO
    }
}