﻿using master_data.domain.Model.Entities;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Interfaces;

namespace master_data.mappers.Mappers
{
    public class TagMapper : ITagMapper
    {
        public Tag FromRepositoryDto(TagRepoDto tagRepoDto)
        {
            return new Tag(tagRepoDto.Id.AsGuid(),tagRepoDto.Value);
        }

        public TagRepoDto ToRepositoryDto(Tag tag)
        {
            return new TagRepoDto(tag.Value);
        }
    }
}