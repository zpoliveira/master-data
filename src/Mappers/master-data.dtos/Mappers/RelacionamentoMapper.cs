using System;
using System.Collections.Generic;
using System.Linq;
using master_data.domain.Model.Entities;
using master_data.domain.Model.ValueObjects;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Controller.Relacionamento;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.GraphEntities;
using master_data.mappers.Interfaces;

namespace master_data.mappers.Mappers
{
    public class RelacionamentoMapper : IRelacionamentoMapper
    {
        
        public Relacionamento toDomain(RelacionamentoRepositoryDto dto)
        {
            if (dto is null)
                return null;
            return new Relacionamento(dto.Id.AsGuid(), dto.JogadorDe.Id.AsGuid(), dto.JogadorPara.Id.AsGuid(),
                new ForcaLigacao(dto.ForcaLigacao),
                new ForcaRelacionamento(dto.ForcaRelacao), dto.TagList.Select(z => new Tag(z.Value)).ToList());
        }

        public EditarRelacionamentoDto FromRepositoryToEditarRelacionamentoDto(RelacionamentoRepositoryDto dto)
        {
            if (dto is null)
                return null;
            return new EditarRelacionamentoDto(dto.JogadorDe.Email, dto.JogadorPara.Email, dto.ForcaLigacao,
                dto.TagList.Select(x => x.Value).ToList());
        }

        public RelacionamentoDto FromRepositoryToRelacionamentoDto(RelacionamentoRepositoryDto dto)
        {
            if (dto is null)
            {
                return null;
            }

            return new RelacionamentoDto()
            {
                Avatar = dto.JogadorPara.Avatar is null?null: Convert.ToBase64String(dto.JogadorPara.Avatar, 0, dto.JogadorPara.Avatar.Length),
                EmailPara = dto.JogadorPara.Email,
                EmailDe = dto.JogadorDe.Email,
                Nome = dto.JogadorPara.Nome,
                Tags = dto.TagList.Select(x => x.Value).ToList(),
                ForcaLigacao = dto.ForcaLigacao,
                ForcaRelacao = dto.ForcaRelacao
            };
        }

        public PaginacaoDto<RelacionamentoDto> FromRepositoryToRelacionamentoDto(PaginacaoDto<RelacionamentoRepositoryDto> dto)
        {
            List<RelacionamentoDto> lst = dto.Select(x => FromRepositoryToRelacionamentoDto(x)).ToList();
            return new PaginacaoDto<RelacionamentoDto>(lst, dto.ContagemTotal, dto.NumeroPagina, dto.TamanhoPagina);
        }

        public JogadorComLigacoesDiretasDto FromSocialNetworkJogadorNodeToJogadorComLigacoesDiretasDto(
            JogadorNode jogadorNode)
        {
            var res = new JogadorComLigacoesDiretasDto
            {
                Jogador = new SimpleJogador {nome = jogadorNode.Nome, email = jogadorNode.Email},
                listLigacoes = new List<JogadorComLigacoesDiretasDto>()
            };

            if (jogadorNode.listLigacoes is null) return res;
            
            foreach (var edge in jogadorNode.listLigacoes)
            {
                var edgeTarget = FromSocialNetworkJogadorNodeToJogadorComLigacoesDiretasDto(edge.Target);
                
                edgeTarget.FromParent = new SimpleRelacionamento
                {
                    forcaLigacao = edge.FromParent.forcaLigacao,
                    forcaRelacao = edge.FromParent.forcaLigacao,
                    tags = edge.FromParent.listTags
                };  
                
                edgeTarget.ToParent = new SimpleRelacionamento
                {
                    forcaLigacao = edge.ToParent.forcaLigacao,
                    forcaRelacao = edge.ToParent.forcaLigacao,
                    tags = edge.ToParent.listTags
                };
    
                
                res.listLigacoes.Add(edgeTarget);
             
            }

            return res;
        }
    }
}