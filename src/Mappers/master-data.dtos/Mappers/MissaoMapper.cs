using System;
using System.Collections.Generic;
using System.Linq;
using master_data.domain.Model.Entities;
using master_data.mappers.Dtos.Controller.Missao;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Interfaces;
using master_data.mappers.Shared;

namespace master_data.mappers.Mappers
{
    public class MissaoMapper: IMissaoMapper
    {
        
        public MissaoRepositoryDto FromDomainToRepository(Missao missaoDomain)
        {
            if (missaoDomain is null)
                return null;

            return new MissaoRepositoryDto(new RepoGuid(missaoDomain.Id),
                new RepoGuid(missaoDomain.JogadorDesafiado.Id),
                new RepoGuid(missaoDomain.JogadorObjectivo.Id),
                missaoDomain.GrauDificuldade.value, missaoDomain.Pontos.Value, missaoDomain.DataHoraInicio.value,
                missaoDomain.DataHoraFim is null?null:missaoDomain.DataHoraFim.value);
        }

        public RegistarMissaoDto FromRepositoryToController(MissaoRepositoryDto missaoInserida)
        {
            if (missaoInserida is null)
                return null;

            RegistarMissaoDto result = new RegistarMissaoDto
            {
                JogadorObjectivo = missaoInserida.JogadorObjectivo.Email,
                JogadorDesafiado = missaoInserida.JogadorDesafiado.Email,
                GrauDificuldade = missaoInserida.GrauDificuldade
            };
            return result;
        }

        public PaginacaoDto<MissaoDto> FromRepositoryToMissaoDto(PaginacaoDto<MissaoRepositoryDto> obj)
        {
            List<MissaoDto> lst = obj.Select(x => FromRepositoryToMissaoDto(x)).ToList();
            return new PaginacaoDto<MissaoDto>(lst, obj.ContagemTotal, obj.NumeroPagina, obj.TamanhoPagina);
        }

        public MissaoDto FromRepositoryToMissaoDto(MissaoRepositoryDto obj)
        {
            if (obj is null)
            {
                return null;
            }

            return new MissaoDto()
            {
                JogadorDesafiadoEmail = obj.JogadorDesafiado.Email,
                JogadorObjectivoEmail = obj.JogadorObjectivo.Email,
                JogadorObjectivoNome = obj.JogadorObjectivo.Nome,
                JogadorObjectivoAvatar = obj.JogadorObjectivo.Avatar is null
                    ? null
                    : Convert.ToBase64String(obj.JogadorObjectivo.Avatar),
                GrauDificuldade = obj.GrauDificuldade
            };
        }
    }
}