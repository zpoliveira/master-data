using System;
using master_data.domain.Shared;
using master_data.domain.Model.ValueObjects;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Interfaces;
using master_data.mappers.Shared;
using master_data.mappers.Dtos.Controller.Jogador;

namespace master_data.mappers.Mappers
{
    public class HumorMapper : IHumorMapper
    {
        public Humor FromRepositoryDto(HumorRepositoryDto dto)
        {
            return new Humor(
                dto.Id.AsGuid(),
                (EstadoHumor) Enum.Parse(typeof(EstadoHumor), dto.EstadoHumor));
        }

        public HumorRepositoryDto ToRepositoryDto(Humor humor)
        {
            return new HumorRepositoryDto(
                new RepoGuid(humor.Id),
                humor.Value.ToString());
        }

        public HumorControllerDto ToListControllerDto(HumorRepositoryDto dto)
        {
            return new HumorControllerDto(
               dto.EstadoHumor);
        }

        public AtualizarHumorDto FromRepoEditarHumorDto(JogadorRepositoryDto dto)
        {
            return new AtualizarHumorDto(
                nome: dto.Nome,
                humor: dto.Humor);
                
        }
        
    }







}