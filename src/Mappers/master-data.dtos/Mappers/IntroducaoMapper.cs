using System;
using System.Collections.Generic;
using System.Linq;
using master_data.domain.Model.Entities;
using master_data.mappers.Dtos.Controller.Introducao;
using master_data.domain.Shared;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Interfaces;
using QuikGraph;

namespace master_data.mappers.Mappers
{
    public class IntroducaoMapper : IIntroducaoMapper
    {
        private readonly IJogadorMapper _jogadorMapper;

        public IntroducaoMapper(IJogadorMapper jogadorMapper)
        {
            _jogadorMapper = jogadorMapper;
        }

        public IntroducaoRepositoryDto ToRepositoryDto(Introducao introducao)
        {
            return new IntroducaoRepositoryDto(
                introducao.Id,
                _jogadorMapper.ToRepositoryDto(introducao.jogadorRequisitante),
                _jogadorMapper.ToRepositoryDto(introducao.jogadorIntrodutor),
                _jogadorMapper.ToRepositoryDto(introducao.jogadorObjectivo),
                introducao.ForcaLigacao.Value,
                introducao.estadoIntroducaoIntrodutor.ToString(),
                introducao.estadoIntroducaoObjectivo.ToString()
            );
        }

        public List<LigacaoPendenteDto> ToListaLigacaoPendenteDto(IEnumerable<IntroducaoRepositoryDto> repoDtoList)
        {
            return repoDtoList.Select(dto =>
            {
                var intro = dto.JogadorIntrodutor is null;
                return new LigacaoPendenteDto
                {
                    emailJogador = intro ? dto.JogadorObjectivo.Email : dto.JogadorIntrodutor.Email,
                    forcaLigacao = dto.ForcaLigacao,
                    idLigacao = dto.Id.ToString(),
                    introducao = intro
                };
            }).ToList();
        }

        public Introducao FromRepositoryDto(IntroducaoRepositoryDto dto)
        {
            throw new NotImplementedException();
        }

        public IntroducaoRepositoryDto FromListRepositoryDto(ObterListaPedidosLigacaoPendentesDto dto)
        {
            throw new NotImplementedException();
        }

        public IntroducaoRepositoryDto FromPedidoLigacaoDto(JogadorRepositoryDto requisitante,
            JogadorRepositoryDto objetivo, int forca)
        {
            return new IntroducaoRepositoryDto(
                Guid.NewGuid(),
                requisitante,
                null,
                objetivo,
                forca,
                null,
                EstadoEnum.PENDENTE.ToString()
            );
        }

        public IntroducaoRepositoryDto FromPedidoIntroducaoDto(JogadorRepositoryDto requisitante,
            JogadorRepositoryDto intermediario, JogadorRepositoryDto objetivo, int forca)
        {
            return new IntroducaoRepositoryDto(
                Guid.NewGuid(),
                requisitante,
                intermediario,
                objetivo,
                forca,
                EstadoEnum.PENDENTE.ToString(),
                EstadoEnum.PENDENTE.ToString()
            );
        }
    }
}