using System;
using System.Collections.Generic;
using System.Linq;
using master_data.domain.Model.Entities;
using master_data.domain.Model.ValueObjects;
using master_data.domain.Shared;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Interfaces;
using master_data.mappers.Shared;

namespace master_data.mappers.Mappers
{
    public class JogadorMapper : IJogadorMapper
    {
        private readonly ITagMapper _tagMapper;

        public JogadorMapper(ITagMapper tagMapper)
        {
            _tagMapper = tagMapper;
        }

        public JogadorRepositoryDto ToRepositoryDto(Jogador jogador)
        {
            var dto = new JogadorRepositoryDto(
                new RepoGuid(jogador.Id),
                jogador.Nome.Value,
                jogador.Email.Value,
                jogador.Password.Value,
                jogador.Telefone.Value,
                jogador.UrlFacebook.Value,
                jogador.UrlLinkedIn.Value,
                jogador.Humor.Value.ToString(),
                jogador.Nacionalidade.Value,
                jogador.CidadeResidencia.Value,
                jogador.DescricaoBreve.Value,
                jogador.Avatar.Value,
                //_humorMapper.ToRepositoryDto(jogador.Humor),
                jogador.DataNascimento.Value
            );

            foreach (var jogadorTag in jogador.Tags) dto.tagList.Add(_tagMapper.ToRepositoryDto(jogadorTag));

            return dto;
        }

        public RegistarJogadorDto ToRegistarUtilizadorDto(Jogador jogador)
        {
            if (jogador is null)
                return null;
            
            var tagList = jogador.Tags?.Select(t => t.Value).ToList();

            return new RegistarJogadorDto(
                jogador.Nome.Value,
                jogador.Email.Value,
                jogador.Password.Value,
                jogador.Telefone.Value,
                jogador.UrlFacebook.Value,
                jogador.UrlLinkedIn.Value,
                jogador.DataNascimento.Value.ToShortDateString(),
                jogador.Humor.ToString(),
                jogador.Nacionalidade.Value,
                jogador.CidadeResidencia.Value,
                jogador.DescricaoBreve.Value,
                jogador.Avatar.ToString(),
                tagList);
        }

        public Jogador FromRegistarUtilizadorDto(RegistarJogadorDto dto)
        {
            var jogador = new Jogador(
                new Nome(dto.nome),
                new Email(dto.email),
                new Password(dto.password),
                new Url(dto.urlFacebook),
                new Url(dto.urlLinkedIn),
                new Telefone(dto.telefone),
                new DataNascimento(dto.dataNascimento),
                new Nacionalidade(dto.nacionalidade),
                new CidadeResidencia(dto.cidadeResidencia),
                new DescricaoBreve(dto.descricaoBreve),
                new Avatar(Convert.FromBase64String(dto.avatar)));

            foreach (var tag in dto.tagList)
                jogador.Tags.Add(new Tag(tag));

            return jogador;
        }

        public Jogador FromRepositoryToDomain(JogadorRepositoryDto dto)
        {
            if (dto is null)
                return null;
            
            return new Jogador(
                dto.Id.AsGuid(),
                new Nome(dto.Nome),
                new Email(dto.Email),
                password: new Password(dto.Password),
                humor: new Humor((EstadoHumor) Enum.Parse(typeof(EstadoHumor), dto.Humor)),
                urlFacebook: new Url(dto.UrlFacebook),
                urlLinkedIn: new Url(dto.UrlLinkedIn),
                telefone: new Telefone(dto.Telefone),
                dataNascimento: new DataNascimento(dto.DataNascimento),
                new Nacionalidade(dto.Nacionalidade),
                new CidadeResidencia(dto.CidadeResidencia),
                new DescricaoBreve(dto.DescricaoBreve),
                new Avatar(dto.Avatar)
            );
        }

        public JogadorSugestaoDto FromRepositoryToJogadorSugestaoDto(JogadorRepositoryDto dto)
        {
            if (dto is null)
                return null;

            return new JogadorSugestaoDto()
            {
                Email = dto.Email,
                Nome = dto.Nome,
                Avatar = dto.Avatar is null ? null : Convert.ToBase64String(dto.Avatar),
                Tags = dto.tagList.Select(t => t.Value).ToList()
            };
        }

        public Jogador FromUpdateUtilizadorDto(UpdateJogadorDto dto)
        {
            return new Jogador(
                new Nome(dto.nome),
                new Email(dto.email),
                password: new Password(dto.password),
                urlFacebook: new Url(dto.urlFacebook),
                urlLinkedIn: new Url(dto.urlLinkedIn),
                telefone: new Telefone(dto.telefone),
                dataNascimento: new DataNascimento(dto.dataNascimento),
                new Nacionalidade(dto.nacionalidade),
                new CidadeResidencia(dto.cidadeResidencia),
                new DescricaoBreve(dto.descricaoBreve),
                new Avatar(Convert.FromBase64String(dto.avatar)));
        }

        public PaginacaoDto<JogadorSugestaoDto> FromRepositoryToJogadorSugestaoDto(PaginacaoDto<JogadorRepositoryDto> lst)
        {
            List<JogadorSugestaoDto> dados = lst.Select(x => FromRepositoryToJogadorSugestaoDto(x)).ToList();
            return new PaginacaoDto<JogadorSugestaoDto>(dados, lst.PaginasTotal, lst.NumeroPagina, lst.TamanhoPagina);
        }
    }
}