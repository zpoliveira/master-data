﻿using System;
using System.Collections.Generic;

namespace master_data.mappers.GraphEntities
{
    public class JogadorNode
    {
        public Guid IdJogador { get; set; }
        
        public string Nome { get; set; }
        
        public string Email { get; set; }
        
        public List<RelacionamentoEdge> listLigacoes { get; set; }

        public bool IsSource { get; set; }

        public JogadorNode(Guid idJogador, string nome, string email, bool isSource)
        {
            IdJogador = idJogador;
            Nome = nome;
            Email = email;
            IsSource = isSource;
        }


        protected bool Equals(JogadorNode other)
        {
            return IdJogador.Equals(other.IdJogador);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((JogadorNode) obj);
        }

        public override int GetHashCode()
        {
            return IdJogador.GetHashCode();
        }
    }
}