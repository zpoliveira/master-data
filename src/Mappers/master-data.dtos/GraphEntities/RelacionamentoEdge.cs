﻿using QuikGraph;

namespace master_data.mappers.GraphEntities
{
    public class RelacionamentoEdge : Edge<JogadorNode>
    {
        public RelacionamentoDetail FromParent { get; set; }
        
        public RelacionamentoDetail ToParent { get; set; }

        public RelacionamentoEdge(
            JogadorNode source,
            JogadorNode target,
            RelacionamentoDetail fromParent, 
            RelacionamentoDetail toParent) : base(source, target)
        {
            FromParent = fromParent;
            ToParent = toParent;
        }
    }
}