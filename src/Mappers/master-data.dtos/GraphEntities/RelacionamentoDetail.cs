﻿using System.Collections.Generic;

namespace master_data.mappers.GraphEntities
{
    public class RelacionamentoDetail
    {
        public int forcaLigacao { get; set; }
        
        public decimal forcaRelacao { get; set; }
        
        public List<string> listTags { get; set; }
    }
}