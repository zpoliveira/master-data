﻿using System.Collections.Generic;
using master_data.domain.Model.Entities;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;

namespace master_data.mappers.Interfaces
{
    public interface IJogadorMapper
    {
        public JogadorRepositoryDto ToRepositoryDto(Jogador jogador);

        public RegistarJogadorDto ToRegistarUtilizadorDto(Jogador jogador);

        public Jogador FromRegistarUtilizadorDto(RegistarJogadorDto dto);

        public Jogador FromRepositoryToDomain(JogadorRepositoryDto dto);
        
        public JogadorSugestaoDto FromRepositoryToJogadorSugestaoDto(JogadorRepositoryDto dto);

        public Jogador FromUpdateUtilizadorDto(UpdateJogadorDto dto);

        PaginacaoDto<JogadorSugestaoDto> FromRepositoryToJogadorSugestaoDto(PaginacaoDto<JogadorRepositoryDto> lst);
    }
}