using master_data.domain.Model.Entities;
using master_data.mappers.Dtos.Controller.Missao;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;

namespace master_data.mappers.Interfaces
{
    public interface IMissaoMapper
    {
        MissaoRepositoryDto FromDomainToRepository(Missao missaoDomain);
        RegistarMissaoDto FromRepositoryToController(MissaoRepositoryDto missaoInserida);
        PaginacaoDto<MissaoDto> FromRepositoryToMissaoDto(PaginacaoDto<MissaoRepositoryDto> lst);
        
        MissaoDto FromRepositoryToMissaoDto(MissaoRepositoryDto obj);
    }
}