﻿using master_data.domain.Model.Entities;
using master_data.mappers.Dtos.Repository;

namespace master_data.mappers.Interfaces
{
    public interface ITagMapper
    {
        Tag FromRepositoryDto(TagRepoDto tagRepoDto);

        TagRepoDto ToRepositoryDto(Tag tag);
    }
}