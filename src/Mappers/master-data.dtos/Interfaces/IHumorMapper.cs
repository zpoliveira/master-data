using master_data.domain.Model.ValueObjects;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Controller.Jogador;

namespace master_data.mappers.Interfaces
{
    public interface IHumorMapper
    {
        Humor FromRepositoryDto(HumorRepositoryDto humorRepositoryDto);

        HumorRepositoryDto ToRepositoryDto(Humor humor);

        HumorControllerDto ToListControllerDto(HumorRepositoryDto humorRepositoryDto);

        AtualizarHumorDto FromRepoEditarHumorDto(JogadorRepositoryDto dto);
    }
}