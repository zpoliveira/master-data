using master_data.domain.Model.Entities;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Controller.Relacionamento;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.GraphEntities;

namespace master_data.mappers.Interfaces
{
    public interface IRelacionamentoMapper
    {
        public Relacionamento toDomain(RelacionamentoRepositoryDto dto);
        
        public EditarRelacionamentoDto FromRepositoryToEditarRelacionamentoDto(RelacionamentoRepositoryDto dto);
        
        public RelacionamentoDto FromRepositoryToRelacionamentoDto(RelacionamentoRepositoryDto dto);
        
        public PaginacaoDto<RelacionamentoDto> FromRepositoryToRelacionamentoDto(PaginacaoDto<RelacionamentoRepositoryDto> dto);

        public JogadorComLigacoesDiretasDto FromSocialNetworkJogadorNodeToJogadorComLigacoesDiretasDto(
            JogadorNode jogadorNode);
    }
}