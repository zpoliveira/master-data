using System.Collections.Generic;
using master_data.domain.Model.Entities;
using master_data.mappers.Dtos.Controller.Introducao;
using master_data.mappers.Dtos.Repository;

namespace master_data.mappers.Interfaces
{
    public interface IIntroducaoMapper
    {
        public IntroducaoRepositoryDto ToRepositoryDto(Introducao introducao);

        public Introducao FromRepositoryDto(IntroducaoRepositoryDto dto);

        public IntroducaoRepositoryDto FromListRepositoryDto(ObterListaPedidosLigacaoPendentesDto dto);

        public IntroducaoRepositoryDto FromPedidoLigacaoDto(JogadorRepositoryDto requisitante,
            JogadorRepositoryDto objetivo, int forca);

        public IntroducaoRepositoryDto FromPedidoIntroducaoDto(JogadorRepositoryDto requisitante,
        JogadorRepositoryDto intermediario, JogadorRepositoryDto objetivo, int forca);

        List<LigacaoPendenteDto> ToListaLigacaoPendenteDto(IEnumerable<IntroducaoRepositoryDto> repoDtoList);
    }
}