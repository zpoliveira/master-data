using System;
using System.Collections.Generic;

namespace master_data.mappers.Dtos.Shared
{
    public class PaginacaoDto<T>: List<T>
    {
        public PaginacaoDto(List<T> lista,int contagemTotal, int numeroPagina, int tamanhoPagina)
        {
            ContagemTotal = contagemTotal;
            NumeroPagina = numeroPagina;
            TamanhoPagina = tamanhoPagina;
            PaginasTotal = (int)Math.Ceiling(contagemTotal / (decimal) tamanhoPagina);
            TemProxima = numeroPagina < PaginasTotal;
            TemAnterior = numeroPagina > 1;
            AddRange(lista);
        }

        public int ContagemTotal { get; private set; }
        
        public int NumeroPagina { get; private set; }
        
        public int TamanhoPagina { get; private set; }
        
        public int PaginasTotal { get; private set; }
        
        public bool TemAnterior { get; private set; }
        
        public bool TemProxima { get; private set; }
        
    }
}