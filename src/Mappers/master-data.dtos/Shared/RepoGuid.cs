﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace master_data.mappers.Shared
{
    [ComplexType]
    public class RepoGuid : EntityId
    {
        [JsonConstructor]
        public RepoGuid(Guid value) : base(value)
        {
        }

        public RepoGuid(string value) : base(value)
        {
        }

        protected override object createFromString(string text)
        {
            return Guid.Parse(text);
        }

        public override string AsString()
        {
            var obj = (Guid) ObjValue;
            return obj.ToString();
        }

        public Guid AsGuid()
        {
            return (Guid) ObjValue;
        }

        public override string ToString()
        {
            return AsString();
        }
    }
}