using System;

namespace master_data.mappers.Dtos.Controller.Missao
{
    public class RegistarMissaoDto
    {

        public String JogadorDesafiado { get; set; }
        
        public String JogadorObjectivo { get; set; }
        
        public int GrauDificuldade { get; set; }
        
    }
}