using master_data.mappers.Dtos.Controller.Shared;

namespace master_data.mappers.Dtos.Controller.Missao
{
    public class PaginaMissaoDto :PaginaDto
    {
        public string email { get; set; }
    }
}