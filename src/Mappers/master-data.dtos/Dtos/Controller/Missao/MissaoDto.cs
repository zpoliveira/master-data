namespace master_data.mappers.Dtos.Controller.Missao
{
    public class MissaoDto
    {
        public string JogadorObjectivoEmail { get; set; }

        public string JogadorDesafiadoEmail { get; set; }
        
        public string JogadorObjectivoNome { get; set; }
        
        public string JogadorObjectivoAvatar { get; set; }
        
        public int GrauDificuldade { get; set; }
    }
}