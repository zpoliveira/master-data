using System;
using System.Collections.Generic;

namespace master_data.mappers.Dtos.Controller.Relacionamento
{
    public class RelacionamentoDto
    {
        public string EmailDe { get; set; }
        
        public string EmailPara { get; set; }

        public string Nome { get; set; }

        public int ForcaLigacao { get; set; }
        
        public Decimal ForcaRelacao { get; set; }
        
        public string Avatar { get; set; }
        
        public List<String> Tags { get; set; }
    }
}