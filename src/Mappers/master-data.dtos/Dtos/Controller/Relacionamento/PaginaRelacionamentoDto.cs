using master_data.mappers.Dtos.Controller.Shared;

namespace master_data.mappers.Dtos.Controller.Relacionamento
{
    public class PaginaRelacionamentoDto: PaginaDto
    {
        public string email { get; set; }
    }
}