using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace master_data.mappers.Dtos.Controller.Relacionamento
{
    public class EditarRelacionamentoDto
    {
        public EditarRelacionamentoDto(string emailJodadorDe, string emailJogadorPara, int forca, ICollection<string> lstTags)
        {
            EmailJogadorDe = emailJodadorDe;
            EmailJogadorPara = emailJogadorPara;
            Forca = forca;
            LstTags = lstTags;
        }

        [Required] public string EmailJogadorDe { get; set; }

        [Required] public string EmailJogadorPara { get; set; }

        [Required] public int Forca { get; set; }

        public ICollection<string> LstTags { get; set; }


        protected bool Equals(EditarRelacionamentoDto other)
        {
            return EmailJogadorDe.Equals(other.EmailJogadorDe) && EmailJogadorPara.Equals(other.EmailJogadorPara);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((EditarRelacionamentoDto) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(EmailJogadorDe, EmailJogadorPara);
        }
    }
}