﻿using System.Collections.Generic;
using master_data.mappers.Dtos.Controller.Jogador;

namespace master_data.mappers.Dtos.Controller.Relacionamento
{
    public class RedeJogadorNiveisDto
    {
        public IEnumerable<JogadorComLigacoesDiretasDto> listaRede { get; set; }
    }
}