﻿using System.Collections.Generic;

namespace master_data.mappers.Dtos.Controller.Relacionamento
{
    public class SimpleRelacionamento
    {
        public int forcaLigacao { get; set; }
        public int forcaRelacao { get; set; }
        public List<string> tags { get; set; }
    }
}