using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;

namespace master_data.mappers.Dtos.Controller.Jogador
{
    public class PesquisarJogadorDto
    {
        public PesquisarJogadorDto()
        {
        }

        public PesquisarJogadorDto(
            string nome,
            string email,
            string telefone,
            string urlFacebook,
            string urlLinkedIn,
            string dataNascimento,
            string nacionalidade,
            string cidadeResidencia)
        {
            this.nome = nome;
            this.email = email;
            this.telefone = telefone;
            this.urlFacebook = urlFacebook;
            this.urlLinkedIn = urlLinkedIn;
            this.dataNascimento = dataNascimento;
            this.nacionalidade = nacionalidade;
            this.cidadeResidencia = cidadeResidencia;
        }

        public string nome { get; set; }

        public string email { get; set; }

        public string telefone { get; set; }

        public string urlFacebook { get; set; }

        public string urlLinkedIn { get; set; }

        public string dataNascimento { get; set; }
        
        public string nacionalidade { get; set; }
        public string cidadeResidencia { get; set; }
    }
}