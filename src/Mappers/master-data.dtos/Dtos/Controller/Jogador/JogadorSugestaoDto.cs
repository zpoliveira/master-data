using System.Collections.Generic;

namespace master_data.mappers.Dtos.Controller.Jogador
{
    public class JogadorSugestaoDto
    {
        public JogadorSugestaoDto()
        {
            Tags = new List<string>();
        }

        public string Avatar { get; set; }
        public string Email { get; set; }

        public string Nome { get; set; }

        public List<string> Tags { get; set; }
        
    }
}