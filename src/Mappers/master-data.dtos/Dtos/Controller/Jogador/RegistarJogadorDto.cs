﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using master_data.domain.Model.ValueObjects;

namespace master_data.mappers.Dtos.Controller.Jogador
{
    public class RegistarJogadorDto
    {
        protected RegistarJogadorDto()
        {
        }

        public RegistarJogadorDto(
            string nome,
            string email,
            string password,
            string telefone,
            string urlFacebook,
            string urlLinkedIn,
            string dataNascimento,
            string humor,
            string nacionalidade,
            string cidadeResidencia,
            string descricaoBreve,
            string avatar,
            IEnumerable<string> tagList)
        {
            this.nome = nome;
            this.email = email;
            this.password = password;
            this.telefone = telefone;
            this.urlFacebook = urlFacebook;
            this.urlLinkedIn = urlLinkedIn;
            this.dataNascimento = dataNascimento;
            this.humor = humor;
            this.nacionalidade = nacionalidade;
            this.cidadeResidencia = cidadeResidencia;
            this.descricaoBreve = descricaoBreve;
            this.avatar = avatar;
            this.tagList = tagList;
        }

        [Required(ErrorMessage = "O nome é obrigatório para registo de Jogador")]
        [MaxLength(512, ErrorMessage = "O nome não pode ser superior a 512 caracteres")]
        public string nome { get; }

        [Required(ErrorMessage = "O email é obrigatório para registo de Jogador")]
        [MaxLength(512, ErrorMessage = "O email não pode ser superior a 512 caracteres")]
        public string email { get; }

        [Required(ErrorMessage = "A password é obrigatório para registo de Jogador")]
        [StringLength(32, MinimumLength = 8,
            ErrorMessage = "Password só pode ter tentre 8 a 32 caracteres")]
        public string password { get; }

        [MaxLength(12, ErrorMessage = "O telefone nao pode ter mais de 12 numeros")]
        public string telefone { get; }

        public string urlFacebook { get; }

        public string urlLinkedIn { get; }

        [Required(ErrorMessage = "A data de nascimento do jogador é obrigatória")]
        public string dataNascimento { get; set; }

        public string humor { get; set; }
        
        public string nacionalidade { get; set; }
        
        public string cidadeResidencia { get; set; }
        
        [MaxLength(4000, ErrorMessage = "A descrição breve não pode ser superior a 4000 caracteres")]
        public string descricaoBreve { get; set; }
        
        [MaxLength(1000000)]
        public string avatar { get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "O jogador tem de ter pelo menos uma Tag atribuída")]
        public IEnumerable<string> tagList { get; set; }

        protected bool Equals(RegistarJogadorDto other)
        {
            return nome == other.nome &&
                   email == other.email &&
                   password == other.password &&
                   telefone == other.telefone &&
                   urlFacebook == other.urlFacebook &&
                   urlLinkedIn == other.urlLinkedIn &&
                   dataNascimento == other.dataNascimento &&
                   humor == other.humor;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RegistarJogadorDto) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(nome, email, password, telefone, urlFacebook, urlLinkedIn, dataNascimento, humor);
        }
    }
}