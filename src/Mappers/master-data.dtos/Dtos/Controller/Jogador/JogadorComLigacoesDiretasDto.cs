﻿using System.Collections.Generic;
using master_data.mappers.Dtos.Controller.Relacionamento;

namespace master_data.mappers.Dtos.Controller.Jogador
{
    public class JogadorComLigacoesDiretasDto
    {
        public SimpleJogador Jogador { get; set; }
        public SimpleRelacionamento FromParent { get; set; }
        public SimpleRelacionamento ToParent { get; set; }
        public List<JogadorComLigacoesDiretasDto> listLigacoes { get; set; }

        protected bool Equals(JogadorComLigacoesDiretasDto other)
        {
            return Equals(Jogador, other.Jogador);
        }
/*
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((JogadorComLigacoesDiretasDto) obj);
        }
*/
        public override int GetHashCode()
        {
            return (Jogador != null ? Jogador.GetHashCode() : 0);
        }
    }
}