using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;

namespace master_data.mappers.Dtos.Controller.Jogador
{
    public class HumorControllerDto
    {
        public HumorControllerDto()
        {
        }

        public HumorControllerDto(
            string estadoHumor)
        {
            this.EstadoHumor = estadoHumor; 
        }

        [Required(ErrorMessage = "O estado humor é obrigatório")]
        public string EstadoHumor { get; set; }

    }
}