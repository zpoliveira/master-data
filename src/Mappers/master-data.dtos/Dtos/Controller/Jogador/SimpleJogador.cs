﻿using System;

namespace master_data.mappers.Dtos.Controller.Jogador
{
    public class SimpleJogador
    {
        public string nome { get; set; }
        public string email { get; set; }

        protected bool Equals(SimpleJogador other)
        {
            return nome == other.nome && email == other.email;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((SimpleJogador) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(nome, email);
        }
    }
}