using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace master_data.mappers.Dtos.Controller.Jogador
{
    public record AtualizarHumorDto
    {
        protected AtualizarHumorDto(){
        }

        public AtualizarHumorDto(
            string nome,
            string humor)
        {
            this.nome = nome;
            this.humor = humor;
            
        }

        public string nome { get; set; }
        public string humor { get; set; }
        
    }
}