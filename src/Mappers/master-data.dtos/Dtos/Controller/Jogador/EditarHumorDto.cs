using System.ComponentModel.DataAnnotations;

namespace master_data.mappers.Dtos.Controller.Jogador
{
    public class EditarHumorDto
    {
        protected EditarHumorDto()
        {
        }

        public EditarHumorDto(
            string email,
            string humor
        )
        {
            this.email = email;
            this.humor = humor;
        }

        [Required(ErrorMessage = "O email é obrigatório para atualizar o estado de humor")]
        [MaxLength(512, ErrorMessage = "O email não pode ser superior a 512 caracteres")]
        public string email { get; }

        [Required(ErrorMessage = "É obrigatório indicar o novo estado de humor")]
        public string humor { get; }
    }
}