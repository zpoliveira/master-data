﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace master_data.mappers.Dtos.Controller.Jogador
{
    public record UpdateJogadorDto
    {
        protected UpdateJogadorDto()
        {
        }

        public UpdateJogadorDto(
            string nome,
            string email,
            string password,
            string telefone,
            string urlFacebook,
            string urlLinkedIn,
            string dataNascimento,
            string humor,
            string nacionalidade,
            string cidadeResidencia,
            string descricaoBreve,
            string avatar,
            IEnumerable<string> tagList)
        {
            this.nome = nome;
            this.email = email;
            this.password = password;
            this.telefone = telefone;
            this.urlFacebook = urlFacebook;
            this.urlLinkedIn = urlLinkedIn;
            this.dataNascimento = dataNascimento;
            this.humor = humor;
            this.nacionalidade = nacionalidade;
            this.cidadeResidencia = cidadeResidencia;
            this.descricaoBreve = descricaoBreve;
            this.avatar = avatar;
            this.tagList = tagList;
        }

        public string nome { get; set; }
        public string email { get; }
        public string password { get; set; }
        public string telefone { get; set; }

        public string urlFacebook { get; set; }

        public string urlLinkedIn { get; set; }
        public string dataNascimento { get; set; }
        public string humor { get; set; }
        public string nacionalidade { get; set; }
        public string cidadeResidencia { get; set; }
        public string descricaoBreve { get; set; }
        public string avatar { get; set; }
        public IEnumerable<string> tagList { get; set; }


        public virtual bool Equals(UpdateJogadorDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return email == other.email;
        }

        private sealed class EmailEqualityComparer : IEqualityComparer<UpdateJogadorDto>
        {
            public bool Equals(UpdateJogadorDto x, UpdateJogadorDto y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.email == y.email;
            }

            public int GetHashCode(UpdateJogadorDto obj)
            {
                return obj.email != null ? obj.email.GetHashCode() : 0;
            }
        }

        public static IEqualityComparer<UpdateJogadorDto> EmailComparer { get; } = new EmailEqualityComparer();

        public override int GetHashCode()
        {
            return email != null ? email.GetHashCode() : 0;
        }
    }
}