namespace master_data.mappers.Dtos.Controller.Shared
{
    public abstract class PaginaDto
    {
        private const int MaxTamanhoPagina = 50;
        private const int MinNumeroPagina = 1;
        
        private int _numeroPagina = 1;
        public int NumeroPagina
        {
            get
            {
                return _numeroPagina;
            }
            set
            {
                _numeroPagina = value < MinNumeroPagina ? MinNumeroPagina : value;
            }
        }

        private int _tamanhoPagina = 10;

        public int TamanhoPagina
        {
            get
            {
                return _tamanhoPagina;
            }
            set
            {
                _tamanhoPagina = (value > MaxTamanhoPagina) ? MaxTamanhoPagina : value;
            }
        }
    }
}