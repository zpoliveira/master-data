﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace master_data.mappers.Dtos.Controller.Introducao
{
    public class AceitarIntroducaoDto
    {
        public Guid idIntroducao { get; set; }

        [Required(ErrorMessage = "E-mail do Jogador é obrigatório")]
        [MaxLength(320, ErrorMessage = "O email não pode ter mais do que 320 caracteres")]
        public string emailJogador { get; set; }

        public bool aceitar { get; set; }
        
        public int forcaLigacao { get; set; }
        
        public IEnumerable<string> tags { get; set; }
    }
}