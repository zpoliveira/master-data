﻿namespace master_data.mappers.Dtos.Controller.Introducao
{
    public class LigacaoPendenteDto
    {
        public string idLigacao { get; set; }
        public string emailJogador { get; set; }
        public int forcaLigacao { get; set; }
        public bool introducao { get; set; }
    }
}