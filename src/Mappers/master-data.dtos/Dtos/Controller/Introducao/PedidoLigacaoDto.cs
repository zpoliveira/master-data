using System;
using System.ComponentModel.DataAnnotations;

namespace master_data.mappers.Dtos.Controller.Introducao
{
    public class PedidoLigacaoDto
    {
        public PedidoLigacaoDto()
        {
        }

        public PedidoLigacaoDto(string emailJogadorRequisitante, string emailJogadorObjectivo, int forcaLigacao)
        {
            this.emailJogadorRequisitante = emailJogadorRequisitante;
            this.emailJogadorObjectivo = emailJogadorObjectivo;
            this.forcaLigacao = forcaLigacao;
        }

        [Required(ErrorMessage = "O email do requerente é obrigatório para pedido de ligação")]
        [MaxLength(512, ErrorMessage = "O email não pode ser superior a 512 caracteres")]
        public string emailJogadorRequisitante { get; set; }

        [Required(ErrorMessage = "O email do objetivo é obrigatório para pedido de ligação")]
        [MaxLength(512, ErrorMessage = "O email não pode ser superior a 512 caracteres")]
        public string emailJogadorObjectivo { get; set; }

        [Required(ErrorMessage = "Força de ligação é obrigatória para pedido de ligação")]
        [Range(0, 10, ErrorMessage = "Valor da força de ligação tem de ser entre 0 e 10")]
        public int forcaLigacao { get; set; }

        protected bool Equals(PedidoLigacaoDto other)
        {
            return emailJogadorRequisitante == other.emailJogadorRequisitante &&
                   emailJogadorObjectivo == other.emailJogadorObjectivo;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PedidoLigacaoDto) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(emailJogadorRequisitante, emailJogadorObjectivo);
        }
    }
}