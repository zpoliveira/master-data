using System;
using System.ComponentModel.DataAnnotations;

namespace master_data.mappers.Dtos.Controller.Introducao
{
    public class ObterListaPedidosLigacaoPendentesDto
    {
        public ObterListaPedidosLigacaoPendentesDto()
        {
        }

        public ObterListaPedidosLigacaoPendentesDto(string jogadorRequisitanteEmail)
        {
            this.jogadorRequisitanteEmail = jogadorRequisitanteEmail;
        }

        [Required(ErrorMessage = "Email de Jogador a pesquisar ligações é obrigatório")]
        [MaxLength(512, ErrorMessage = "O email não pode ser superior a 512 caracteres")]
        public string jogadorRequisitanteEmail { get; set; }
    }
}