﻿using System.Collections.Generic;

namespace master_data.mappers.Dtos.Controller.Introducao
{
    public class ListaLigacaoPendenteDto
    {
        public List<LigacaoPendenteDto> ligacoesPendentes { get; set; }
    }
}