using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using master_data.mappers.Shared;

namespace master_data.mappers.Dtos.Repository
{
    [Table("Relacionamento")]
    public class RelacionamentoRepositoryDto : Entity<RepoGuid>
    {
        public RelacionamentoRepositoryDto()
        {
        }

        public RelacionamentoRepositoryDto(RepoGuid id, JogadorRepositoryDto jogadorDe, JogadorRepositoryDto jogadorPara, int forcaLigacao, ICollection<TagRepoDto> tagList)
        {
            Id = id;
            JogadorDe = jogadorDe;
            JogadorPara = jogadorPara;
            ForcaLigacao = forcaLigacao;
            TagList = tagList;
        }
        
        public RelacionamentoRepositoryDto(JogadorRepositoryDto jogadorDe, JogadorRepositoryDto jogadorPara, int forcaLigacao, ICollection<TagRepoDto> tagList)
        {
            Id = new RepoGuid(Guid.NewGuid());
            JogadorDe = jogadorDe;
            JogadorPara = jogadorPara;
            ForcaLigacao = forcaLigacao;
            TagList = tagList;
        }

        public RelacionamentoRepositoryDto(RepoGuid id,JogadorRepositoryDto jogadorDe, JogadorRepositoryDto jogadorPara, int forcaLigacao, decimal forcaRelacao, ICollection<TagRepoDto> tagList)
        {
            Id = id;
            JogadorDe = jogadorDe;
            JogadorPara = jogadorPara;
            ForcaLigacao = forcaLigacao;
            ForcaRelacao = forcaRelacao;
            TagList = tagList;
        }


        public JogadorRepositoryDto JogadorDe { get; set; }

        public JogadorRepositoryDto JogadorPara { get; set; }

        public int ForcaLigacao { get; set; }

        public decimal ForcaRelacao { get; set; }

        public ICollection<TagRepoDto> TagList { get; set; }


        protected bool Equals(RelacionamentoRepositoryDto other)
        {
            return Equals(JogadorDe, other.JogadorDe) && Equals(JogadorPara, other.JogadorPara);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RelacionamentoRepositoryDto) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(JogadorDe, JogadorPara);
        }
    }
}