using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Security.Principal;
using master_data.mappers.Shared;

namespace master_data.mappers.Dtos.Repository
{
    public class MissaoRepositoryDto : Entity<RepoGuid>
    {
        protected MissaoRepositoryDto()
        {
        }

        public MissaoRepositoryDto(RepoGuid id,RepoGuid jogadorDesafiadoId, RepoGuid jogadorObjectivoId, int grauDificuldade, int pontos, DateTime dataHoraInicio, DateTime? dataHoraFim)
        {
            Id = id;
            JogadorDesafiadoID = jogadorDesafiadoId;
            JogadorObjectivoID = jogadorObjectivoId;
            GrauDificuldade = grauDificuldade;
            Pontos = pontos;
            DataHoraInicio = dataHoraInicio;
            DataHoraFim = dataHoraFim;
        }

        public RepoGuid JogadorDesafiadoID { get; set; }
        public virtual JogadorRepositoryDto JogadorDesafiado { get; set; }

        public RepoGuid JogadorObjectivoID { get; set; }
        public virtual JogadorRepositoryDto JogadorObjectivo { get; set; }

        public int GrauDificuldade { get; set; }
        
        public int Pontos { get; set; } 
        
        public DateTime DataHoraInicio { get; set; }
        
        public DateTime? DataHoraFim { get; set; }

        public ICollection<IntroducaoRepositoryDto> Introducoes { get; set; }

        protected bool Equals(MissaoRepositoryDto other)
        {
            return Equals(JogadorDesafiado, other.JogadorDesafiado) && Equals(JogadorObjectivo, other.JogadorObjectivo);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MissaoRepositoryDto) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(JogadorDesafiado, JogadorObjectivo);
        }
    }
}