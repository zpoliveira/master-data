﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using master_data.mappers.Shared;

namespace master_data.mappers.Dtos.Repository
{
    [Table("Jogador")]
    public class JogadorRepositoryDto : Entity<RepoGuid>
    {
        protected JogadorRepositoryDto()
        {
        }

        public JogadorRepositoryDto(
            RepoGuid id,
            string nome,
            string email,
            string password,
            string telefone,
            string urlFacebook,
            string urlLinkedIn,
            string humor,
            string nacionalidade,
            string cidadeResidencia,
            string descricaoBreve,
            byte[] avatar,
            //HumorRepositoryDto humor,
            DateTime dataNascimento)
        {
            Id = id;
            Nome = nome;
            Email = email;
            Password = password;
            Telefone = telefone;
            UrlFacebook = urlFacebook;
            UrlLinkedIn = urlLinkedIn;
            Humor = humor;
            DataNascimento = dataNascimento;
            Nacionalidade = nacionalidade;
            CidadeResidencia = cidadeResidencia;
            DescricaoBreve = descricaoBreve;
            Avatar = avatar;

            tagList = new List<TagRepoDto>();
        }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Telefone { get; set; }

        public string UrlFacebook { get; set; }

        public string UrlLinkedIn { get; set; }

        public string Humor { get; set; }
        //public HumorRepositoryDto Humor { get; set; }

        public DateTime DataNascimento { get; set; }
        public string Nacionalidade { get; set; }
        public string CidadeResidencia { get; set; }
        
        [MaxLength(4000)]
        public string DescricaoBreve { get; set; }
        public byte[] Avatar { get; set; }

        public ICollection<TagRepoDto> tagList { get; set; }

        public ICollection<RelacionamentoRepositoryDto> lstRelacionamentosDe { get; set; }

        public ICollection<RelacionamentoRepositoryDto> lstRelacionamentosPara { get; set; }

        public ICollection<IntroducaoRepositoryDto> IntroducaoIntrodutor { get; set; }

        public ICollection<IntroducaoRepositoryDto> IntroducaoRequesitante { get; set; }

        public ICollection<IntroducaoRepositoryDto> IntroducaoObjectivo { get; set; }

        public ICollection<MissaoRepositoryDto> MissoesDesafiado { get; set; }
        
        public ICollection<MissaoRepositoryDto> MissoesObjectivo { get; set; }

        protected bool Equals(JogadorRepositoryDto other)
        {
            return Nome == other.Nome &&
                   Email == other.Email &&
                   Password == other.Password &&
                   Telefone == other.Telefone &&
                   UrlFacebook == other.UrlFacebook &&
                   UrlLinkedIn == other.UrlLinkedIn &&
                   Humor == other.Humor &&
                   DataNascimento.Equals(other.DataNascimento);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((JogadorRepositoryDto) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Nome, Email, Password, Telefone, UrlFacebook, UrlLinkedIn, Humor, DataNascimento);
        }
    }
}