using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using master_data.mappers.Shared;

namespace master_data.mappers.Dtos.Repository
{
    [Table("Introducao")]
    public class IntroducaoRepositoryDto : Entity<RepoGuid>
    {
        protected IntroducaoRepositoryDto()
        {
        }

        public IntroducaoRepositoryDto(
            Guid id,
            JogadorRepositoryDto jogadorRequisitante,
            JogadorRepositoryDto jogadorIntrodutor,
            JogadorRepositoryDto jogadorObjectivo,
            int forcaLigacao,
            string estadoIntroducaoIntrodutor,
            string estadoIntroducaoObjectivo)
        {
            Id = new RepoGuid(id);
            JogadorRequisitante = jogadorRequisitante;
            JogadorIntrodutor = jogadorIntrodutor;
            JogadorObjectivo = jogadorObjectivo;
            ForcaLigacao = forcaLigacao;
            EstadoIntroducaoIntrodutor = estadoIntroducaoIntrodutor;
            EstadoIntroducaoObjectivo = estadoIntroducaoObjectivo;
        }

        public IntroducaoRepositoryDto(
            Guid id,
            JogadorRepositoryDto jogadorRequisitante,
            JogadorRepositoryDto jogadorObjectivo,
            int forcaLigacao,
            string estadoIntroducao)
        {
            Id = new RepoGuid(id);
            JogadorRequisitante = jogadorRequisitante;
            JogadorObjectivo = jogadorObjectivo;
            ForcaLigacao = forcaLigacao;
            EstadoIntroducaoObjectivo = estadoIntroducao;
        }


        [Required] public JogadorRepositoryDto JogadorRequisitante { get; set; }

        public JogadorRepositoryDto JogadorIntrodutor { get; set; }

        [Required] public JogadorRepositoryDto JogadorObjectivo { get; set; }

        [Required] public int ForcaLigacao { get; set; }

        public string EstadoIntroducaoIntrodutor { get; set; }

        [Required] public string EstadoIntroducaoObjectivo { get; set; }

        public MissaoRepositoryDto MissaoRepository { get; set; }
        
        protected bool Equals(IntroducaoRepositoryDto other)
        {
            return Equals(JogadorRequisitante,
                       other.JogadorRequisitante) &&
                   Equals(JogadorIntrodutor,
                       other.JogadorIntrodutor) &&
                   Equals(JogadorObjectivo,
                       other.JogadorObjectivo) &&
                   ForcaLigacao == other.ForcaLigacao &&
                   EstadoIntroducaoIntrodutor == other.EstadoIntroducaoIntrodutor &&
                   EstadoIntroducaoObjectivo == other.EstadoIntroducaoObjectivo;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((IntroducaoRepositoryDto) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(JogadorRequisitante,
                JogadorIntrodutor,
                JogadorObjectivo,
                ForcaLigacao,
                EstadoIntroducaoIntrodutor,
                EstadoIntroducaoObjectivo);
        }
    }
}