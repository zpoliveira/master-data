using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using master_data.mappers.Shared;

namespace master_data.mappers.Dtos.Repository
{
    [Table("EstadosHumor")]
    public class HumorRepositoryDto : Entity<RepoGuid>
    {
        public HumorRepositoryDto(string estadoHumor)
        {
            Id = new RepoGuid(Guid.NewGuid());
            EstadoHumor = estadoHumor;
        }

        public HumorRepositoryDto(RepoGuid id, string estadoHumor)
        {
            Id = id;
            EstadoHumor = estadoHumor;
        }

        public string EstadoHumor { get; set; }

        public IEnumerable<JogadorRepositoryDto> jogadores { get; set; }
    }
}