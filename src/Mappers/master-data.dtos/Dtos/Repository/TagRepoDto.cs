﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using master_data.mappers.Shared;

namespace master_data.mappers.Dtos.Repository
{
    [Table("Tag")]
    public class TagRepoDto : Entity<RepoGuid>
    {
        public TagRepoDto(string value)
        {
            Id = new RepoGuid(Guid.NewGuid());
            Value = value;
        }

        public TagRepoDto(RepoGuid id, string value)
        {
            Id = id;
            Value = value;
        }

        public string Value { get; set; }

        public IEnumerable<JogadorRepositoryDto> Jogadores { get; set; }

        public IEnumerable<RelacionamentoRepositoryDto> Relacionamentos { get; set; }

        protected bool Equals(TagRepoDto other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TagRepoDto) obj);
        }

        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }
    }
}