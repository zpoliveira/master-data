using System.Threading.Tasks;

namespace master_data.repository.Shared
{
    public interface IUnitOfWork
    {
        Task<int> CommitAsync();
    }
}