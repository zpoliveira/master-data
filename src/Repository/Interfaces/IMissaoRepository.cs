using System.Threading.Tasks;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Shared;
using master_data.repository.Shared;

namespace master_data.repository.Interfaces
{
    public interface IMissaoRepository : IRepository<MissaoRepositoryDto,RepoGuid>
    {
        Task<PaginacaoDto<MissaoRepositoryDto>> GetMissoes(string paginaEmail, int paginaNumeroPagina, int paginaTamanhoPagina);
    }
}