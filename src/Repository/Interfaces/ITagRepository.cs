﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Shared;
using master_data.repository.Shared;

namespace master_data.repository.Interfaces
{
    public interface ITagRepository : IRepository<TagRepoDto, RepoGuid>
    {
        Task<List<TagRepoDto>> GetByValuesAsync(List<string> values);

        Task AddRangeAsync(List<TagRepoDto> obj);

        Task<TagRepoDto> GetByValueAsync(string value);
    }
}