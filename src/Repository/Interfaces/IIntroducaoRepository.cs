using System.Collections.Generic;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Introducao;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Shared;
using master_data.repository.Shared;

namespace master_data.repository.Interfaces
{
    public interface IIntroducaoRepository : IRepository<IntroducaoRepositoryDto, RepoGuid>
    {
        Task<IEnumerable<IntroducaoRepositoryDto>> GetListaPedidosLigacaoPendentesAsync(
            ObterListaPedidosLigacaoPendentesDto dto);

        bool UpdateIntroducao(IntroducaoRepositoryDto dto);
    }
}