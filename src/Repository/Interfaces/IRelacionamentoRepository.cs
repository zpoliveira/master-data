using System.Collections.Generic;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Shared;
using master_data.repository.Shared;

namespace master_data.repository.Interfaces
{
    public interface IRelacionamentoRepository : IRepository<RelacionamentoRepositoryDto, RepoGuid>
    {
        public Task<IEnumerable<RelacionamentoRepositoryDto>> getRelacoesJogadorAsync(RepoGuid jogadorDe);

        public Task<RelacionamentoRepositoryDto> getRelacionamentoAsync(string emailJogadorDe, string emailJogadorPara);

        public Task<PaginacaoDto<RelacionamentoRepositoryDto>> GetRelacionamentosAsync(string email, int numeroPagina,int tamanhoPagina);
    }
}