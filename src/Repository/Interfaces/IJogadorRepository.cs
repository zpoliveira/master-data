﻿using System.Collections.Generic;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Shared;
using master_data.repository.Shared;

namespace master_data.repository.Interfaces
{
    public interface IJogadorRepository : IRepository<JogadorRepositoryDto, RepoGuid>
    {
        Task<JogadorRepositoryDto> GetUtilizadorByEmailAsync(string email);
        Task UpdateUtilizadorAsync(JogadorRepositoryDto dto);
        Task EditarEstadoHumorAsync(JogadorRepositoryDto dto);
        Task<List<JogadorRepositoryDto>> PesquisarJogadoresAsync(PesquisarJogadorDto dto);
        void DeleteJogadorByEmail(JogadorRepositoryDto dto);
        Task<PaginacaoDto<JogadorRepositoryDto>> PesquisarJogadoresSugestaoAsync(string email, int numeroPagina, int tamanhoPagina);
    }
}