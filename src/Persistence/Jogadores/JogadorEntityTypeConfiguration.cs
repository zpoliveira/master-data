﻿using master_data.mappers.Dtos.Repository;
using master_data.mappers.Shared;
using master_data.persistence.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace master_data.persistence.Jogadores
{
    internal class JogadorEntityTypeConfiguration : IEntityTypeConfiguration<JogadorRepositoryDto>
    {
        public void Configure(EntityTypeBuilder<JogadorRepositoryDto> entity)
        {
            var converter = new EntityIdValueConverter<RepoGuid>();

            entity.HasKey(k => k.Id);
            entity.Property(p => p.Id).HasConversion(converter);

            entity.HasIndex(k => k.Email).IsUnique();
            entity.HasMany(r => r.tagList).WithMany(r => r.Jogadores);

            // entity.HasOne(e => e.Humor).WithMany(e => e.jogadores);

            entity.HasMany(p => p.IntroducaoIntrodutor).WithOne(p => p.JogadorIntrodutor)
                .OnDelete(DeleteBehavior.Restrict);
            entity.HasMany(p => p.IntroducaoObjectivo).WithOne(p => p.JogadorObjectivo)
                .OnDelete(DeleteBehavior.Restrict);
            entity.HasMany(p => p.IntroducaoRequesitante).WithOne(p => p.JogadorRequisitante)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}