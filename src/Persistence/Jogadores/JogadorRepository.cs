using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Schema;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Shared;
using master_data.persistence.Database;
using master_data.persistence.Shared;
using master_data.repository.Interfaces;
using master_data.repository.Shared;
using Microsoft.EntityFrameworkCore;

namespace master_data.persistence.Jogadores
{
    public class JogadorRepository : BaseRepository<JogadorRepositoryDto, RepoGuid>, IJogadorRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly MasterDataDbContext _dbContext;

        public JogadorRepository(MasterDataDbContext dbContext, IUnitOfWork unitOfWork) : base(dbContext.Jogadores)
        {
            _dbContext = dbContext;
            _unitOfWork = unitOfWork;
        }

        public async Task<JogadorRepositoryDto> GetUtilizadorByEmailAsync(string email)
        {
            var res = await _dbContext.Jogadores.Where(u => u.Email == email).FirstOrDefaultAsync();

            return res;
        }

        public async Task UpdateUtilizadorAsync(JogadorRepositoryDto dto)
        {
            _dbContext.Jogadores.Update(dto);

            await _dbContext.SaveChangesAsync();
        }

        public async Task EditarEstadoHumorAsync(JogadorRepositoryDto dto)
        {
            _dbContext.Jogadores.Update(dto);

            await _dbContext.SaveChangesAsync();
        }


        public async Task<List<JogadorRepositoryDto>> PesquisarJogadoresAsync(PesquisarJogadorDto dto)
        {
            var result = _dbContext.Jogadores.AsQueryable();
            var count = 0;
            const int TOTAL_PARAMETERS = 8;

            if (dto == null) return result.ToList();

            if (!string.IsNullOrEmpty(dto.nome))
                result = result.Where(x => x.Nome == dto.nome);
            else
                count += 1;
            if (!string.IsNullOrEmpty(dto.email))
                result = result.Where(x => x.Email == dto.email);
            else
                count += 1;
            if (!string.IsNullOrEmpty(dto.nacionalidade))
                result = result.Where(x => x.Nacionalidade == dto.nacionalidade);
            else
                count += 1;
            if (!string.IsNullOrEmpty(dto.cidadeResidencia))
                result = result.Where(x => x.CidadeResidencia == dto.cidadeResidencia);
            else
                count += 1;
            if (!string.IsNullOrEmpty(dto.telefone))
                result = result.Where(x => x.Telefone == dto.telefone);
            else
                count += 1;
            if (!string.IsNullOrEmpty(dto.urlFacebook))
                result = result.Where(x => x.UrlFacebook == dto.urlFacebook);
            else
                count += 1;
            if (!string.IsNullOrEmpty(dto.urlLinkedIn))
                result = result.Where(x => x.UrlLinkedIn == dto.urlLinkedIn);
            else
                count += 1;
            if (!string.IsNullOrEmpty(dto.dataNascimento))
                result = result.Where(x => x.DataNascimento.Date == DateTime.Parse(dto.dataNascimento));
            else
                count += 1;
            

            if (count == TOTAL_PARAMETERS) throw new ArgumentException("Pesquisa sem argumentos válidos");

            return result.ToList();
        }

        public void DeleteJogadorByEmail(JogadorRepositoryDto dto)
        {
            _dbContext.Attach(dto);
            _dbContext.Remove(dto);
            _dbContext.SaveChanges();
        }

        public async Task<PaginacaoDto<JogadorRepositoryDto>> PesquisarJogadoresSugestaoAsync(string email, int numeroPagina, int tamanhoPagina)
        {

            //Ler o Jogador
            var queryOrigin = _objs.Where(x => x.Email.Equals(email)).Include(x=>x.tagList);
            
            //Ler as sugestoes
            // var querySugestao = queryOrigin
            //     .SelectMany(x => x.tagList.SelectMany(r => r.Jogadores.Where(t => !t.Email.Equals(x.Email))
            //     )).Distinct().Include(x => x.tagList);

            
            var querySugestao = _objs.Where(x=>x.lstRelacionamentosDe.Any(xx=>  !xx.JogadorDe.Email.Equals(email)) 
                                               || x.lstRelacionamentosPara.Any(xx=>!xx.JogadorPara.Email.Equals(email)))
                .Distinct().Include(x=>x.tagList);
            //Calcular quantas tags em comum
            var final = queryOrigin.SelectMany(origin => querySugestao,
                (origin, sugestao) => new
                {
                    Jogador = sugestao, Match = sugestao.tagList.Count(x => origin.tagList.Contains(x))
                });
                                        
            //Ordenar pelo maior número de tags em comum e devolver os melhores 20
            return await GetPaginacao( final.OrderByDescending(x=>x.Match).Take(50).Select(x=>x.Jogador),numeroPagina,tamanhoPagina);
        }
    }
}