using master_data.mappers.Dtos.Repository;
using master_data.mappers.Shared;
using master_data.persistence.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace master_data.persistence.Jogadores
{
    public class RelacionamentoEntityTypeConfiguration : IEntityTypeConfiguration<RelacionamentoRepositoryDto>
    {
        public void Configure(EntityTypeBuilder<RelacionamentoRepositoryDto> builder)
        {
            var converter = new EntityIdValueConverter<RepoGuid>();

            builder.HasKey(k => k.Id);
            builder.Property(p => p.Id).HasConversion(converter);
            builder.HasOne(x => x.JogadorDe).WithMany(x => x.lstRelacionamentosDe);
            builder.HasOne(x => x.JogadorPara).WithMany(x => x.lstRelacionamentosPara);
            builder.Property(x => x.ForcaRelacao).HasPrecision(18, 4);
            //builder.HasIndex(x => new {IxDe = x.JogadorDe, IxPara = x.JogadorPara}).IsUnique(true);
        }
    }
}