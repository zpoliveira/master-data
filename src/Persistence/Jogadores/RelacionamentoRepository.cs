using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Schema;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Shared;
using master_data.persistence.Database;
using master_data.persistence.Shared;
using master_data.repository.Interfaces;
using master_data.repository.Shared;
using Microsoft.EntityFrameworkCore;

namespace master_data.persistence.Jogadores
{
    public class RelacionamentoRepository : BaseRepository<RelacionamentoRepositoryDto, RepoGuid>,
        IRelacionamentoRepository
    {
        private readonly IUnitOfWork _unitOfWork;

        public RelacionamentoRepository(MasterDataDbContext dbContext, IUnitOfWork unitOfWork)
            : base(dbContext.Relacionamentos)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<RelacionamentoRepositoryDto> getRelacionamentoAsync(string emailJogadorDe, string emailJogadorPara)
        {
            return _objs.Where(x => x.JogadorDe.Email.Equals(emailJogadorDe) && x.JogadorPara.Email.Equals(emailJogadorPara))
                .Include(x => x.JogadorDe)
                .Include(x => x.JogadorPara)
                .Include(x => x.TagList).FirstOrDefaultAsync();
        }

        public async Task<PaginacaoDto<RelacionamentoRepositoryDto>> GetRelacionamentosAsync(string email, int numeroPagina, int tamanhoPagina)
        {
            return await GetPaginacao(
                _objs.Where(x => x.JogadorDe.Email.Equals(email))
                    .Include(x=>x.JogadorDe)
                    .Include(x=>x.JogadorPara)
                    .Include(x=>x.TagList)
                    .OrderBy(x => x.JogadorPara.Nome), 
                numeroPagina,
                tamanhoPagina);
        }

        public async Task<IEnumerable<RelacionamentoRepositoryDto>> getRelacoesJogadorAsync(RepoGuid jogadorDe)
        {
            return await _objs.Where(x => x.JogadorDe.Id.Equals(jogadorDe))
                .Include(x => x.JogadorDe)
                .Include(x => x.JogadorPara)
                .Include(x => x.TagList)
                .ToListAsync();
        }

        public async  Task<List<RelacionamentoRepositoryDto>> GetAllAsync()
        {
            return await _objs.Include(x => x.JogadorDe)
                .Include(x => x.JogadorPara)
                .Include(x => x.TagList)
                .ToListAsync();
        }
        
    }
}