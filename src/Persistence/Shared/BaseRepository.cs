using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Shared;
using master_data.repository.Shared;
using Microsoft.EntityFrameworkCore;

namespace master_data.persistence.Shared
{
    public class BaseRepository<TEntity, TEntityId> : IRepository<TEntity, TEntityId>
        where TEntity : Entity<TEntityId>
        where TEntityId : EntityId
    {
        internal readonly DbSet<TEntity> _objs;

        public BaseRepository(DbSet<TEntity> objs)
        {
            _objs = objs ?? throw new ArgumentNullException(nameof(objs));
        }


        internal async Task<PaginacaoDto<TEntity>> GetPaginacao(IQueryable<TEntity> query, int numeroPagina, int tamanhoPagina)
        {
            return new PaginacaoDto<TEntity>(
                await query.Skip(numeroPagina - 1).Take(tamanhoPagina).ToListAsync(),await query.CountAsync(),numeroPagina,tamanhoPagina);
        }
        
        public async Task<List<TEntity>> GetAllAsync()
        {
            return await _objs.ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(TEntityId id)
        {
            //return await this._context.Categories.FindAsync(id);
            return await _objs
                .Where(x => id.Equals(x.Id)).FirstOrDefaultAsync();
        }

        public async Task<List<TEntity>> GetByIdsAsync(List<TEntityId> ids)
        {
            return await _objs
                .Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<TEntity> AddAsync(TEntity obj)
        {
            var ret = await _objs.AddAsync(obj);
            return ret.Entity;
        }

        public void Remove(TEntity obj)
        {
            _objs.Remove(obj);
        }
    }
}