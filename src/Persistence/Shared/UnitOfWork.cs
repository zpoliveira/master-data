using System.Threading.Tasks;
using master_data.persistence.Database;
using master_data.repository.Shared;

namespace master_data.persistence.Shared
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MasterDataDbContext _context;

        public UnitOfWork(MasterDataDbContext context)
        {
            _context = context;
        }

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}