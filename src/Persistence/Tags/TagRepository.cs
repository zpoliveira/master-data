﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Shared;
using master_data.persistence.Database;
using master_data.persistence.Shared;
using master_data.repository.Interfaces;
using master_data.repository.Shared;
using Microsoft.EntityFrameworkCore;

namespace master_data.persistence.Tags
{
    public class TagRepository : BaseRepository<TagRepoDto, RepoGuid>, ITagRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly MasterDataDbContext _dbContext;

        public TagRepository(IUnitOfWork unitOfWork, MasterDataDbContext dbContext) : base(dbContext.Tags)
        {
            _unitOfWork = unitOfWork;
            _dbContext = dbContext;
        }

        public async Task<List<TagRepoDto>> GetByValuesAsync(List<string> values)
        {
            return await _objs.Where(x => values.Contains(x.Value)).ToListAsync();
        }

        public async Task AddRangeAsync(List<TagRepoDto> obj)
        {
            await _objs.AddRangeAsync(obj);
            return;
        }

        public async Task<TagRepoDto> GetByValueAsync(string value)
        {
            return await _dbContext.Tags.Where(t => t.Value == value).SingleOrDefaultAsync();
        }
    }
}