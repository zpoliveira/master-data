﻿using master_data.mappers.Dtos.Repository;
using master_data.mappers.Shared;
using master_data.persistence.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace master_data.persistence.Tags
{
    public class TagEntityTypeConfiguration : IEntityTypeConfiguration<TagRepoDto>
    {
        public void Configure(EntityTypeBuilder<TagRepoDto> builder)
        {
            var converter = new EntityIdValueConverter<RepoGuid>();

            builder.Property(p => p.Id).HasConversion(converter);
            builder.HasKey(k => k.Id);
            builder.HasIndex(x => x.Value).IsUnique();
        }
    }
}