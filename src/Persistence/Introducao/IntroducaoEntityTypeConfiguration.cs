﻿using master_data.mappers.Dtos.Repository;
using master_data.mappers.Shared;
using master_data.persistence.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace master_data.persistence.Introducao
{
    public class IntroducaoEntityTypeConfiguration : IEntityTypeConfiguration<IntroducaoRepositoryDto>
    {
        public void Configure(EntityTypeBuilder<IntroducaoRepositoryDto> entity)
        {
            var converter = new EntityIdValueConverter<RepoGuid>();

            entity.HasKey(k => k.Id);
            entity.Property(p => p.Id).HasConversion(converter);
            entity.HasOne(x => x.MissaoRepository).WithMany(x => x.Introducoes);
        }
    }
}