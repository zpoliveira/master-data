﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Introducao;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Shared;
using master_data.persistence.Database;
using master_data.persistence.Shared;
using master_data.repository.Interfaces;
using master_data.repository.Shared;
using Microsoft.EntityFrameworkCore;

namespace master_data.persistence.Introducao
{
    public class IntroducaoRepository : BaseRepository<IntroducaoRepositoryDto, RepoGuid>, IIntroducaoRepository
    {
        private readonly MasterDataDbContext _dbContext;
        private readonly IUnitOfWork _unitOfWork;

        private const string ESTADO_PENDENTE = "PENDENTE";

        public IntroducaoRepository(MasterDataDbContext dbContext, IUnitOfWork unitOfWork) : base(dbContext.Introducao)
        {
            _dbContext = dbContext;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<IntroducaoRepositoryDto>> GetListaPedidosLigacaoPendentesAsync(
            ObterListaPedidosLigacaoPendentesDto dto)
        {
            var listIntro = await _dbContext.Introducao
                .Include(i => i.JogadorIntrodutor)
                .Where(i =>
                    i.JogadorIntrodutor.Email == dto.jogadorRequisitanteEmail
                    && i.EstadoIntroducaoIntrodutor == ESTADO_PENDENTE).ToListAsync();
            var listLig = await _dbContext.Introducao
                .Include(i => i.JogadorObjectivo)
                .Where(i =>
                    i.JogadorObjectivo.Email == dto.jogadorRequisitanteEmail
                    && i.EstadoIntroducaoObjectivo == ESTADO_PENDENTE).ToListAsync();

            var result = listIntro.Union(listLig).ToList();   
         

            return result;
        }

        public bool UpdateIntroducao(IntroducaoRepositoryDto dto)
        {
            var result = _dbContext.Introducao.Update(dto);

            return result is not null;
        }

        public async Task<IntroducaoRepositoryDto> GetByIdAsync(RepoGuid id)
        {
            return await _dbContext.Introducao.Where(i => i.Id == id)
                .Include(p => p.JogadorIntrodutor)
                .Include(p => p.JogadorObjectivo)
                .Include(p => p.JogadorRequisitante)
                .FirstOrDefaultAsync();
        }
    }
}