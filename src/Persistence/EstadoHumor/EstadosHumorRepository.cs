using master_data.mappers.Dtos.Repository;
using master_data.mappers.Shared;
using master_data.persistence.Database;
using master_data.persistence.Shared;
using master_data.repository.Shared;
using master_data.repository.Interfaces;



namespace master_data.persistence.EstadoHumor
{
    public class EstadoHumorRepository : BaseRepository<HumorRepositoryDto, RepoGuid>, IEstadoHumorRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly MasterDataDbContext _dbContext;

        public EstadoHumorRepository(MasterDataDbContext dbContext, IUnitOfWork unitOfWork) : base(
            dbContext.EstadosHumor)
        {
            _dbContext = dbContext;
            _unitOfWork = unitOfWork;
        }
        
    }
}