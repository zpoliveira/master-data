using System;
using System.Collections.Generic;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Shared;
using master_data.persistence.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace master_data.persistence.EstadoHumor
{
    internal class EstadoHumorEntityTypeConfiguration : IEntityTypeConfiguration<HumorRepositoryDto>
    {
        public void Configure(EntityTypeBuilder<HumorRepositoryDto> builder)
        {
            var converter = new EntityIdValueConverter<RepoGuid>();

            builder.HasKey(k => k.Id);
            builder.Property(p => p.Id).HasConversion(converter);
            builder.HasData(new List<HumorRepositoryDto>
            {
                new HumorRepositoryDto("FELIZ"),
                new HumorRepositoryDto("TRISTE"),
                new HumorRepositoryDto("NEUTRO")

            });

        }
    }
}