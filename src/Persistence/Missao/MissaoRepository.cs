using System.Linq;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Shared;
using master_data.persistence.Database;
using master_data.persistence.Shared;
using master_data.repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace master_data.persistence.Missao
{
    public class MissaoRepository : BaseRepository<MissaoRepositoryDto, RepoGuid>, IMissaoRepository
    {
        public MissaoRepository(MasterDataDbContext dbContext) : base(dbContext.Missoes)
        {
            
        }


        public async Task<PaginacaoDto<MissaoRepositoryDto>> GetMissoes(string paginaEmail, int paginaNumeroPagina, int paginaTamanhoPagina)
        {
            return await GetPaginacao(_objs.Where(x=>x.JogadorDesafiado.Email.Equals(paginaEmail)).OrderBy(x=>x.JogadorObjectivo.Email)
                .Include(x=>x.JogadorObjectivo)
                .Include(x=>x.JogadorDesafiado),paginaNumeroPagina,paginaTamanhoPagina);
        }
    }
}