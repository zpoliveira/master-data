using master_data.mappers.Dtos.Repository;
using master_data.mappers.Shared;
using master_data.persistence.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace master_data.persistence.Missao
{
    public class MissaoEntityTypeConfiguration  : IEntityTypeConfiguration<MissaoRepositoryDto>
    {
        public void Configure(EntityTypeBuilder<MissaoRepositoryDto> builder)
        {
            var converter = new EntityIdValueConverter<RepoGuid>();
            
            builder.ToTable("Missoes");
            builder.HasKey(k => k.Id);
            builder.Property(p => p.Id).HasConversion(converter);
            builder.Property(p => p.JogadorDesafiadoID).HasConversion(converter);
            builder.Property(p => p.JogadorObjectivoID).HasConversion(converter);
            builder.HasOne(x => x.JogadorDesafiado)
                .WithMany(x => x.MissoesDesafiado)
                .HasForeignKey(x=>x.JogadorDesafiadoID);
            builder.HasOne(x => x.JogadorObjectivo)
                .WithMany(x => x.MissoesObjectivo)
                .HasForeignKey(x=>x.JogadorObjectivoID);
            
        }
    }
}