﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace master_data.persistence.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EstadosHumor",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    EstadoHumor = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstadosHumor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tag",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Jogador",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Telefone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UrlFacebook = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UrlLinkedIn = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Humor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataNascimento = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Nacionalidade = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CidadeResidencia = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DescricaoBreve = table.Column<string>(type: "nvarchar(4000)", maxLength: 4000, nullable: true),
                    Avatar = table.Column<byte[]>(type: "varbinary(max)", nullable: true),
                    HumorRepositoryDtoId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jogador", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Jogador_EstadosHumor_HumorRepositoryDtoId",
                        column: x => x.HumorRepositoryDtoId,
                        principalTable: "EstadosHumor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JogadorRepositoryDtoTagRepoDto",
                columns: table => new
                {
                    JogadoresId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    tagListId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JogadorRepositoryDtoTagRepoDto", x => new { x.JogadoresId, x.tagListId });
                    table.ForeignKey(
                        name: "FK_JogadorRepositoryDtoTagRepoDto_Jogador_JogadoresId",
                        column: x => x.JogadoresId,
                        principalTable: "Jogador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JogadorRepositoryDtoTagRepoDto_Tag_tagListId",
                        column: x => x.tagListId,
                        principalTable: "Tag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Missoes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    JogadorDesafiadoID = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    JogadorObjectivoID = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    GrauDificuldade = table.Column<int>(type: "int", nullable: false),
                    Pontos = table.Column<int>(type: "int", nullable: false),
                    DataHoraInicio = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DataHoraFim = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Missoes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Missoes_Jogador_JogadorDesafiadoID",
                        column: x => x.JogadorDesafiadoID,
                        principalTable: "Jogador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Missoes_Jogador_JogadorObjectivoID",
                        column: x => x.JogadorObjectivoID,
                        principalTable: "Jogador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Relacionamento",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    JogadorDeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    JogadorParaId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    ForcaLigacao = table.Column<int>(type: "int", nullable: false),
                    ForcaRelacao = table.Column<decimal>(type: "decimal(18,4)", precision: 18, scale: 4, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Relacionamento", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Relacionamento_Jogador_JogadorDeId",
                        column: x => x.JogadorDeId,
                        principalTable: "Jogador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Relacionamento_Jogador_JogadorParaId",
                        column: x => x.JogadorParaId,
                        principalTable: "Jogador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Introducao",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    JogadorRequisitanteId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    JogadorIntrodutorId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    JogadorObjectivoId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ForcaLigacao = table.Column<int>(type: "int", nullable: false),
                    EstadoIntroducaoIntrodutor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EstadoIntroducaoObjectivo = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MissaoRepositoryId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Introducao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Introducao_Jogador_JogadorIntrodutorId",
                        column: x => x.JogadorIntrodutorId,
                        principalTable: "Jogador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Introducao_Jogador_JogadorObjectivoId",
                        column: x => x.JogadorObjectivoId,
                        principalTable: "Jogador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Introducao_Jogador_JogadorRequisitanteId",
                        column: x => x.JogadorRequisitanteId,
                        principalTable: "Jogador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Introducao_Missoes_MissaoRepositoryId",
                        column: x => x.MissaoRepositoryId,
                        principalTable: "Missoes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RelacionamentoRepositoryDtoTagRepoDto",
                columns: table => new
                {
                    RelacionamentosId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TagListId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RelacionamentoRepositoryDtoTagRepoDto", x => new { x.RelacionamentosId, x.TagListId });
                    table.ForeignKey(
                        name: "FK_RelacionamentoRepositoryDtoTagRepoDto_Relacionamento_RelacionamentosId",
                        column: x => x.RelacionamentosId,
                        principalTable: "Relacionamento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RelacionamentoRepositoryDtoTagRepoDto_Tag_TagListId",
                        column: x => x.TagListId,
                        principalTable: "Tag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Introducao_JogadorIntrodutorId",
                table: "Introducao",
                column: "JogadorIntrodutorId");

            migrationBuilder.CreateIndex(
                name: "IX_Introducao_JogadorObjectivoId",
                table: "Introducao",
                column: "JogadorObjectivoId");

            migrationBuilder.CreateIndex(
                name: "IX_Introducao_JogadorRequisitanteId",
                table: "Introducao",
                column: "JogadorRequisitanteId");

            migrationBuilder.CreateIndex(
                name: "IX_Introducao_MissaoRepositoryId",
                table: "Introducao",
                column: "MissaoRepositoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Jogador_Email",
                table: "Jogador",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Jogador_HumorRepositoryDtoId",
                table: "Jogador",
                column: "HumorRepositoryDtoId");

            migrationBuilder.CreateIndex(
                name: "IX_JogadorRepositoryDtoTagRepoDto_tagListId",
                table: "JogadorRepositoryDtoTagRepoDto",
                column: "tagListId");

            migrationBuilder.CreateIndex(
                name: "IX_Missoes_JogadorDesafiadoID",
                table: "Missoes",
                column: "JogadorDesafiadoID");

            migrationBuilder.CreateIndex(
                name: "IX_Missoes_JogadorObjectivoID",
                table: "Missoes",
                column: "JogadorObjectivoID");

            migrationBuilder.CreateIndex(
                name: "IX_Relacionamento_JogadorDeId",
                table: "Relacionamento",
                column: "JogadorDeId");

            migrationBuilder.CreateIndex(
                name: "IX_Relacionamento_JogadorParaId",
                table: "Relacionamento",
                column: "JogadorParaId");

            migrationBuilder.CreateIndex(
                name: "IX_RelacionamentoRepositoryDtoTagRepoDto_TagListId",
                table: "RelacionamentoRepositoryDtoTagRepoDto",
                column: "TagListId");

            migrationBuilder.CreateIndex(
                name: "IX_Tag_Value",
                table: "Tag",
                column: "Value",
                unique: true,
                filter: "[Value] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Introducao");

            migrationBuilder.DropTable(
                name: "JogadorRepositoryDtoTagRepoDto");

            migrationBuilder.DropTable(
                name: "RelacionamentoRepositoryDtoTagRepoDto");

            migrationBuilder.DropTable(
                name: "Missoes");

            migrationBuilder.DropTable(
                name: "Relacionamento");

            migrationBuilder.DropTable(
                name: "Tag");

            migrationBuilder.DropTable(
                name: "Jogador");

            migrationBuilder.DropTable(
                name: "EstadosHumor");
        }
    }
}
