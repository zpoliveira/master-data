﻿using System.Linq;
using master_data.mappers.Dtos.Repository;
using master_data.persistence.Introducao;
using master_data.persistence.Jogadores;
using master_data.persistence.Missao;
using master_data.persistence.Tags;
using Microsoft.EntityFrameworkCore;

namespace master_data.persistence.Database
{
    public class MasterDataDbContext : DbContext
    {
        public MasterDataDbContext(DbContextOptions<MasterDataDbContext> options) : base(options)
        {
        }


        public DbSet<JogadorRepositoryDto> Jogadores { get; set; }
        public DbSet<RelacionamentoRepositoryDto> Relacionamentos { get; set; }

        public DbSet<TagRepoDto> Tags { get; set; }
        public DbSet<HumorRepositoryDto> EstadosHumor { get; set; }
        public DbSet<IntroducaoRepositoryDto> Introducao { get; set; }
        public DbSet<MissaoRepositoryDto> Missoes { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.ApplyConfiguration(new EstadoHumorEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TagEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RelacionamentoEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new IntroducaoEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new JogadorEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MissaoEntityTypeConfiguration());
            
        }
    }
}