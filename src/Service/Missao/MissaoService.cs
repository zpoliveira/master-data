using System;
using System.Threading.Tasks;
using master_data.domain.Model.ValueObjects;
using master_data.mappers.Dtos.Controller.Missao;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Interfaces;
using master_data.repository.Interfaces;
using master_data.repository.Shared;

namespace master_data.service.Missao
{
    public class MissaoService : IMissaoService
    {

        private readonly IJogadorRepository _jogadorRepository;
        private readonly IMissaoRepository _missaoRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMissaoMapper _missaoMapper;
        private readonly IJogadorMapper _jogadorMapper;
        private readonly ITagMapper _tagMapper;
        public MissaoService(IJogadorRepository jogadorRepository, IMissaoRepository missaoRepository, IUnitOfWork unitOfWork, IMissaoMapper missaoMapper, IJogadorMapper jogadorMapper, ITagMapper tagMapper)
        {
            _jogadorRepository = jogadorRepository;
            _missaoRepository = missaoRepository;
            _unitOfWork = unitOfWork;
            _missaoMapper = missaoMapper;
            _jogadorMapper = jogadorMapper;
            _tagMapper = tagMapper;
        }


        public async Task<RegistarMissaoDto> RegistarMissao(RegistarMissaoDto dto)
        {
            JogadorRepositoryDto jogadorDesafiadoRepo = await _jogadorRepository.GetUtilizadorByEmailAsync(dto.JogadorDesafiado);

            JogadorRepositoryDto jogadorObjectivoRepo = await _jogadorRepository.GetUtilizadorByEmailAsync(dto.JogadorObjectivo);

            domain.Model.Entities.Jogador jogadorDesafiadoDomain = _jogadorMapper.FromRepositoryToDomain(jogadorDesafiadoRepo);
            
            domain.Model.Entities.Jogador jogadorObjectivoDomain = _jogadorMapper.FromRepositoryToDomain(jogadorObjectivoRepo);

            domain.Model.Entities.Missao missaoDomain = new domain.Model.Entities.Missao(jogadorDesafiadoDomain,
                jogadorObjectivoDomain,
                new GrauDificuldade(dto.GrauDificuldade), new DataHoraInicio(DateTime.Now));

            MissaoRepositoryDto repositoryDto = _missaoMapper.FromDomainToRepository(missaoDomain);

           MissaoRepositoryDto missaoInserida = await _missaoRepository.AddAsync(repositoryDto);



          int entitiesAlteradas = await _unitOfWork.CommitAsync();

          if (entitiesAlteradas < 1)
              throw new ApplicationException("Não foi possível gravar as alterações.");

          return dto;
        }

        public async Task<PaginacaoDto<MissaoDto>> GetMissoesAsync(PaginaMissaoDto pagina)
        {
            PaginacaoDto<MissaoRepositoryDto> lst = await  _missaoRepository.GetMissoes(pagina.email, pagina.NumeroPagina, pagina.TamanhoPagina);

            return _missaoMapper.FromRepositoryToMissaoDto(lst);
        }
    }
}