using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Missao;
using master_data.mappers.Dtos.Shared;

namespace master_data.service.Missao
{
    public interface IMissaoService
    {
        Task<RegistarMissaoDto> RegistarMissao(RegistarMissaoDto dto);
        Task<PaginacaoDto<MissaoDto>> GetMissoesAsync(PaginaMissaoDto pagina);
    }
}