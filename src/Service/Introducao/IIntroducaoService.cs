using System.Collections.Generic;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Introducao;
using master_data.mappers.Dtos.Repository;

namespace master_data.service.Introducao
{
    public interface IIntroducaoService
    {
        Task<List<LigacaoPendenteDto>> obterListaPedidosLigacaoPendentesAsync(
            ObterListaPedidosLigacaoPendentesDto dto);

        Task<bool> aceitarPedidoIntroducaoAsync(AceitarIntroducaoDto dto);

        Task criaPedidoLigacaoAsync(PedidoLigacaoDto dto);

        Task criarPedidoIntroducaoAsync(PedidoIntroducaoDto dto);
    }
}