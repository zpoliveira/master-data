using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using master_data.domain.Shared;
using master_data.mappers.Dtos.Controller.Introducao;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Interfaces;
using master_data.mappers.Shared;
using master_data.repository.Interfaces;
using master_data.repository.Shared;

namespace master_data.service.Introducao
{
    public class IntroducaoService : IIntroducaoService
    {
        public IntroducaoService(IUnitOfWork unitOfWork,
            IIntroducaoMapper introducaoMapper,
            IIntroducaoRepository introducaoRepository,
            IJogadorRepository jogadorRepository,
            ITagRepository repositoryTag,
            IRelacionamentoRepository relacionamentoRepository)
        {
            _unitOfWork = unitOfWork;
            _introducaoMapper = introducaoMapper;
            _introducaoRepository = introducaoRepository;
            _jogadorRepository = jogadorRepository;
            _repositoryTag = repositoryTag;
            _relacionamentoRepository = relacionamentoRepository;
        }

        private readonly IUnitOfWork _unitOfWork;
        private readonly IIntroducaoMapper _introducaoMapper;

        private readonly IIntroducaoRepository _introducaoRepository;
        private readonly IJogadorRepository _jogadorRepository;
        private readonly ITagRepository _repositoryTag;
        private readonly IRelacionamentoRepository _relacionamentoRepository;

        public async Task<List<LigacaoPendenteDto>> obterListaPedidosLigacaoPendentesAsync(
            ObterListaPedidosLigacaoPendentesDto dto)
        {
            var repoList = await _introducaoRepository.GetListaPedidosLigacaoPendentesAsync(dto);
            var res = _introducaoMapper.ToListaLigacaoPendenteDto(repoList);
            
            return res;
        }

        public async Task criaPedidoLigacaoAsync(PedidoLigacaoDto dto)
        {
            var jogadorRequisitante = await _jogadorRepository.GetUtilizadorByEmailAsync(dto.emailJogadorRequisitante);
            if (jogadorRequisitante == null)
                throw new ApplicationException("Não foi encontrado o jogador requisitante");
            var jogadorObjetivo = await _jogadorRepository.GetUtilizadorByEmailAsync(dto.emailJogadorObjectivo);
            if (jogadorObjetivo == null) throw new ApplicationException("Não foi encontrado o jogador objetivo");

            var relacao =
                _introducaoMapper.FromPedidoLigacaoDto(jogadorRequisitante, jogadorObjetivo, dto.forcaLigacao);

            await _introducaoRepository.AddAsync(relacao);
            await _unitOfWork.CommitAsync();
        }

        public async Task criarPedidoIntroducaoAsync(PedidoIntroducaoDto dto)
        {
            var jogadorRequisitante = await _jogadorRepository.GetUtilizadorByEmailAsync(dto.emailJogadorRequisitante);
            if (jogadorRequisitante == null)
                throw new ApplicationException("Não foi encontrado o jogador requisitante");

            var jogadorIntermediario =
                await _jogadorRepository.GetUtilizadorByEmailAsync(dto.emailJogadorIntermediario);
            if (jogadorIntermediario == null)
                throw new ApplicationException("Não foi encontrado o jogador intermediario");

            var jogadorObjetivo = await _jogadorRepository.GetUtilizadorByEmailAsync(dto.emailJogadorObjectivo);
            if (jogadorObjetivo == null) throw new ApplicationException("Não foi encontrado o jogador objetivo");

            var relacao =
                _introducaoMapper.FromPedidoIntroducaoDto(jogadorRequisitante, jogadorIntermediario, jogadorObjetivo,
                    dto.forcaLigacao);

            await _introducaoRepository.AddAsync(relacao);
            await _unitOfWork.CommitAsync();
        }

        public async Task<bool> aceitarPedidoIntroducaoAsync(AceitarIntroducaoDto dto)
        {
            var introducaoRepositoryDto = await _introducaoRepository.GetByIdAsync(new RepoGuid(dto.idIntroducao));

            if (introducaoRepositoryDto.JogadorIntrodutor != null &&
                dto.emailJogador == introducaoRepositoryDto.JogadorIntrodutor.Email)
            {
                introducaoRepositoryDto.EstadoIntroducaoIntrodutor = EstadoEnum.ACEITE.ToString();
            }
            else if (introducaoRepositoryDto.JogadorObjectivo != null &&
                     dto.emailJogador == introducaoRepositoryDto.JogadorObjectivo.Email)
            {
                introducaoRepositoryDto.EstadoIntroducaoObjectivo = EstadoEnum.ACEITE.ToString();
            }
            else
            {
                introducaoRepositoryDto.EstadoIntroducaoIntrodutor = EstadoEnum.PENDENTE.ToString();
                introducaoRepositoryDto.EstadoIntroducaoObjectivo = EstadoEnum.PENDENTE.ToString();
            }

            _introducaoRepository.UpdateIntroducao(introducaoRepositoryDto);


            if ((introducaoRepositoryDto.EstadoIntroducaoIntrodutor == EstadoEnum.ACEITE.ToString() ||
                 introducaoRepositoryDto.JogadorIntrodutor == null)
                && introducaoRepositoryDto.EstadoIntroducaoObjectivo == EstadoEnum.ACEITE.ToString())
            {
                //Ler as tags existentes na base de dados
                var tags = await _repositoryTag.GetByValuesAsync(dto.tags.ToList());
                var tagsNew = dto.tags.Where(x => !tags.Exists(w => w.Value.Equals(x)));
                var lstAdd = tagsNew.Select(s => new TagRepoDto(s)).ToList();
                await _repositoryTag.AddRangeAsync(lstAdd);
                tags.AddRange(lstAdd);

                var relacaoAB = new RelacionamentoRepositoryDto(
                    new RepoGuid(Guid.NewGuid()),
                    introducaoRepositoryDto.JogadorRequisitante,
                    introducaoRepositoryDto.JogadorObjectivo,
                    dto.forcaLigacao,
                    0,
                    tags);

                var relacaoBA = new RelacionamentoRepositoryDto
                (
                    new RepoGuid(Guid.NewGuid()),
                    introducaoRepositoryDto.JogadorObjectivo,
                    introducaoRepositoryDto.JogadorRequisitante,
                    dto.forcaLigacao,
                    0,
                    tags);

                await _relacionamentoRepository.AddAsync(relacaoAB);
                await _relacionamentoRepository.AddAsync(relacaoBA);
            }

            var res = await _unitOfWork.CommitAsync();

            return res > 0;
        }
    }
}