using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using master_data.domain.Model.ValueObjects;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Controller.Relacionamento;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Interfaces;
using master_data.mappers.Shared;
using master_data.repository.Interfaces;
using master_data.repository.Shared;
using master_data.social_engine.Engine;

namespace master_data.service.Relacionamento
{
    public class RelacionamentoService : IRelacionamentoService
    {
        private readonly IRelacionamentoMapper _relacionamentoMapper;
        private readonly IRelacionamentoRepository _repositoryRelacionamento;
        private readonly IJogadorRepository _jogadorRepository;
        private readonly ITagRepository _repositoryTag;
        private readonly IUnitOfWork _unitOfWork;

        private readonly SocialNetwork _socialNetwork;

        public RelacionamentoService(
            IUnitOfWork unitOfWork,
            IRelacionamentoMapper relacionamentoMapper,
            IRelacionamentoRepository repositoryRelacionamento,
            IJogadorRepository jogadorRepository,
            ITagRepository repositoryTag,
            SocialNetwork socialNetwork)
        {
            _relacionamentoMapper = relacionamentoMapper;
            _repositoryRelacionamento = repositoryRelacionamento;
            _repositoryTag = repositoryTag;
            _jogadorRepository = jogadorRepository;
            _unitOfWork = unitOfWork;
            _socialNetwork = socialNetwork;
            
        }


        public async Task<JogadorComLigacoesDiretasDto> verRedeLigacaoEmNiveis(int niveis, string emailJogador)
        {
            _socialNetwork.Initialize();
            
            var jogador = await _jogadorRepository.GetUtilizadorByEmailAsync(emailJogador);
            var idJogador = jogador.Id;

            var jogadorNode = _socialNetwork.getNodePerspective(idJogador.AsGuid(), niveis);
            
            return _relacionamentoMapper.FromSocialNetworkJogadorNodeToJogadorComLigacoesDiretasDto(jogadorNode);
        }

        public async Task<PaginacaoDto<RelacionamentoDto>> GetRelacionamentosAsync(PaginaRelacionamentoDto pagina)
        {
           var dadosRepository = await _repositoryRelacionamento.GetRelacionamentosAsync(pagina.email, pagina.NumeroPagina, pagina.TamanhoPagina);

           return _relacionamentoMapper.FromRepositoryToRelacionamentoDto(dadosRepository);
        }


        public async Task<EditarRelacionamentoDto> EditarRelacionamentoAsync(EditarRelacionamentoDto dto)
        {
            var relacionamento =
                await _repositoryRelacionamento.getRelacionamentoAsync(dto.EmailJogadorDe,
                    dto.EmailJogadorPara);
            if (relacionamento is null) return null;

            // Criar modelo de dominio, para validar as regras de negócio
            var domainRelacionamento = _relacionamentoMapper.toDomain(relacionamento);
            
            domainRelacionamento.AlterarForcaLigacao(dto.Forca);
            domainRelacionamento.AlterarListaTags(dto.LstTags);

            //Ler as tags existentes na base de dados
            var tags = await _repositoryTag.GetByValuesAsync(dto.LstTags.ToList());
            var tagsNew = dto.LstTags.Where(x => !tags.Exists(w => w.Value.Equals(x)));

            var lstAdd = new List<TagRepoDto>();
            foreach (var s in tagsNew) lstAdd.Add(new TagRepoDto(s));
            await _repositoryTag.AddRangeAsync(lstAdd);

            //unir as listas
            tags.AddRange(lstAdd);
            relacionamento.ForcaLigacao = dto.Forca;
            relacionamento.TagList = tags;

            await _unitOfWork.CommitAsync();

            return _relacionamentoMapper.FromRepositoryToEditarRelacionamentoDto(relacionamento);
        }
    }
}