using System;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Controller.Relacionamento;
using master_data.mappers.Dtos.Shared;

namespace master_data.service.Relacionamento
{
    public interface IRelacionamentoService
    {
         Task<EditarRelacionamentoDto> EditarRelacionamentoAsync(EditarRelacionamentoDto dto);
         Task<JogadorComLigacoesDiretasDto> verRedeLigacaoEmNiveis(int niveis, string emailJogador);
         Task<PaginacaoDto<RelacionamentoDto>> GetRelacionamentosAsync(PaginaRelacionamentoDto pagina);
    }
}