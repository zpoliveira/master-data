﻿using System.Collections.Generic;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;

namespace master_data.service.Jogador
{
    public interface IJogadorService
    {
        public Task<RegistarJogadorDto> criarNovoJogadorAsync(RegistarJogadorDto dto);
        public Task updateJogadorAsync(UpdateJogadorDto dto);
        public Task<List<RegistarJogadorDto>> GetAllAsync();
        public Task<RegistarJogadorDto> editarEstadoHumorAsync(EditarHumorDto dto);
        public Task<List<RegistarJogadorDto>> pesquisarJogadoresAsync(PesquisarJogadorDto dto);
        public void deleteJogadorByEmail(string email);
        public Task<List<HumorControllerDto>> obterEstadosHumorAsync();
        public Task<PaginacaoDto<JogadorSugestaoDto>> GetSugestoesJogador(PaginaJogadorSugestaoDto pagina);
    }
}