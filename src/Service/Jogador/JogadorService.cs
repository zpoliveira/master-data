﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using master_data.mappers.Dtos.Controller.Jogador;
using master_data.mappers.Dtos.Repository;
using master_data.mappers.Dtos.Shared;
using master_data.mappers.Interfaces;
using master_data.repository.Interfaces;
using master_data.repository.Shared;

namespace master_data.service.Jogador
{
    public class JogadorService : IJogadorService
    {
        public JogadorService(IUnitOfWork unitOfWork,
            IJogadorMapper jogadorMapper,
            ITagRepository tagRepository,
            IJogadorRepository jogadorRepository,
            IHumorMapper humorMapper,
            IEstadoHumorRepository humorRepository)
        {
            _unitOfWork = unitOfWork;
            _jogadorMapper = jogadorMapper;
            _tagRepository = tagRepository;
            _jogadorRepository = jogadorRepository;
            _humorMapper = humorMapper;
            _humorRepository = humorRepository;

        }

        private readonly IUnitOfWork _unitOfWork;
        private readonly IJogadorMapper _jogadorMapper;
        private readonly ITagRepository _tagRepository;

        private readonly IJogadorRepository _jogadorRepository;

         private readonly IHumorMapper _humorMapper;

        private IEstadoHumorRepository _humorRepository { get; }

        public async Task<RegistarJogadorDto> criarNovoJogadorAsync(RegistarJogadorDto dto)
        {
            var jogador = _jogadorMapper.FromRegistarUtilizadorDto(dto);

            //Ler as tags existentes na base de dados
            var tags = await _tagRepository.GetByValuesAsync(dto.tagList.ToList());
            var tagsNew = dto.tagList.Where(x => !tags.Exists(w => w.Value.Equals(x)));

            var lstAdd = tagsNew.Select(s => new TagRepoDto(s)).ToList();
            await _tagRepository.AddRangeAsync(lstAdd);

            //unir as listas
            tags.AddRange(lstAdd);
            
            var jogadorRepositoryDto = _jogadorMapper.ToRepositoryDto(jogador);
            jogadorRepositoryDto.tagList = tags;
            
            var anyJogador = await _jogadorRepository.GetUtilizadorByEmailAsync(jogadorRepositoryDto.Email);

            if (anyJogador is not null)
                return _jogadorMapper.ToRegistarUtilizadorDto(_jogadorMapper.FromRepositoryToDomain(anyJogador));

            await _jogadorRepository.AddAsync(jogadorRepositoryDto);

            await _unitOfWork.CommitAsync();

            return dto;
        }

        public async Task updateJogadorAsync(UpdateJogadorDto dto)
        {
            var jogador = await _jogadorRepository.GetUtilizadorByEmailAsync(dto.email);

            if (jogador == null) throw new ApplicationException("Jogador Inexistente");

            jogador.Nome = dto.nome;
            jogador.Password = dto.password;
            jogador.Telefone = dto.telefone;
            jogador.DataNascimento = DateTime.Parse(dto.dataNascimento);
            jogador.UrlFacebook = dto.urlFacebook;
            jogador.UrlLinkedIn = dto.urlLinkedIn;
            jogador.Humor = dto.humor;
            
            //Ler as tags existentes na base de dados
            var tags = await _tagRepository.GetByValuesAsync(dto.tagList.ToList());
            var tagsNew = dto.tagList.Where(x => !tags.Exists(w => w.Value.Equals(x)));

            var lstAdd = tagsNew.Select(s => new TagRepoDto(s)).ToList();
            await _tagRepository.AddRangeAsync(lstAdd);

            //unir as listas
            tags.AddRange(lstAdd);
            
            jogador.tagList = tags;

            await _jogadorRepository.UpdateUtilizadorAsync(jogador);
        }

        public async Task<List<RegistarJogadorDto>> GetAllAsync()
        {
            var todosJogadores = await _jogadorRepository.GetAllAsync();

            var result = todosJogadores.Select(jogador =>
                    _jogadorMapper.ToRegistarUtilizadorDto(
                        _jogadorMapper.FromRepositoryToDomain(jogador)))
                .ToList();

            return result;
        }

        public async Task<RegistarJogadorDto> editarEstadoHumorAsync(EditarHumorDto dto)
        {
            if (Enum.IsDefined(typeof(domain.Shared.EstadoHumor), dto.humor))
            {
                var jogador = await _jogadorRepository.GetUtilizadorByEmailAsync(dto.email);

                if (jogador == null) throw new ApplicationException("Jogador Inexistente");

                //TODO: Validar se humor existe
                jogador.Humor = dto.humor; //new HumorRepositoryDto(dto.humor);

                await _jogadorRepository.EditarEstadoHumorAsync(jogador);

                var jogadorHumorAtualizado = await _jogadorRepository.GetUtilizadorByEmailAsync(dto.email);

                return _jogadorMapper.ToRegistarUtilizadorDto(_jogadorMapper.FromRepositoryToDomain(jogadorHumorAtualizado));
            }
            else
            {
                throw new ApplicationException("Estado de Humor não consta da lista.");
            }
        }

        public async Task<List<RegistarJogadorDto>> pesquisarJogadoresAsync(PesquisarJogadorDto dto)
        {
            var resultadoPesquisa = await _jogadorRepository.PesquisarJogadoresAsync(dto);

            if (resultadoPesquisa == null || resultadoPesquisa.Count == 0 )
                throw new ArgumentException("Pesquisa sem resultados");

            var resultadoJogador = resultadoPesquisa.Select(jogador => _jogadorMapper.FromRepositoryToDomain(jogador));

            return resultadoJogador.Select(jogador => _jogadorMapper.ToRegistarUtilizadorDto(jogador)).ToList();
        }

        public void deleteJogadorByEmail(string email)
        {
            var dto = new PesquisarJogadorDto();
            dto.email = email;
            var jogador = _jogadorRepository.PesquisarJogadoresAsync(dto).Result;


            _jogadorRepository.DeleteJogadorByEmail(jogador[0]);
        }


        public async Task<List<HumorControllerDto>> obterEstadosHumorAsync()
        {
            var estadosHumor = await _humorRepository.GetAllAsync();

            var result = estadosHumor.Select(humor =>
                    _humorMapper.ToListControllerDto(humor))
                .ToList();

            return result;
        }

        public async Task<PaginacaoDto<JogadorSugestaoDto>> GetSugestoesJogador(PaginaJogadorSugestaoDto pagina)
        {
            PaginacaoDto<JogadorRepositoryDto> lst = await  _jogadorRepository.PesquisarJogadoresSugestaoAsync(pagina.email, pagina.NumeroPagina, pagina.TamanhoPagina);

          return _jogadorMapper.FromRepositoryToJogadorSugestaoDto(lst);

        }
    }
}